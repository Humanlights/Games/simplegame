using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public class NetworkScenePostProcess : MonoBehaviour
    {
        [PostProcessScene]
        public static void OnPostProcessScene()
        {
            IEnumerable<NetworkIdentity> identities = Resources.FindObjectsOfTypeAll<NetworkIdentity>()
                .Where(identity => identity.gameObject.hideFlags != HideFlags.NotEditable &&
                                   identity.gameObject.hideFlags != HideFlags.HideAndDontSave &&
                                   identity.gameObject.scene.name != "DontDestroyOnLoad" &&
                                   !PrefabUtility.IsPartOfPrefabAsset(identity.gameObject));

            foreach (NetworkIdentity identity in identities)
            {
                if (identity.GetComponent<NetworkManager>() != null)
                {
                    Debug.LogError("NetworkManager has a NetworkIdentity component. This will cause the NetworkManager object to be disabled, so it is not recommended.");
                }

                if (!identity.isClient && !identity.isServer)
                {
                    if (identity.sceneId != 0)
                    {
                        identity.SetSceneIdSceneHashPartInternal();

                        identity.gameObject.SetActive(false);

        #if UNITY_2018_2_OR_NEWER
                        GameObject prefabGO = PrefabUtility.GetCorrespondingObjectFromSource(identity.gameObject) as GameObject;
        #else
                        GameObject prefabGO = PrefabUtility.GetPrefabParent(identity.gameObject) as GameObject;
        #endif
                        if (prefabGO)
                        {
        #if UNITY_2018_3_OR_NEWER
                            GameObject prefabRootGO = prefabGO.transform.root.gameObject;
        #else
                            GameObject prefabRootGO = PrefabUtility.FindPrefabRoot(prefabGO);
        #endif
                            if (prefabRootGO)
                            {
                                if (prefabRootGO.GetComponentsInChildren<NetworkIdentity>().Length > 1)
                                {
                                    Debug.LogWarningFormat("Prefab '{0}' has several NetworkIdentity components attached to itself or its children, this is not supported.", prefabRootGO.name);
                                }
                            }
                        }
                    }
                    else Debug.LogError("Scene " + identity.gameObject.scene.path + " needs to be opened and resaved, because the scene object " + identity.name + " has no valid sceneId yet.");
                }
            }
        }
    }
}
