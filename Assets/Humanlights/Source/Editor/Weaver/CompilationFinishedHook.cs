using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;
using UnityAssembly = UnityEditor.Compilation.Assembly;

namespace Humanlights.Unity.UNet.Weaver
{
    public static class CompilationFinishedHook
    {
        const string HumanlightsRuntimeAssemblyName = "Humanlights.Unity.UNet";
        const string HumanlightsWeaverAssemblyName = "Humanlights.Unity.UNet.Weaver";

        public static Action<string> OnWeaverMessage;
        public static Action<string> OnWeaverWarning;
        public static Action<string> OnWeaverError;

        public static bool WeaverEnabled { get; set; }
        public static bool UnityLogEnabled = true;
        public static bool WeaveFailed { get; private set; }

        static void HandleMessage ( string msg )
        {
            if ( UnityLogEnabled ) Debug.Log ( msg );
            if ( OnWeaverMessage != null ) OnWeaverMessage.Invoke ( msg );
        }

        static void HandleWarning ( string msg )
        {
            if ( UnityLogEnabled ) Debug.LogWarning ( msg );
            if ( OnWeaverWarning != null ) OnWeaverWarning.Invoke ( msg );
        }

        static void HandleError ( string msg )
        {
            if ( UnityLogEnabled ) Debug.LogError ( msg );
            if ( OnWeaverError != null ) OnWeaverError.Invoke ( msg );
        }

        [InitializeOnLoadMethod]
        static void OnInitializeOnLoad ()
        {
            CompilationPipeline.assemblyCompilationFinished += OnCompilationFinished;

            if ( !SessionState.GetBool ( "HUMANLIGHTS_WEAVED", false ) )
            {
                SessionState.SetBool ( "HUMANLIGHTS_WEAVED", true );
                WeaveExisingAssemblies ();
            }
        }

        static void WeaveExisingAssemblies ()
        {
            foreach ( UnityAssembly assembly in CompilationPipeline.GetAssemblies () )
            {
                if ( File.Exists ( assembly.outputPath ) )
                {
                    OnCompilationFinished ( assembly.outputPath, new CompilerMessage [ 0 ] );
                }
            }

            EditorUtility.RequestScriptReload ();
        }

        static string FindMirrorRuntime ()
        {
            foreach ( UnityAssembly assembly in CompilationPipeline.GetAssemblies () )
            {
                if ( assembly.name == HumanlightsRuntimeAssemblyName )
                {
                    return assembly.outputPath;
                }
            }
            return "";
        }

        static bool CompilerMessagesContainError ( CompilerMessage [] messages )
        {
            return messages.Any ( msg => msg.type == CompilerMessageType.Error );
        }

        static void OnCompilationFinished ( string assemblyPath, CompilerMessage [] messages )
        {
            if ( CompilerMessagesContainError ( messages ) )
            {
                // Debug.Log ( "Weaver: stop because compile errors on target" );
                return;
            }

            if ( assemblyPath.Contains ( "-Editor" ) || assemblyPath.Contains ( ".Editor" ) )
            {
                return;
            }

            string assemblyName = Path.GetFileNameWithoutExtension ( assemblyPath );
            if ( assemblyName == HumanlightsRuntimeAssemblyName || assemblyName == HumanlightsWeaverAssemblyName )
            {
                return;
            }

            string mirrorRuntimeDll = FindMirrorRuntime ();
            if ( string.IsNullOrEmpty ( mirrorRuntimeDll ) )
            {
                // Debug.LogError ( "Failed to find Humanlights.Unity.UNet runtime assembly" );
                return;
            }
            if ( !File.Exists ( mirrorRuntimeDll ) )
            {
                return;
            }

            string unityEngineCoreModuleDLL = UnityEditorInternal.InternalEditorUtility.GetEngineCoreModuleAssemblyPath ();
            if ( string.IsNullOrEmpty ( unityEngineCoreModuleDLL ) )
            {
                // Debug.LogError ( "Failed to find UnityEngine assembly" );
                return;
            }

            HashSet<string> dependencyPaths = new HashSet<string> ();
            dependencyPaths.Add ( Path.GetDirectoryName ( assemblyPath ) );
            foreach ( UnityAssembly unityAsm in CompilationPipeline.GetAssemblies () )
            {
                if ( unityAsm.outputPath != assemblyPath ) continue;

                foreach ( string unityAsmRef in unityAsm.compiledAssemblyReferences )
                {
                    dependencyPaths.Add ( Path.GetDirectoryName ( unityAsmRef ) );
                }
            }

            if ( Program.Process ( unityEngineCoreModuleDLL, mirrorRuntimeDll, null, new [] { assemblyPath }, dependencyPaths.ToArray (), HandleWarning, HandleError ) )
            {
                WeaveFailed = false;
            }
            else
            {
                WeaveFailed = true;
                // if ( UnityLogEnabled ) Debug.LogError ( "Weaving failed for: " + assemblyPath );
            }
        }
    }
}
