using Mono.CecilX;
using Mono.CecilX.Cil;

namespace Humanlights.Unity.UNet.Weaver
{
    static class SyncListProcessor
    {
        public static void Process(TypeDefinition td)
        {
            SyncObjectProcessor.GenerateSerialization(td, 0, "SerializeItem", "DeserializeItem");
        }
    }
}
