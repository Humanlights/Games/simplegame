using Mono.CecilX;

namespace Humanlights.Unity.UNet.Weaver
{
    static class MonoBehaviourProcessor
    {
        public static void Process ( TypeDefinition td )
        {
            ProcessSyncVars ( td );
            ProcessMethods ( td );
        }

        static void ProcessSyncVars ( TypeDefinition td )
        {
            foreach ( FieldDefinition fd in td.Fields )
            {
                foreach ( CustomAttribute ca in fd.CustomAttributes )
                {
                    if ( ca.AttributeType.FullName == Weaver.SyncVarType.FullName )
                    {
                        Weaver.Error ( $"[SyncVar] {fd} must be inside a NetworkBehaviour.  {td} is not a NetworkBehaviour" );
                    }
                }

                if ( SyncObjectInitializer.ImplementsSyncObject ( fd.FieldType ) )
                {
                    Weaver.Error ( $"{fd} is a SyncObject and must be inside a NetworkBehaviour.  {td} is not a NetworkBehaviour" );
                }
            }
        }

        static void ProcessMethods ( TypeDefinition td )
        {
            foreach ( MethodDefinition md in td.Methods )
            {
                foreach ( CustomAttribute ca in md.CustomAttributes )
                {
                    if ( ca.AttributeType.FullName == Weaver.CommandType.FullName )
                    {
                        Weaver.Error ( $"[Command] {md} must be declared inside a NetworkBehaviour" );
                    }

                    if ( ca.AttributeType.FullName == Weaver.ClientRpcType.FullName )
                    {
                        Weaver.Error ( $"[ClienRpc] {md} must be declared inside a NetworkBehaviour" );
                    }

                    if ( ca.AttributeType.FullName == Weaver.TargetRpcType.FullName )
                    {
                        Weaver.Error ( $"[TargetRpc] {md} must be declared inside a NetworkBehaviour" );
                    }

                    string attributeName = ca.Constructor.DeclaringType.ToString ();

                    switch ( attributeName )
                    {
                        case "Humanlights.Unity.UNet.ServerAttribute":
                            Weaver.Error ( $"[Server] {md} must be declared inside a NetworkBehaviour" );
                            break;
                        case "Humanlights.Unity.UNet.ServerCallbackAttribute":
                            Weaver.Error ( $"[ServerCallback] {md} must be declared inside a NetworkBehaviour" );
                            break;
                        case "Humanlights.Unity.UNet.ClientAttribute":
                            Weaver.Error ( $"[Client] {md} must be declared inside a NetworkBehaviour" );
                            break;
                        case "Humanlights.Unity.UNet.ClientCallbackAttribute":
                            Weaver.Error ( $"[ClientCallback] {md} must be declared inside a NetworkBehaviour" );
                            break;
                    }
                }
            }
        }
    }
}
