using System;
using Mono.CecilX;
using Mono.CecilX.Cil;

namespace Humanlights.Unity.UNet.Weaver
{
    public static class PropertySiteProcessor
    {
        public static void ProcessSitesModule ( ModuleDefinition moduleDef )
        {
            DateTime startTime = DateTime.Now;

            foreach ( TypeDefinition td in moduleDef.Types )
            {
                if ( td.IsClass )
                {
                    ProcessSiteClass ( td );
                }
            }
            if ( Weaver.WeaveLists.generateContainerClass != null )
            {
                moduleDef.Types.Add ( Weaver.WeaveLists.generateContainerClass );
                Weaver.CurrentAssembly.MainModule.ImportReference ( Weaver.WeaveLists.generateContainerClass );

                foreach ( MethodDefinition f in Weaver.WeaveLists.generatedReadFunctions )
                {
                    Weaver.CurrentAssembly.MainModule.ImportReference ( f );
                }

                foreach ( MethodDefinition f in Weaver.WeaveLists.generatedWriteFunctions )
                {
                    Weaver.CurrentAssembly.MainModule.ImportReference ( f );
                }
            }
            Console.WriteLine ( "  ProcessSitesModule " + moduleDef.Name + " elapsed time:" + ( DateTime.Now - startTime ) );
        }

        static void ProcessSiteClass ( TypeDefinition td )
        {
            foreach ( MethodDefinition md in td.Methods )
            {
                ProcessSiteMethod ( td, md );
            }

            foreach ( TypeDefinition nested in td.NestedTypes )
            {
                ProcessSiteClass ( nested );
            }
        }

        static void ProcessSiteMethod ( TypeDefinition td, MethodDefinition md )
        {

            if ( md.Name == ".cctor" ||
                md.Name == NetworkBehaviourProcessor.ProcessedFunctionName ||
                md.Name.StartsWith ( "InvokeSyn" ) )
                return;

            if ( md.Body != null && md.Body.Instructions != null )
            {
                foreach ( CustomAttribute attr in md.CustomAttributes )
                {
                    switch ( attr.Constructor.DeclaringType.ToString () )
                    {
                        case "Humanlights.Unity.UNet.ServerAttribute":
                            InjectServerGuard ( td, md, true );
                            break;
                        case "Humanlights.Unity.UNet.ServerCallbackAttribute":
                            InjectServerGuard ( td, md, false );
                            break;
                        case "Humanlights.Unity.UNet.ClientAttribute":
                            InjectClientGuard ( td, md, true );
                            break;
                        case "Humanlights.Unity.UNet.ClientCallbackAttribute":
                            InjectClientGuard ( td, md, false );
                            break;
                    }
                }

                for ( int iCount = 0; iCount < md.Body.Instructions.Count; )
                {
                    Instruction instr = md.Body.Instructions [ iCount ];
                    iCount += ProcessInstruction ( md, instr, iCount );
                }
            }
        }

        static void InjectServerGuard ( TypeDefinition td, MethodDefinition md, bool logWarning )
        {
            if ( !Weaver.IsNetworkBehaviour ( td ) )
            {
                Weaver.Error ( $"[Server] {md} must be declared in a NetworkBehaviour" );
                return;
            }
            ILProcessor worker = md.Body.GetILProcessor ();
            Instruction top = md.Body.Instructions [ 0 ];

            worker.InsertBefore ( top, worker.Create ( OpCodes.Call, Weaver.NetworkServerGetActive ) );
            worker.InsertBefore ( top, worker.Create ( OpCodes.Brtrue, top ) );
            if ( logWarning )
            {
                worker.InsertBefore ( top, worker.Create ( OpCodes.Ldstr, "[Server] function '" + md.FullName + "' called on client" ) );
                worker.InsertBefore ( top, worker.Create ( OpCodes.Call, Weaver.logWarningReference ) );
            }
            InjectGuardParameters ( md, worker, top );
            InjectGuardReturnValue ( md, worker, top );
            worker.InsertBefore ( top, worker.Create ( OpCodes.Ret ) );
        }

        static void InjectClientGuard ( TypeDefinition td, MethodDefinition md, bool logWarning )
        {
            if ( !Weaver.IsNetworkBehaviour ( td ) )
            {
                Weaver.Error ( $"[Client] {md} must be declared in a NetworkBehaviour" );
                return;
            }
            ILProcessor worker = md.Body.GetILProcessor ();
            Instruction top = md.Body.Instructions [ 0 ];

            worker.InsertBefore ( top, worker.Create ( OpCodes.Call, Weaver.NetworkClientGetActive ) );
            worker.InsertBefore ( top, worker.Create ( OpCodes.Brtrue, top ) );
            if ( logWarning )
            {
                worker.InsertBefore ( top, worker.Create ( OpCodes.Ldstr, "[Client] function '" + md.FullName + "' called on server" ) );
                worker.InsertBefore ( top, worker.Create ( OpCodes.Call, Weaver.logWarningReference ) );
            }

            InjectGuardParameters ( md, worker, top );
            InjectGuardReturnValue ( md, worker, top );
            worker.InsertBefore ( top, worker.Create ( OpCodes.Ret ) );
        }

        static void ProcessInstructionSetterField ( MethodDefinition md, Instruction i, FieldDefinition opField )
        {
            if ( md.Name == ".ctor" )
                return;

            if ( Weaver.WeaveLists.replacementSetterProperties.TryGetValue ( opField, out MethodDefinition replacement ) )
            {
                i.OpCode = OpCodes.Call;
                i.Operand = replacement;
            }
        }

        static void ProcessInstructionGetterField ( MethodDefinition md, Instruction i, FieldDefinition opField )
        {
            if ( md.Name == ".ctor" )
                return;

            if ( Weaver.WeaveLists.replacementGetterProperties.TryGetValue ( opField, out MethodDefinition replacement ) )
            {
                i.OpCode = OpCodes.Call;
                i.Operand = replacement;
            }
        }

        static int ProcessInstruction ( MethodDefinition md, Instruction instr, int iCount )
        {
            if ( instr.OpCode == OpCodes.Call || instr.OpCode == OpCodes.Callvirt )
            {
                if ( instr.Operand is MethodReference opMethod )
                {
                    ProcessInstructionMethod ( md, instr, opMethod, iCount );
                }
            }

            if ( instr.OpCode == OpCodes.Stfld )
            {
                if ( instr.Operand is FieldDefinition opField )
                {
                    ProcessInstructionSetterField ( md, instr, opField );
                }
            }

            if ( instr.OpCode == OpCodes.Ldfld )
            {
                if ( instr.Operand is FieldDefinition opField )
                {
                    ProcessInstructionGetterField ( md, instr, opField );
                }
            }

            if ( instr.OpCode == OpCodes.Ldflda )
            {
                if ( instr.Operand is FieldDefinition opField )
                {
                    return ProcessInstructionLoadAddress ( md, instr, opField, iCount );
                }
            }

            return 1;
        }

        static int ProcessInstructionLoadAddress ( MethodDefinition md, Instruction instr, FieldDefinition opField, int iCount )
        {
            if ( md.Name == ".ctor" )
                return 1;

            if ( Weaver.WeaveLists.replacementSetterProperties.TryGetValue ( opField, out MethodDefinition replacement ) )
            {
                Instruction nextInstr = md.Body.Instructions [ iCount + 1 ];

                if ( nextInstr.OpCode == OpCodes.Initobj )
                {
                    ILProcessor worker = md.Body.GetILProcessor ();
                    VariableDefinition tmpVariable = new VariableDefinition ( opField.FieldType );
                    md.Body.Variables.Add ( tmpVariable );

                    worker.InsertBefore ( instr, worker.Create ( OpCodes.Ldloca, tmpVariable ) );
                    worker.InsertBefore ( instr, worker.Create ( OpCodes.Initobj, opField.FieldType ) );
                    worker.InsertBefore ( instr, worker.Create ( OpCodes.Ldloc, tmpVariable ) );
                    worker.InsertBefore ( instr, worker.Create ( OpCodes.Call, replacement ) );

                    worker.Remove ( instr );
                    worker.Remove ( nextInstr );
                    return 4;

                }

            }

            return 1;
        }

        static void ProcessInstructionMethod ( MethodDefinition md, Instruction instr, MethodReference opMethodRef, int iCount )
        {
            if ( opMethodRef.Name == "Invoke" )
            {

                bool found = false;
                while ( iCount > 0 && !found )
                {
                    iCount -= 1;
                    Instruction inst = md.Body.Instructions [ iCount ];
                    if ( inst.OpCode == OpCodes.Ldfld )
                    {
                        FieldReference opField = inst.Operand as FieldReference;

                        if ( Weaver.WeaveLists.replaceEvents.TryGetValue ( opField.Name, out MethodDefinition replacement ) )
                        {
                            instr.Operand = replacement;
                            inst.OpCode = OpCodes.Nop;
                            found = true;
                        }
                    }
                }
            }
        }


        static void InjectGuardParameters ( MethodDefinition md, ILProcessor worker, Instruction top )
        {
            int offset = md.Resolve ().IsStatic ? 0 : 1;
            for ( int index = 0; index < md.Parameters.Count; index++ )
            {
                ParameterDefinition param = md.Parameters [ index ];
                if ( param.IsOut )
                {
                    TypeReference elementType = param.ParameterType.GetElementType ();
                    if ( elementType.IsPrimitive )
                    {
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Ldarg, index + offset ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Ldc_I4_0 ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Stind_I4 ) );
                    }
                    else
                    {
                        md.Body.Variables.Add ( new VariableDefinition ( elementType ) );
                        md.Body.InitLocals = true;

                        worker.InsertBefore ( top, worker.Create ( OpCodes.Ldarg, index + offset ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Ldloca_S, ( byte ) ( md.Body.Variables.Count - 1 ) ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Initobj, elementType ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Ldloc, md.Body.Variables.Count - 1 ) );
                        worker.InsertBefore ( top, worker.Create ( OpCodes.Stobj, elementType ) );
                    }
                }
            }
        }

        static void InjectGuardReturnValue ( MethodDefinition md, ILProcessor worker, Instruction top )
        {
            if ( md.ReturnType.FullName != Weaver.voidType.FullName )
            {
                if ( md.ReturnType.IsPrimitive )
                {
                    worker.InsertBefore ( top, worker.Create ( OpCodes.Ldc_I4_0 ) );
                }
                else
                {
                    md.Body.Variables.Add ( new VariableDefinition ( md.ReturnType ) );
                    md.Body.InitLocals = true;

                    worker.InsertBefore ( top, worker.Create ( OpCodes.Ldloca_S, ( byte ) ( md.Body.Variables.Count - 1 ) ) );
                    worker.InsertBefore ( top, worker.Create ( OpCodes.Initobj, md.ReturnType ) );
                    worker.InsertBefore ( top, worker.Create ( OpCodes.Ldloc, md.Body.Variables.Count - 1 ) );
                }
            }
        }
    }
}
