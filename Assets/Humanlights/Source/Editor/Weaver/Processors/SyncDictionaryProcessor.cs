using Mono.CecilX;
using Mono.CecilX.Cil;

namespace Humanlights.Unity.UNet.Weaver
{
    static class SyncDictionaryProcessor
    {
        public static void Process(TypeDefinition td)
        {
            SyncObjectProcessor.GenerateSerialization(td, 0, "SerializeKey", "DeserializeKey");
            SyncObjectProcessor.GenerateSerialization(td, 1, "SerializeItem", "DeserializeItem");
        }
    }
}
