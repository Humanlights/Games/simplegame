using System;
using Mono.CecilX;
using UnityEditor.Compilation;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Humanlights.Unity.UNet.Weaver
{
    public static class ReaderWriterProcessor
    {
        public static void ProcessReadersAndWriters(AssemblyDefinition CurrentAssembly)
        {
            Readers.Init();
            Writers.Init();

            foreach (Assembly unityAsm in CompilationPipeline.GetAssemblies())
            {
                if (unityAsm.name != CurrentAssembly.Name.Name)
                {
                    try
                    {
                        using (DefaultAssemblyResolver asmResolver = new DefaultAssemblyResolver())
                        using (AssemblyDefinition assembly = AssemblyDefinition.ReadAssembly(unityAsm.outputPath, new ReaderParameters { ReadWrite = false, ReadSymbols = false, AssemblyResolver = asmResolver }))
                        {
                            ProcessAssemblyClasses(CurrentAssembly, assembly);
                        }
                    }
                    catch(FileNotFoundException)
                    {
                    }
                }
            }

            ProcessAssemblyClasses(CurrentAssembly, CurrentAssembly);
        }

        static void ProcessAssemblyClasses(AssemblyDefinition CurrentAssembly, AssemblyDefinition assembly)
        {
            foreach (TypeDefinition klass in assembly.MainModule.Types)
            {
                if (klass.IsAbstract && klass.IsSealed)
                {
                    LoadWriters(CurrentAssembly, klass);
                    LoadReaders(CurrentAssembly, klass);
                }
            }
        }

        static void LoadWriters(AssemblyDefinition currentAssembly, TypeDefinition klass)
        {
            foreach (MethodDefinition method in klass.Methods)
            {
                if (method.Parameters.Count != 2)
                    continue;

                if (method.Parameters[0].ParameterType.FullName != "Humanlights.Unity.UNet.NetworkWriter")
                    continue;

                if (method.ReturnType.FullName != "System.Void")
                    continue;

                if (method.GetCustomAttribute("System.Runtime.CompilerServices.ExtensionAttribute") == null)
                    continue;

                TypeReference dataType = method.Parameters[1].ParameterType;
                Writers.Register(dataType, currentAssembly.MainModule.ImportReference(method));
            }
        }

        static void LoadReaders(AssemblyDefinition currentAssembly, TypeDefinition klass)
        {
            foreach (MethodDefinition method in klass.Methods)
            {
                if (method.Parameters.Count != 1)
                    continue;

                if (method.Parameters[0].ParameterType.FullName != "Humanlights.Unity.UNet.NetworkReader")
                    continue;

                if (method.ReturnType.FullName == "System.Void")
                    continue;

                if (method.GetCustomAttribute("System.Runtime.CompilerServices.ExtensionAttribute") == null)
                    continue;

                Readers.Register(method.ReturnType, currentAssembly.MainModule.ImportReference(method));
            }
        }
    }
}
