using System;
using System.Linq;
using Mono.CecilX;
using Mono.CecilX.Cil;

namespace Humanlights.Unity.UNet.Weaver
{
    public static class SyncObjectInitializer
    {
        public static void GenerateSyncObjectInitializer(ILProcessor methodWorker, FieldDefinition fd)
        {
            GenerateSyncObjectInstanceInitializer(methodWorker, fd);

            GenerateSyncObjectRegistration(methodWorker, fd);
        }

        static void GenerateSyncObjectInstanceInitializer(ILProcessor ctorWorker, FieldDefinition fd)
        {
            foreach (Instruction ins in ctorWorker.Body.Instructions)
            {
                if (ins.OpCode.Code == Code.Stfld)
                {
                    FieldDefinition field = (FieldDefinition)ins.Operand;
                    if (field.DeclaringType == fd.DeclaringType && field.Name == fd.Name)
                    {
                        return;
                    }
                }
            }

            MethodReference objectConstructor;
            try
            {
                objectConstructor = Weaver.CurrentAssembly.MainModule.ImportReference(fd.FieldType.Resolve().Methods.First<MethodDefinition>(x => x.Name == ".ctor" && !x.HasParameters));
            }
            catch (Exception)
            {
                Weaver.Error($"{fd} does not have a default constructor");
                return;
            }

            ctorWorker.Append(ctorWorker.Create(OpCodes.Ldarg_0));
            ctorWorker.Append(ctorWorker.Create(OpCodes.Newobj, objectConstructor));
            ctorWorker.Append(ctorWorker.Create(OpCodes.Stfld, fd));
        }

        public static bool ImplementsSyncObject(TypeReference typeRef)
        {
            try
            {
                if (typeRef.IsValueType)
                {
                    return false;
                }

                return typeRef.Resolve().ImplementsInterface(Weaver.SyncObjectType);
            }
            catch
            {
            }

            return false;
        }

        /*
            this.InitSyncObject(m_sizes);
        */
        static void GenerateSyncObjectRegistration(ILProcessor methodWorker, FieldDefinition fd)
        {
            methodWorker.Append(methodWorker.Create(OpCodes.Ldarg_0));
            methodWorker.Append(methodWorker.Create(OpCodes.Ldarg_0));
            methodWorker.Append(methodWorker.Create(OpCodes.Ldfld, fd));

            methodWorker.Append(methodWorker.Create(OpCodes.Call, Weaver.InitSyncObjectReference));
        }
    }
}
