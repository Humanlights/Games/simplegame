using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [CustomEditor ( typeof ( NetworkBehaviour ) )]
    [CanEditMultipleObjects]
    public class NetworkBehaviourInspector : UnityEditor.Editor
    {
        bool initialized;
        protected List<string> syncVarNames = new List<string> ();
        bool syncsAnything;
        bool [] showSyncLists;

        readonly GUIContent syncVarIndicatorContent = new GUIContent ( "SyncVar", "This variable has been marked with the [SyncVar] attribute." );

        internal virtual bool HideScriptField => false;

        bool SyncsAnything ( Type scriptClass )
        {
            MethodInfo method = scriptClass.GetMethod ( "OnSerialize" );
            if ( method != null && method.DeclaringType != typeof ( NetworkBehaviour ) )
            {
                return true;
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            foreach ( FieldInfo field in scriptClass.GetFields ( flags ) )
            {
                if ( field.FieldType.BaseType != null &&
                    field.FieldType.BaseType.FullName != null &&
                    field.FieldType.BaseType.FullName.Contains ( "Humanlights.Unity.UNet.Sync" ) )
                {
                    return true;
                }
            }

            return false;
        }

        void Init ( MonoScript script )
        {
            initialized = true;
            Type scriptClass = script.GetClass ();

            foreach ( FieldInfo field in scriptClass.GetFields ( BindingFlags.Public | BindingFlags.Instance ) )
            {
                Attribute [] fieldMarkers = ( Attribute [] ) field.GetCustomAttributes ( typeof ( SyncVarAttribute ), true );
                if ( fieldMarkers.Length > 0 )
                {
                    syncVarNames.Add ( field.Name );
                }
            }

            int numSyncLists = scriptClass.GetFields ().Count (
                field => field.FieldType.BaseType != null &&
                         field.FieldType.BaseType.Name.Contains ( "SyncList" ) );
            if ( numSyncLists > 0 )
            {
                showSyncLists = new bool [ numSyncLists ];
            }

            syncsAnything = SyncsAnything ( scriptClass );
        }

        public override void OnInspectorGUI ()
        {
            if ( !initialized )
            {
                serializedObject.Update ();
                SerializedProperty scriptProperty = serializedObject.FindProperty ( "m_Script" );
                if ( scriptProperty == null )
                    return;

                MonoScript targetScript = scriptProperty.objectReferenceValue as MonoScript;
                Init ( targetScript );
            }

            EditorGUI.BeginChangeCheck ();
            serializedObject.Update ();

            SerializedProperty property = serializedObject.GetIterator ();
            bool expanded = true;
            while ( property.NextVisible ( expanded ) )
            {
                bool isSyncVar = syncVarNames.Contains ( property.name );
                if ( property.propertyType == SerializedPropertyType.ObjectReference )
                {
                    if ( property.name == "m_Script" )
                    {
                        if ( HideScriptField )
                        {
                            continue;
                        }

                        EditorGUI.BeginDisabledGroup ( true );
                    }

                    EditorGUILayout.PropertyField ( property, true );

                    if ( isSyncVar )
                    {
                        GUILayout.Label ( syncVarIndicatorContent, EditorStyles.miniLabel, GUILayout.Width ( EditorStyles.miniLabel.CalcSize ( syncVarIndicatorContent ).x ) );
                    }

                    if ( property.name == "m_Script" )
                    {
                        EditorGUI.EndDisabledGroup ();
                    }
                }
                else
                {
                    EditorGUILayout.BeginHorizontal ();

                    EditorGUILayout.PropertyField ( property, true );

                    if ( isSyncVar )
                    {
                        GUILayout.Label ( syncVarIndicatorContent, EditorStyles.miniLabel, GUILayout.Width ( EditorStyles.miniLabel.CalcSize ( syncVarIndicatorContent ).x ) );
                    }

                    EditorGUILayout.EndHorizontal ();
                }
                expanded = false;
            }
            serializedObject.ApplyModifiedProperties ();
            EditorGUI.EndChangeCheck ();

            int syncListIndex = 0;
            foreach ( FieldInfo field in serializedObject.targetObject.GetType ().GetFields () )
            {
                if ( field.FieldType.BaseType != null && field.FieldType.BaseType.Name.Contains ( "SyncList" ) )
                {
                    showSyncLists [ syncListIndex ] = EditorGUILayout.Foldout ( showSyncLists [ syncListIndex ], "SyncList " + field.Name + "  [" + field.FieldType.Name + "]" );
                    if ( showSyncLists [ syncListIndex ] )
                    {
                        EditorGUI.indentLevel += 1;
                        if ( field.GetValue ( serializedObject.targetObject ) is IEnumerable synclist )
                        {
                            int index = 0;
                            IEnumerator enu = synclist.GetEnumerator ();
                            while ( enu.MoveNext () )
                            {
                                if ( enu.Current != null )
                                {
                                    EditorGUILayout.LabelField ( "Item:" + index, enu.Current.ToString () );
                                }
                                index += 1;
                            }
                        }
                        EditorGUI.indentLevel -= 1;
                    }
                    syncListIndex += 1;
                }
            }

            if ( syncsAnything )
            {
                NetworkBehaviour networkBehaviour = target as NetworkBehaviour;
                if ( networkBehaviour != null )
                {
                    serializedObject.FindProperty ( "syncMode" ).enumValueIndex = ( int ) ( SyncMode )
                        EditorGUILayout.EnumPopup ( "Network Sync Mode", networkBehaviour.syncMode );

                    serializedObject.FindProperty ( "syncInterval" ).floatValue = EditorGUILayout.Slider (
                        new GUIContent ( "Network Sync Interval",
                                       "Time in seconds until next change is synchronized to the client. '0' means send immediately if changed. '0.5' means only send changes every 500ms.\n(This is for state synchronization like SyncVars, SyncLists, OnSerialize. Not for Cmds, Rpcs, etc.)" ),
                        networkBehaviour.syncInterval, 0, 2 );

                    serializedObject.ApplyModifiedProperties ();
                }
            }
        }
    }
}
