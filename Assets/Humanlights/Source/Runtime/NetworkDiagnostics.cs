using System;
using System.Diagnostics;
using UnityEngine.Events;

namespace Humanlights.Unity.UNet
{
    public static class NetworkDiagnostics
    {
        public readonly struct MessageInfo
        {
            public readonly IMessageBase message;
            public readonly int channel;
            public readonly int bytes;
            public readonly int count;

            internal MessageInfo(IMessageBase message, int channel, int bytes, int count)
            {
                this.message = message;
                this.channel = channel;
                this.bytes = bytes;
                this.count = count;
            }
        }

        #region Out messages
        public static event Action<MessageInfo> OutMessageEvent;

        internal static void OnSend<T>(T message, int channel, int bytes, int count) where T : IMessageBase
        {
            if (count > 0 && OutMessageEvent != null)
            {
                MessageInfo outMessage = new MessageInfo(message, channel, bytes, count);
                OutMessageEvent?.Invoke(outMessage);
            }
        }
        #endregion

        #region In messages

        public static event Action<MessageInfo> InMessageEvent;
        
        internal static void OnReceive<T>(T message, int channel, int bytes) where T : IMessageBase
        {
            if (InMessageEvent != null)
            {
                MessageInfo inMessage = new MessageInfo(message, channel, bytes, 1);
                InMessageEvent?.Invoke(inMessage);
            }
        }

        #endregion
    }
}
