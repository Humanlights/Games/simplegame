using System;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace Humanlights.Unity.UNet
{
    public static class NetworkTime
    {
        public static float PingFrequency = 2.0f;

        public static int PingWindowSize = 10;

        static double lastPingTime;


        static readonly Stopwatch stopwatch = new Stopwatch();

        static NetworkTime()
        {
            stopwatch.Start();
        }

        static ExponentialMovingAverage _rtt = new ExponentialMovingAverage(10);
        static ExponentialMovingAverage _offset = new ExponentialMovingAverage(10);

        static double offsetMin = double.MinValue;
        static double offsetMax = double.MaxValue;

        static double LocalTime()
        {
            return stopwatch.Elapsed.TotalSeconds;
        }

        public static void Reset()
        {
            _rtt = new ExponentialMovingAverage(PingWindowSize);
            _offset = new ExponentialMovingAverage(PingWindowSize);
            offsetMin = double.MinValue;
            offsetMax = double.MaxValue;
        }

        internal static void UpdateClient()
        {
            if (Time.time - lastPingTime >= PingFrequency)
            {
                NetworkPingMessage pingMessage = new NetworkPingMessage(LocalTime());
                NetworkClient.Send(pingMessage);
                lastPingTime = Time.time;
            }
        }

        internal static void OnServerPing(NetworkConnection conn, NetworkPingMessage msg)
        {
            if (LogFilter.Debug) Debug.Log("OnPingServerMessage  conn=" + conn);

            NetworkPongMessage pongMsg = new NetworkPongMessage
            {
                clientTime = msg.clientTime,
                serverTime = LocalTime()
            };

            conn.Send(pongMsg);
        }

        internal static void OnClientPong(NetworkConnection _, NetworkPongMessage msg)
        {
            double now = LocalTime();

            double newRtt = now - msg.clientTime;
            _rtt.Add(newRtt);

            double newOffset = now - newRtt * 0.5f - msg.serverTime;

            double newOffsetMin = now - newRtt - msg.serverTime;
            double newOffsetMax = now - msg.serverTime;
            offsetMin = Math.Max(offsetMin, newOffsetMin);
            offsetMax = Math.Min(offsetMax, newOffsetMax);

            if (_offset.Value < offsetMin || _offset.Value > offsetMax)
            {
                _offset = new ExponentialMovingAverage(PingWindowSize);
                _offset.Add(newOffset);
            }
            else if (newOffset >= offsetMin || newOffset <= offsetMax)
            {
                _offset.Add(newOffset);
            }
        }

        public static double time => LocalTime() - _offset.Value;

        public static double timeVar => _offset.Var;

        public static double timeSd => Math.Sqrt(timeVar);

        public static double offset => _offset.Value;

        public static double rtt => _rtt.Value;

        public static double rttVar => _rtt.Var;

        public static double rttSd => Math.Sqrt(rttVar);
    }
}
