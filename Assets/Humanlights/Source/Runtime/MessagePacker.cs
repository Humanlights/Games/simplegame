using System;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public static class MessagePacker
    {
        public static int GetId<T> () where T : IMessageBase
        {
            return typeof ( T ).FullName.GetStableHashCode () & 0xFFFF;
        }

        public static int GetId ( Type type )
        {
            return type.FullName.GetStableHashCode () & 0xFFFF;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use Pack<T> instead" )]
        public static byte [] PackMessage ( int msgType, MessageBase msg )
        {
            NetworkWriter writer = NetworkWriterPool.GetWriter ();
            try
            {
                writer.WriteInt16 ( ( short ) msgType );

                msg.Serialize ( writer );

                return writer.ToArray ();
            }
            finally
            {
                NetworkWriterPool.Recycle ( writer );
            }
        }

        public static void Pack<T> ( T message, NetworkWriter writer ) where T : IMessageBase
        {
            int msgType = GetId ( typeof ( T ).IsValueType ? typeof ( T ) : message.GetType () );
            writer.WriteUInt16 ( ( ushort ) msgType );

            message.Serialize ( writer );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public static byte [] Pack<T> ( T message ) where T : IMessageBase
        {
            NetworkWriter writer = NetworkWriterPool.GetWriter ();

            Pack ( message, writer );
            byte [] data = writer.ToArray ();

            NetworkWriterPool.Recycle ( writer );

            return data;
        }

        public static T Unpack<T> ( byte [] data ) where T : IMessageBase, new()
        {
            NetworkReader reader = new NetworkReader ( data );

            int msgType = GetId<T> ();

            int id = reader.ReadUInt16 ();
            if ( id != msgType )
                throw new FormatException ( "Invalid message,  could not unpack " + typeof ( T ).FullName );

            T message = new T ();
            message.Deserialize ( reader );
            return message;
        }

        public static bool UnpackMessage ( NetworkReader messageReader, out int msgType )
        {
            try
            {
                msgType = messageReader.ReadUInt16 ();
                return true;
            }
            catch ( System.IO.EndOfStreamException )
            {
                msgType = 0;
                return false;
            }
        }

        internal static NetworkMessageDelegate MessageHandler<T> ( Action<NetworkConnection, T> handler, bool requireAuthenication ) where T : IMessageBase, new() => networkMessage =>
           {
               T message = default;
               try
               {
                   if ( requireAuthenication && !networkMessage.conn.isAuthenticated )
                   {
                       Debug.LogWarning ( $"Closing connection: {networkMessage.conn}. Received message {typeof ( T )} that required authentication, but the user has not authenticated yet" );
                       networkMessage.conn.Disconnect ();
                       return;
                   }

                   message = networkMessage.ReadMessage<T> ();
               }
               catch ( Exception exception )
               {
                   Debug.LogError ( "Closed connection: " + networkMessage.conn + ". This can happen if the other side accidentally (or an attacker intentionally) sent invalid data. Reason: " + exception );
                   networkMessage.conn.Disconnect ();
                   return;
               }
               finally
               {
                   NetworkDiagnostics.OnReceive ( message, networkMessage.channelId, networkMessage.reader.Length );
               }

               handler ( networkMessage.conn, message );
           };
    }
}
