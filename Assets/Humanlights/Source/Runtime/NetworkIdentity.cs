using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#if UNITY_2018_3_OR_NEWER
using UnityEditor.Experimental.SceneManagement;
#endif
#endif

namespace Humanlights.Unity.UNet
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkIdentity" )]
    public class NetworkIdentity : MonoBehaviour
    {
        bool m_IsServer;
        NetworkBehaviour [] networkBehavioursCache;

        bool m_Reset;

        public bool isClient { get; internal set; }

        public bool isServer
        {
            get => m_IsServer && NetworkServer.active && netId != 0;
            internal set => m_IsServer = value;
        }

        public bool isLocalPlayer { get; private set; }

        internal bool pendingLocalPlayer { get; set; }

        public bool hasAuthority { get; private set; }

        public Dictionary<int, NetworkConnection> observers;

        public uint netId { get; internal set; }

        public ulong sceneId => m_SceneId;

        [FormerlySerializedAs ( "m_ServerOnly" )]
        public bool serverOnly;

        public NetworkConnectionToClient clientAuthorityOwner { get; internal set; }

        public NetworkConnection connectionToServer { get; internal set; }

        public NetworkConnection connectionToClient { get; internal set; }

        public static readonly Dictionary<uint, NetworkIdentity> spawned = new Dictionary<uint, NetworkIdentity> ();

        public NetworkBehaviour [] NetworkBehaviours => networkBehavioursCache = networkBehavioursCache ?? GetComponents<NetworkBehaviour> ();

        [SerializeField] string m_AssetId;

        public Guid assetId
        {
            get
            {
#if UNITY_EDITOR
                if ( string.IsNullOrEmpty ( m_AssetId ) )
                    SetupIDs ();
#endif
                return string.IsNullOrEmpty ( m_AssetId ) ? Guid.Empty : new Guid ( m_AssetId );
            }
            internal set
            {
                string newAssetIdString = value.ToString ( "N" );
                if ( string.IsNullOrEmpty ( m_AssetId ) || m_AssetId == newAssetIdString )
                {
                    m_AssetId = newAssetIdString;
                }
                else Debug.LogWarning ( "SetDynamicAssetId object already has an assetId <" + m_AssetId + ">" );
            }
        }

#pragma warning disable CS0649
        [SerializeField] ulong m_SceneId;
#pragma warning restore CS0649

        static readonly Dictionary<ulong, NetworkIdentity> sceneIds = new Dictionary<ulong, NetworkIdentity> ();

        public static NetworkIdentity GetSceneIdenity ( ulong id ) => sceneIds [ id ];

        internal void SetClientOwner ( NetworkConnection conn )
        {
            if ( clientAuthorityOwner != null )
            {
                Debug.LogError ( "SetClientOwner m_ClientAuthorityOwner already set!" );
            }
            clientAuthorityOwner = ( NetworkConnectionToClient ) conn;
            clientAuthorityOwner.AddOwnedObject ( this );
        }

        internal void ForceAuthority ( bool authority )
        {
            if ( hasAuthority == authority )
            {
                return;
            }

            hasAuthority = authority;
            if ( authority )
            {
                OnStartAuthority ();
            }
            else
            {
                OnStopAuthority ();
            }
        }

        static uint nextNetworkId = 1;
        internal static uint GetNextNetworkId () => nextNetworkId++;

        public static void ResetNextNetworkId () => nextNetworkId = 1;

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Host Migration was removed" )]
        public delegate void ClientAuthorityCallback ( NetworkConnection conn, NetworkIdentity identity, bool authorityState );

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Host Migration was removed" )]
        public static ClientAuthorityCallback clientAuthorityCallback;

        internal void SetNotLocalPlayer ()
        {
            isLocalPlayer = false;

            if ( NetworkServer.active && NetworkServer.localClientActive )
            {
                return;
            }
            hasAuthority = false;
        }

        internal void RemoveObserverInternal ( NetworkConnection conn )
        {
            observers?.Remove ( conn.connectionId );
        }

        void Awake ()
        {
            if ( Application.isPlaying && sceneId != 0 )
            {
                if ( sceneIds.TryGetValue ( sceneId, out NetworkIdentity existing ) && existing != this )
                {
                    Debug.LogError ( name + "'s sceneId: " + sceneId.ToString ( "X" ) + " is already taken by: " + existing.name + ". Don't call Instantiate for NetworkIdentities that were in the scene since the beginning (aka scene objects). Otherwise the client won't know which object to use for a SpawnSceneObject message." );
                    Destroy ( gameObject );
                }
                else
                {
                    sceneIds [ sceneId ] = this;
                }
            }
        }

        void OnValidate ()
        {
#if UNITY_EDITOR
            SetupIDs ();
#endif
        }

#if UNITY_EDITOR
        void AssignAssetID ( GameObject prefab ) => AssignAssetID ( AssetDatabase.GetAssetPath ( prefab ) );
        void AssignAssetID ( string path ) => m_AssetId = AssetDatabase.AssetPathToGUID ( path );

        bool ThisIsAPrefab () => PrefabUtility.IsPartOfPrefabAsset ( gameObject );

        bool ThisIsASceneObjectWithPrefabParent ( out GameObject prefab )
        {
            prefab = null;

            if ( !PrefabUtility.IsPartOfPrefabInstance ( gameObject ) )
            {
                return false;
            }
            prefab = PrefabUtility.GetCorrespondingObjectFromSource ( gameObject );

            if ( prefab == null )
            {
                Debug.LogError ( "Failed to find prefab parent for scene object [name:" + gameObject.name + "]" );
                return false;
            }
            return true;
        }

        static uint GetRandomUInt ()
        {
            using ( RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider () )
            {
                byte [] bytes = new byte [ 4 ];
                rng.GetBytes ( bytes );
                return BitConverter.ToUInt32 ( bytes, 0 );
            }
        }

        void AssignSceneID ()
        {
            if ( Application.isPlaying )
                return;

            bool duplicate = sceneIds.TryGetValue ( m_SceneId, out NetworkIdentity existing ) && existing != null && existing != this;
            if ( m_SceneId == 0 || duplicate )
            {
                m_SceneId = 0;

                if ( BuildPipeline.isBuildingPlayer )
                    throw new Exception ( "Scene " + gameObject.scene.path + " needs to be opened and resaved before building, because the scene object " + name + " has no valid sceneId yet." );

                Undo.RecordObject ( this, "Generated SceneId" );

                uint randomId = GetRandomUInt ();

                duplicate = sceneIds.TryGetValue ( randomId, out existing ) && existing != null && existing != this;
                if ( !duplicate )
                {
                    m_SceneId = randomId;
                }
            }

            sceneIds [ m_SceneId ] = this;
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public void SetSceneIdSceneHashPartInternal ()
        {
            uint pathHash = ( uint ) gameObject.scene.path.GetStableHashCode ();

            ulong shiftedHash = ( ulong ) pathHash << 32;

            m_SceneId = ( m_SceneId & 0xFFFFFFFF ) | shiftedHash;

            if ( LogFilter.Debug ) Debug.Log ( name + " in scene=" + gameObject.scene.name + " scene index hash(" + pathHash.ToString ( "X" ) + ") copied into sceneId: " + m_SceneId.ToString ( "X" ) );
        }

        void SetupIDs ()
        {
            if ( ThisIsAPrefab () )
            {
                m_SceneId = 0;
                AssignAssetID ( gameObject );
            }
            else if ( PrefabStageUtility.GetCurrentPrefabStage () != null )
            {
                if ( PrefabStageUtility.GetPrefabStage ( gameObject ) != null )
                {
                    m_SceneId = 0;
                    string path = PrefabStageUtility.GetCurrentPrefabStage ().prefabAssetPath;
                    AssignAssetID ( path );
                }
            }
            else if ( ThisIsASceneObjectWithPrefabParent ( out GameObject prefab ) )
            {
                AssignSceneID ();
                AssignAssetID ( prefab );
            }
            else
            {
                AssignSceneID ();
                m_AssetId = "";
            }
        }
#endif

        void OnDestroy ()
        {
            sceneIds.Remove ( sceneId );
            sceneIds.Remove ( sceneId & 0x00000000FFFFFFFF );

            if ( m_IsServer && NetworkServer.active )
            {
                NetworkServer.Destroy ( gameObject );
            }
        }

        internal void OnStartServer ( bool allowNonZeroNetId )
        {
            if ( m_IsServer )
            {
                return;
            }
            m_IsServer = true;

            observers = new Dictionary<int, NetworkConnection> ();

            if ( netId == 0 )
            {
                netId = GetNextNetworkId ();
            }
            else
            {
                if ( !allowNonZeroNetId )
                {
                    Debug.LogError ( "Object has non-zero netId " + netId + " for " + gameObject );
                    return;
                }
            }

            if ( LogFilter.Debug ) Debug.Log ( "OnStartServer " + this + " NetId:" + netId + " SceneId:" + sceneId );

            spawned [ netId ] = this;

            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    comp.OnStartServer ();
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnStartServer:" + e.Message + " " + e.StackTrace );
                }
            }

            if ( NetworkClient.active && NetworkServer.localClientActive )
            {
                OnStartClient ();
            }

            if ( hasAuthority )
            {
                OnStartAuthority ();
            }
        }

        internal void OnStartClient ()
        {
            isClient = true;

            if ( LogFilter.Debug ) Debug.Log ( "OnStartClient " + gameObject + " netId:" + netId );
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    comp.OnStartClient ();
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnStartClient:" + e.Message + " " + e.StackTrace );
                }
            }
        }

        void OnStartAuthority ()
        {
            if ( networkBehavioursCache == null )
            {
                Debug.LogError ( "Network object " + name + " not initialized properly. Do you have more than one NetworkIdentity in the same object? Did you forget to spawn this object with NetworkServer?", this );
                return;
            }

            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    comp.OnStartAuthority ();
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnStartAuthority:" + e.Message + " " + e.StackTrace );
                }
            }
        }

        void OnStopAuthority ()
        {
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    comp.OnStopAuthority ();
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnStopAuthority:" + e.Message + " " + e.StackTrace );
                }
            }
        }

        internal void OnSetLocalVisibility ( bool vis )
        {
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    comp.OnSetLocalVisibility ( vis );
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnSetLocalVisibility:" + e.Message + " " + e.StackTrace );
                }
            }
        }

        internal bool OnCheckObserver ( NetworkConnection conn )
        {
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                try
                {
                    if ( !comp.OnCheckObserver ( conn ) )
                        return false;
                }
                catch ( Exception e )
                {
                    Debug.LogError ( "Exception in OnCheckObserver:" + e.Message + " " + e.StackTrace );
                }
            }
            return true;
        }

        // vis2k: readstring bug prevention: https://issuetracker.unity3d.com/issues/unet-networkwriter-dot-write-causing-readstring-slash-readbytes-out-of-range-errors-in-clients
        bool OnSerializeSafely ( NetworkBehaviour comp, NetworkWriter writer, bool initialState )
        {
            int headerPosition = writer.Position;
            writer.WriteInt32 ( 0 );
            int contentPosition = writer.Position;

            bool result = false;
            try
            {
                result = comp.OnSerialize ( writer, initialState );
            }
            catch ( Exception e )
            {
                Debug.LogError ( "OnSerialize failed for: object=" + name + " component=" + comp.GetType () + " sceneId=" + m_SceneId.ToString ( "X" ) + "\n\n" + e );
            }
            int endPosition = writer.Position;

            writer.Position = headerPosition;
            writer.WriteInt32 ( endPosition - contentPosition );
            writer.Position = endPosition;

            if ( LogFilter.Debug ) Debug.Log ( "OnSerializeSafely written for object=" + comp.name + " component=" + comp.GetType () + " sceneId=" + m_SceneId.ToString ( "X" ) + "header@" + headerPosition + " content@" + contentPosition + " end@" + endPosition + " contentSize=" + ( endPosition - contentPosition ) );

            return result;
        }

        internal void OnSerializeAllSafely ( bool initialState, NetworkWriter ownerWriter, out int ownerWritten, NetworkWriter observersWriter, out int observersWritten )
        {
            ownerWritten = observersWritten = 0;

            if ( NetworkBehaviours.Length > 64 )
            {
                Debug.LogError ( "Only 64 NetworkBehaviour components are allowed for NetworkIdentity: " + name + " because of the dirtyComponentMask" );
                return;
            }
            ulong dirtyComponentsMask = GetDirtyMask ( initialState );

            if ( dirtyComponentsMask == 0L )
                return;

            ulong syncModeObserversMask = GetSyncModeObserversMask ();

            ownerWriter.WritePackedUInt64 ( dirtyComponentsMask );
            observersWriter.WritePackedUInt64 ( dirtyComponentsMask & syncModeObserversMask );

            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                if ( initialState || comp.IsDirty () )
                {
                    if ( LogFilter.Debug ) Debug.Log ( "OnSerializeAllSafely: " + name + " -> " + comp.GetType () + " initial=" + initialState );

                    int startPosition = ownerWriter.Position;
                    OnSerializeSafely ( comp, ownerWriter, initialState );
                    ++ownerWritten;

                    if ( comp.syncMode == SyncMode.Observers )
                    {
                        ArraySegment<byte> segment = ownerWriter.ToArraySegment ();
                        int length = ownerWriter.Position - startPosition;
                        observersWriter.WriteBytes ( segment.Array, startPosition, length );
                        ++observersWritten;
                    }
                }
            }
        }

        internal ulong GetDirtyMask ( bool initialState )
        {
            ulong dirtyComponentsMask = 0L;
            NetworkBehaviour [] components = NetworkBehaviours;
            for ( int i = 0; i < components.Length; ++i )
            {
                NetworkBehaviour comp = components [ i ];
                if ( initialState || comp.IsDirty () )
                {
                    dirtyComponentsMask |= ( ulong ) ( 1L << i );
                }
            }

            return dirtyComponentsMask;
        }

        internal ulong GetSyncModeObserversMask ()
        {
            ulong mask = 0UL;
            NetworkBehaviour [] components = NetworkBehaviours;
            for ( int i = 0; i < NetworkBehaviours.Length; ++i )
            {
                NetworkBehaviour comp = components [ i ];
                if ( comp.syncMode == SyncMode.Observers )
                {
                    mask |= 1UL << i;
                }
            }

            return mask;
        }

        void OnDeserializeSafely ( NetworkBehaviour comp, NetworkReader reader, bool initialState )
        {
            int contentSize = reader.ReadInt32 ();
            int chunkStart = reader.Position;
            int chunkEnd = reader.Position + contentSize;

            try
            {
                if ( LogFilter.Debug ) Debug.Log ( "OnDeserializeSafely: " + comp.name + " component=" + comp.GetType () + " sceneId=" + m_SceneId.ToString ( "X" ) + " length=" + contentSize );
                comp.OnDeserialize ( reader, initialState );
            }
            catch ( Exception e )
            {
                Debug.LogError ( "OnDeserialize failed for: object=" + name + " component=" + comp.GetType () + " sceneId=" + m_SceneId.ToString ( "X" ) + " length=" + contentSize + ". Possible Reasons:\n  * Do " + comp.GetType () + "'s OnSerialize and OnDeserialize calls write the same amount of data(" + contentSize + " bytes)? \n  * Was there an exception in " + comp.GetType () + "'s OnSerialize/OnDeserialize code?\n  * Are the server and client the exact same project?\n  * Maybe this OnDeserialize call was meant for another GameObject? The sceneIds can easily get out of sync if the Hierarchy was modified only in the client OR the server. Try rebuilding both.\n\n" + e );
            }

            if ( reader.Position != chunkEnd )
            {
                int bytesRead = reader.Position - chunkStart;
                Debug.LogWarning ( "OnDeserialize was expected to read " + contentSize + " instead of " + bytesRead + " bytes for object:" + name + " component=" + comp.GetType () + " sceneId=" + m_SceneId.ToString ( "X" ) + ". Make sure that OnSerialize and OnDeserialize write/read the same amount of data in all cases." );

                reader.Position = chunkEnd;
            }
        }

        internal void OnDeserializeAllSafely ( NetworkReader reader, bool initialState )
        {
            ulong dirtyComponentsMask = reader.ReadPackedUInt64 ();

            NetworkBehaviour [] components = NetworkBehaviours;
            for ( int i = 0; i < components.Length; ++i )
            {
                ulong dirtyBit = ( ulong ) ( 1L << i );
                if ( ( dirtyComponentsMask & dirtyBit ) != 0L )
                {
                    OnDeserializeSafely ( components [ i ], reader, initialState );
                }
            }
        }

        void HandleRemoteCall ( int componentIndex, int functionHash, MirrorInvokeType invokeType, NetworkReader reader )
        {
            if ( gameObject == null )
            {
                Debug.LogWarning ( invokeType + " [" + functionHash + "] received for deleted object [netId=" + netId + "]" );
                return;
            }

            if ( 0 <= componentIndex && componentIndex < networkBehavioursCache.Length )
            {
                NetworkBehaviour invokeComponent = networkBehavioursCache [ componentIndex ];
                if ( !invokeComponent.InvokeHandlerDelegate ( functionHash, invokeType, reader ) )
                {
                    Debug.LogError ( "Found no receiver for incoming " + invokeType + " [" + functionHash + "] on " + gameObject + ",  the server and client should have the same NetworkBehaviour instances [netId=" + netId + "]." );
                }
            }
            else
            {
                Debug.LogWarning ( "Component [" + componentIndex + "] not found for [netId=" + netId + "]" );
            }
        }

        internal void HandleSyncEvent ( int componentIndex, int eventHash, NetworkReader reader )
        {
            HandleRemoteCall ( componentIndex, eventHash, MirrorInvokeType.SyncEvent, reader );
        }

        internal void HandleCommand ( int componentIndex, int cmdHash, NetworkReader reader )
        {
            HandleRemoteCall ( componentIndex, cmdHash, MirrorInvokeType.Command, reader );
        }

        internal void HandleRPC ( int componentIndex, int rpcHash, NetworkReader reader )
        {
            HandleRemoteCall ( componentIndex, rpcHash, MirrorInvokeType.ClientRpc, reader );
        }

        internal void OnUpdateVars ( NetworkReader reader, bool initialState )
        {
            OnDeserializeAllSafely ( reader, initialState );
        }

        internal void SetLocalPlayer ()
        {
            isLocalPlayer = true;

            bool originAuthority = hasAuthority;
            hasAuthority = true;

            foreach ( NetworkBehaviour comp in networkBehavioursCache )
            {
                comp.OnStartLocalPlayer ();

                if ( !originAuthority )
                {
                    comp.OnStartAuthority ();
                }
            }
        }

        internal void OnNetworkDestroy ()
        {
            for ( int i = 0; networkBehavioursCache != null && i < networkBehavioursCache.Length; i++ )
            {
                NetworkBehaviour comp = networkBehavioursCache [ i ];
                comp.OnNetworkDestroy ();
            }
            m_IsServer = false;
        }

        internal void ClearObservers ()
        {
            if ( observers != null )
            {
                foreach ( NetworkConnection conn in observers.Values )
                {
                    conn.RemoveFromVisList ( this, true );
                }
                observers.Clear ();
            }
        }

        internal void AddObserver ( NetworkConnection conn )
        {
            if ( observers == null )
            {
                Debug.LogError ( "AddObserver for " + gameObject + " observer list is null" );
                return;
            }

            if ( observers.ContainsKey ( conn.connectionId ) )
            {
                return;
            }

            if ( LogFilter.Debug ) Debug.Log ( "Added observer " + conn.address + " added for " + gameObject );

            observers [ conn.connectionId ] = conn;
            conn.AddToVisList ( this );
        }

        static readonly HashSet<NetworkConnection> newObservers = new HashSet<NetworkConnection> ();

        public void RebuildObservers ( bool initialize )
        {
            if ( observers == null )
                return;

            bool changed = false;
            bool result = false;

            newObservers.Clear ();

            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                result |= comp.OnRebuildObservers ( newObservers, initialize );
            }

            if ( connectionToClient != null && connectionToClient.isReady )
            {
                newObservers.Add ( connectionToClient );
            }

            if ( !result )
            {
                if ( initialize )
                {
                    foreach ( NetworkConnection conn in NetworkServer.connections.Values )
                    {
                        if ( conn.isReady )
                            AddObserver ( conn );
                    }

                    if ( NetworkServer.localConnection != null && NetworkServer.localConnection.isReady )
                    {
                        AddObserver ( NetworkServer.localConnection );
                    }
                }
                return;
            }

            foreach ( NetworkConnection conn in newObservers )
            {
                if ( conn == null )
                {
                    continue;
                }

                if ( !conn.isReady )
                {
                    if ( LogFilter.Debug ) Debug.Log ( "Observer is not ready for " + gameObject + " " + conn );
                    continue;
                }

                if ( initialize || !observers.ContainsKey ( conn.connectionId ) )
                {
                    conn.AddToVisList ( this );
                    if ( LogFilter.Debug ) Debug.Log ( "New Observer for " + gameObject + " " + conn );
                    changed = true;
                }
            }

            foreach ( NetworkConnection conn in observers.Values )
            {
                if ( !newObservers.Contains ( conn ) )
                {
                    conn.RemoveFromVisList ( this, false );
                    if ( LogFilter.Debug ) Debug.Log ( "Removed Observer for " + gameObject + " " + conn );
                    changed = true;
                }
            }

            if ( initialize )
            {
                if ( !newObservers.Contains ( NetworkServer.localConnection ) )
                {
                    OnSetLocalVisibility ( false );
                }
            }

            if ( changed )
            {
                observers.Clear ();
                foreach ( NetworkConnection conn in newObservers )
                {
                    if ( conn.isReady )
                        observers.Add ( conn.connectionId, conn );
                }
            }
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "NetworkConnection parameter is no longer needed and nothing is returned" )]
        public bool RemoveClientAuthority ( NetworkConnection conn )
        {
            RemoveClientAuthority ();
            return true;
        }

        public void RemoveClientAuthority ()
        {
            if ( !isServer )
            {
                Debug.LogError ( "RemoveClientAuthority can only be call on the server for spawned objects." );
                return;
            }

            if ( connectionToClient != null )
            {
                Debug.LogError ( "RemoveClientAuthority cannot remove authority for a player object" );
                return;
            }

            if ( clientAuthorityOwner != null )
            {
                ClientAuthorityMessage msg = new ClientAuthorityMessage
                {
                    netId = netId,
                    authority = false
                };

                clientAuthorityOwner.Send ( msg );
#pragma warning disable CS0618 
                clientAuthorityCallback?.Invoke ( clientAuthorityOwner, this, false );
#pragma warning restore CS0618 

                clientAuthorityOwner.RemoveOwnedObject ( this );
                clientAuthorityOwner = null;
            }

            ForceAuthority ( false );
        }

        public bool AssignClientAuthority ( NetworkConnection conn )
        {
            if ( !isServer )
            {
                Debug.LogError ( "AssignClientAuthority can only be called on the server for spawned objects." );
                return false;
            }

            if ( clientAuthorityOwner != null && conn != clientAuthorityOwner )
            {
                Debug.LogError ( "AssignClientAuthority for " + gameObject + " already has an owner. Use RemoveClientAuthority() first." );
                return false;
            }

            if ( conn == null )
            {
                Debug.LogError ( "AssignClientAuthority for " + gameObject + " owner cannot be null. Use RemoveClientAuthority() instead." );
                return false;
            }

            clientAuthorityOwner = ( NetworkConnectionToClient ) conn;
            clientAuthorityOwner.AddOwnedObject ( this );

            ForceAuthority ( false );

            ClientAuthorityMessage msg = new ClientAuthorityMessage
            {
                netId = netId,
                authority = true
            };
            conn.Send ( msg );

#pragma warning disable CS0618 
            clientAuthorityCallback?.Invoke ( conn, this, true );
#pragma warning restore CS0618 
            return true;
        }

        internal void MarkForReset () => m_Reset = true;

        internal void Reset ()
        {
            if ( !m_Reset )
                return;

            m_Reset = false;
            m_IsServer = false;
            isClient = false;
            hasAuthority = false;

            netId = 0;
            isLocalPlayer = false;
            connectionToServer = null;
            connectionToClient = null;
            networkBehavioursCache = null;

            ClearObservers ();
            clientAuthorityOwner = null;
        }

        static UpdateVarsMessage varsMessage = new UpdateVarsMessage ();

        internal void MirrorUpdate ()
        {
            if ( observers != null && observers.Count > 0 )
            {
                NetworkWriter ownerWriter = NetworkWriterPool.GetWriter ();
                NetworkWriter observersWriter = NetworkWriterPool.GetWriter ();

                OnSerializeAllSafely ( false, ownerWriter, out int ownerWritten, observersWriter, out int observersWritten );
                if ( ownerWritten > 0 || observersWritten > 0 )
                {
                    varsMessage.netId = netId;

                    if ( ownerWritten > 0 )
                    {
                        varsMessage.payload = ownerWriter.ToArraySegment ();
                        if ( connectionToClient != null && connectionToClient.isReady )
                            NetworkServer.SendToClientOfPlayer ( this, varsMessage );
                    }

                    if ( observersWritten > 0 )
                    {
                        varsMessage.payload = observersWriter.ToArraySegment ();
                        NetworkServer.SendToReady ( this, varsMessage, false );
                    }

                    ClearDirtyComponentsDirtyBits ();
                }
                NetworkWriterPool.Recycle ( ownerWriter );
                NetworkWriterPool.Recycle ( observersWriter );
            }
            else
            {
                ClearAllComponentsDirtyBits ();
            }
        }

        internal void ClearAllComponentsDirtyBits ()
        {
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                comp.ClearAllDirtyBits ();
            }
        }

        internal void ClearDirtyComponentsDirtyBits ()
        {
            foreach ( NetworkBehaviour comp in NetworkBehaviours )
            {
                if ( comp.IsDirty () )
                {
                    comp.ClearAllDirtyBits ();
                }
            }
        }
    }
}
