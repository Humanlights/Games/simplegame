using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Humanlights.Unity.UNet
{
    public enum PlayerSpawnMethod
    {
        Random,
        RoundRobin
    }

    [AddComponentMenu ( "Network/NetworkManager" )]
    public class NetworkManager : MonoBehaviour
    {
        [Header ( "Configuration" )]
        [FormerlySerializedAs ( "m_DontDestroyOnLoad" )]
        public bool dontDestroyOnLoad = true;

        [FormerlySerializedAs ( "m_RunInBackground" )]
        public bool runInBackground = true;

        public bool startOnHeadless = true;

        [Tooltip ( "Server Update frequency, per second. Use around 60Hz for fast paced games like Counter-Strike to minimize latency. Use around 30Hz for games like WoW to minimize computations. Use around 1-10Hz for slow paced games like EVE." )]
        public int serverTickRate = 30;

        [FormerlySerializedAs ( "m_ShowDebugMessages" )]
        public bool showDebugMessages;

        [Scene]
        [FormerlySerializedAs ( "m_OfflineScene" )]
        public string offlineScene = "";

        [Scene]
        [FormerlySerializedAs ( "m_OnlineScene" )]
        public string onlineScene = "";

        [Header ( "Network Info" )]

        [SerializeField]
        public Transport transport;

        [FormerlySerializedAs ( "m_NetworkAddress" )]
        public string networkAddress = "localhost";

        [FormerlySerializedAs ( "m_MaxConnections" )]
        public int maxConnections = 4;

        public int numPlayers => NetworkServer.connections.Count ( kv => kv.Value.identity != null );

        [Header ( "Authentication" )]

        public NetworkAuthenticator authenticator;

        [Header ( "Spawn Info" )]
        [FormerlySerializedAs ( "m_PlayerPrefab" )]
        public GameObject playerPrefab;

        [FormerlySerializedAs ( "m_AutoCreatePlayer" )]
        public bool autoCreatePlayer = true;

        [FormerlySerializedAs ( "m_PlayerSpawnMethod" )]
        public PlayerSpawnMethod playerSpawnMethod;

        [FormerlySerializedAs ( "m_SpawnPrefabs" ), HideInInspector]
        public List<GameObject> spawnPrefabs = new List<GameObject> ();

        public static NetworkManager singleton;

        [NonSerialized]
        public bool isNetworkActive;

        static NetworkConnection clientReadyConnection;

        [NonSerialized]
        public bool clientLoadedScene;

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient.isConnected instead" )]
        public bool IsClientConnected ()
        {
            return NetworkClient.isConnected;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use isHeadless instead of IsHeadless()" )]
        public static bool IsHeadless ()
        {
            return isHeadless;
        }

        public static bool isHeadless => SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null;

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient directly, it will be made static soon. For example, use NetworkClient.Send(message) instead of NetworkManager.client.Send(message)" )]
        public NetworkClient client => NetworkClient.singleton;

        #region Callback

        public Action OnStartHostCallback { get; set; }
        public Action OnStopHostCallback { get; set; }

        public Action OnStartServerCallback { get; set; }
        public Action OnStopServerCallback { get; set; }

        public Action OnStartClientCallback { get; set; }
        public Action OnStopClientCallback { get; set; }

        public Action<NetworkConnection> OnServerConnectCallback { get; set; }
        public Action<NetworkConnection> OnServerDisconnectCallback { get; set; }
        public Action<NetworkConnection> OnServerReadyCallback { get; set; }
        public Action<NetworkConnection, NetworkIdentity> OnServerAddPlayerCallback { get; set; }
        public Action<NetworkConnection, NetworkIdentity> OnServerRemovePlayerCallback { get; set; }
        public Action<NetworkConnection, int> OnServerErrorCallback { get; set; }
        public Action<string> OnServerChangeSceneCallback { get; set; }
        public Action<string> OnServerSceneChangedCallback { get; set; }

        public Action<NetworkConnection> OnClientConnectCallback { get; set; }
        public Action<NetworkConnection> OnClientDisconnectCallback { get; set; }
        public Action<NetworkConnection, int> OnClientErrorCallback { get; set; }
        public Action<string, SceneOperation> OnClientChangeSceneCallback { get; set; }
        public Action<NetworkConnection> OnClientSceneChangedCallback { get; set; }

        #endregion

        #region Unity Callbacks

        public virtual void OnValidate ()
        {
            if ( transport == null )
            {
                transport = GetComponent<Transport> ();
                if ( transport == null )
                {
                    transport = gameObject.AddComponent<TelepathyTransport> ();
                    // Debug.Log ( "NetworkManager: added default Transport because there was none yet." );
                }
#if UNITY_EDITOR
                UnityEditor.Undo.RecordObject ( gameObject, "Added default Transport" );
#endif
            }

            maxConnections = Mathf.Max ( maxConnections, 0 );

            if ( playerPrefab != null && playerPrefab.GetComponent<NetworkIdentity> () == null )
            {
                // Debug.LogError ( "NetworkManager - playerPrefab must have a NetworkIdentity." );
                playerPrefab = null;
            }
        }

        public virtual void Awake ()
        {
            networkSceneName = offlineScene;

            InitializeSingleton ();

            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public virtual void Start ()
        {
            if ( isHeadless && startOnHeadless )
            {
                StartServer ();
            }
        }

        public virtual void LateUpdate ()
        {
            NetworkServer.Update ();
            NetworkClient.Update ();
            UpdateScene ();
        }

        #endregion

        #region Start & Stop

        public bool StartServer ()
        {
            InitializeSingleton ();

            if ( runInBackground )
                Application.runInBackground = true;

            if ( authenticator != null )
            {
                authenticator.OnStartServer ();
                authenticator.OnServerAuthenticated.AddListener ( OnServerAuthenticated );
            }

            ConfigureServerFrameRate ();

            if ( !NetworkServer.Listen ( maxConnections ) )
            {
                Debug.LogError ( "StartServer listen failed." );
                return false;
            }

            OnStartServer ();

            RegisterServerMessages ();

            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager StartServer" );
            isNetworkActive = true;

            string loadedSceneName = SceneManager.GetActiveScene ().name;
            if ( !string.IsNullOrEmpty ( onlineScene ) && onlineScene != loadedSceneName && onlineScene != offlineScene )
            {
                ServerChangeScene ( onlineScene );
            }
            else
            {
                NetworkServer.SpawnObjects ();
            }
            return true;
        }

        public void StartClient ()
        {
            InitializeSingleton ();

            if ( authenticator != null )
            {
                authenticator.OnStartClient ();
                authenticator.OnClientAuthenticated.AddListener ( OnClientAuthenticated );
            }

            if ( runInBackground )
                Application.runInBackground = true;

            isNetworkActive = true;

            RegisterClientMessages ();

            if ( string.IsNullOrEmpty ( networkAddress ) )
            {
                Debug.LogError ( "Must set the Network Address field in the manager" );
                return;
            }
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager StartClient address:" + networkAddress );

            NetworkClient.Connect ( networkAddress );

            OnStartClient ();
        }

        public virtual void StartHost ()
        {
            OnStartHost ();
            if ( StartServer () )
            {
                ConnectLocalClient ();
                OnStartClient ();
            }
        }

        public void StopHost ()
        {
            OnStopHost ();

            StopServer ();
            StopClient ();
        }

        public void StopServer ()
        {
            if ( !NetworkServer.active )
                return;

            if ( authenticator != null )
                authenticator.OnServerAuthenticated.RemoveListener ( OnServerAuthenticated );

            OnStopServer ();

            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager StopServer" );
            isNetworkActive = false;
            NetworkServer.Shutdown ();
            if ( !string.IsNullOrEmpty ( offlineScene ) )
            {
                ServerChangeScene ( offlineScene );
            }
            CleanupNetworkIdentities ();

            startPositionIndex = 0;
        }

        public void StopClient ()
        {
            if ( authenticator != null )
                authenticator.OnClientAuthenticated.RemoveListener ( OnClientAuthenticated );

            OnStopClient ();

            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager StopClient" );
            isNetworkActive = false;

            NetworkClient.Disconnect ();
            NetworkClient.Shutdown ();

            if ( !string.IsNullOrEmpty ( offlineScene ) && SceneManager.GetActiveScene ().name != offlineScene )
            {
                ClientChangeScene ( offlineScene, SceneOperation.Normal );
            }

            CleanupNetworkIdentities ();
        }

        public virtual void OnApplicationQuit ()
        {
            if ( NetworkClient.isConnected )
            {
                StopClient ();
                // print ( "OnApplicationQuit: stopped client" );
            }

            if ( NetworkServer.active )
            {
                StopServer ();
                // print ( "OnApplicationQuit: stopped server" );
            }

            Transport.activeTransport.Shutdown ();
        }

        public virtual void ConfigureServerFrameRate ()
        {
#if !UNITY_EDITOR
            if (!NetworkClient.active && isHeadless)
            {
                Application.targetFrameRate = serverTickRate;
                Debug.Log("Server Tick Rate set to: " + Application.targetFrameRate + " Hz.");
            }
#endif
        }

        void InitializeSingleton ()
        {
            if ( singleton != null && singleton == this )
            {
                return;
            }

            LogFilter.Debug = showDebugMessages;

            if ( dontDestroyOnLoad )
            {
                if ( singleton != null )
                {
                    Debug.LogWarning ( "Multiple NetworkManagers detected in the scene. Only one NetworkManager can exist at a time. The duplicate NetworkManager will be destroyed." );
                    Destroy ( gameObject );
                    return;
                }
                if ( LogFilter.Debug ) Debug.Log ( "NetworkManager created singleton (DontDestroyOnLoad)" );
                singleton = this;
                if ( Application.isPlaying ) DontDestroyOnLoad ( gameObject );
            }
            else
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkManager created singleton (ForScene)" );
                singleton = this;
            }

            Transport.activeTransport = transport;
        }

        void RegisterServerMessages ()
        {
            NetworkServer.RegisterHandler<ConnectMessage> ( OnServerConnectInternal, false );
            NetworkServer.RegisterHandler<DisconnectMessage> ( OnServerDisconnectInternal, false );
            NetworkServer.RegisterHandler<ReadyMessage> ( OnServerReadyMessageInternal );
            NetworkServer.RegisterHandler<AddPlayerMessage> ( OnServerAddPlayerInternal );
            NetworkServer.RegisterHandler<RemovePlayerMessage> ( OnServerRemovePlayerMessageInternal );
            NetworkServer.RegisterHandler<ErrorMessage> ( OnServerErrorInternal, false );
        }

        void ConnectLocalClient ()
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager StartHost" );

            if ( authenticator != null )
            {
                authenticator.OnStartClient ();
                authenticator.OnClientAuthenticated.AddListener ( OnClientAuthenticated );
            }

            networkAddress = "localhost";
            NetworkServer.ActivateLocalClientScene ();
            NetworkClient.ConnectLocalServer ();
            RegisterClientMessages ();
        }

        void RegisterClientMessages ()
        {
            NetworkClient.RegisterHandler<ConnectMessage> ( OnClientConnectInternal, false );
            NetworkClient.RegisterHandler<DisconnectMessage> ( OnClientDisconnectInternal, false );
            NetworkClient.RegisterHandler<NotReadyMessage> ( OnClientNotReadyMessageInternal );
            NetworkClient.RegisterHandler<ErrorMessage> ( OnClientErrorInternal, false );
            NetworkClient.RegisterHandler<SceneMessage> ( OnClientSceneInternal, false );

            if ( playerPrefab != null )
            {
                ClientScene.RegisterPrefab ( playerPrefab );
            }
            for ( int i = 0; i < spawnPrefabs.Count; i++ )
            {
                GameObject prefab = spawnPrefabs [ i ];
                if ( prefab != null )
                {
                    ClientScene.RegisterPrefab ( prefab );
                }
            }
        }

        void CleanupNetworkIdentities ()
        {
            foreach ( NetworkIdentity identity in Resources.FindObjectsOfTypeAll<NetworkIdentity> () )
            {
                identity.MarkForReset ();
            }
        }

        public static void Shutdown ()
        {
            if ( singleton == null )
                return;

            singleton.startPositions.Clear ();
            startPositionIndex = 0;
            clientReadyConnection = null;

            singleton.StopHost ();
            singleton = null;
        }

        public virtual void OnDestroy ()
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager destroyed" );
        }

        #endregion

        #region Scene Management

        public static string networkSceneName = "";

        static UnityEngine.AsyncOperation loadingSceneAsync;

        public virtual void ServerChangeScene ( string newSceneName )
        {
            ServerChangeScene ( newSceneName, LoadSceneMode.Single, LocalPhysicsMode.None );
        }

        public virtual void ServerChangeScene ( string newSceneName, LoadSceneMode sceneMode, LocalPhysicsMode physicsMode )
        {
            if ( string.IsNullOrEmpty ( newSceneName ) )
            {
                Debug.LogError ( "ServerChangeScene empty scene name" );
                return;
            }

            if ( LogFilter.Debug ) Debug.Log ( "ServerChangeScene " + newSceneName );
            NetworkServer.SetAllClientsNotReady ();
            networkSceneName = newSceneName;

            OnServerChangeScene ( newSceneName );

            loadingSceneAsync = SceneManager.LoadSceneAsync ( newSceneName );

            SceneMessage msg = new SceneMessage ()
            {
                sceneName = newSceneName,
            };

            NetworkServer.SendToAll ( msg );

            Transport.activeTransport.enabled = false;

            startPositionIndex = 0;
            startPositions.Clear ();
        }

        internal void ClientChangeScene ( string newSceneName, SceneOperation sceneOperation = SceneOperation.Normal )
        {
            if ( string.IsNullOrEmpty ( newSceneName ) )
            {
                Debug.LogError ( "ClientChangeScene empty scene name" );
                return;
            }

            if ( LogFilter.Debug ) Debug.Log ( "ClientChangeScene newSceneName:" + newSceneName + " networkSceneName:" + networkSceneName );

            if ( LogFilter.Debug ) Debug.Log ( "ClientChangeScene: pausing handlers while scene is loading to avoid data loss after scene was loaded." );
            Transport.activeTransport.enabled = false;

            OnClientChangeScene ( newSceneName, sceneOperation );

            switch ( sceneOperation )
            {
                case SceneOperation.Normal:
                    loadingSceneAsync = SceneManager.LoadSceneAsync ( newSceneName );
                    break;
                case SceneOperation.LoadAdditive:
                    if ( !SceneManager.GetSceneByName ( newSceneName ).IsValid () )
                        loadingSceneAsync = SceneManager.LoadSceneAsync ( newSceneName, LoadSceneMode.Additive );
                    else
                        Debug.LogWarningFormat ( "Scene {0} is already loaded", newSceneName );
                    break;
                case SceneOperation.UnloadAdditive:
                    if ( SceneManager.GetSceneByName ( newSceneName ).IsValid () )
                    {
                        if ( SceneManager.GetSceneByName ( newSceneName ) != null )
                            loadingSceneAsync = SceneManager.UnloadSceneAsync ( newSceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects );
                    }
                    else
                        Debug.LogWarning ( "Cannot unload the active scene with UnloadAdditive operation" );
                    break;
            }

            if ( sceneOperation == SceneOperation.Normal )
                networkSceneName = newSceneName;
        }

        void OnSceneLoaded ( Scene scene, LoadSceneMode mode )
        {
            if ( mode == LoadSceneMode.Additive )
            {
                if ( NetworkServer.active )
                {
                    NetworkServer.SpawnObjects ();
                    Debug.Log ( "Respawned Server objects after additive scene load: " + scene.name );
                }
                if ( NetworkClient.active )
                {
                    ClientScene.PrepareToSpawnSceneObjects ();
                    Debug.Log ( "Rebuild Client spawnableObjects after additive scene load: " + scene.name );
                }
            }
        }

        static void UpdateScene ()
        {
            if ( singleton != null && loadingSceneAsync != null && loadingSceneAsync.isDone )
            {
                if ( LogFilter.Debug ) Debug.Log ( "ClientChangeScene done readyCon:" + clientReadyConnection );
                singleton.FinishLoadScene ();
                loadingSceneAsync.allowSceneActivation = true;
                loadingSceneAsync = null;
            }
        }

        void FinishLoadScene ()
        {

            if ( LogFilter.Debug ) Debug.Log ( "FinishLoadScene: resuming handlers after scene was loading." );
            Transport.activeTransport.enabled = true;

            if ( clientReadyConnection != null )
            {
                clientLoadedScene = true;
                OnClientConnect ( clientReadyConnection );
                clientReadyConnection = null;
            }

            if ( NetworkServer.active )
            {
                NetworkServer.SpawnObjects ();
                OnServerSceneChanged ( networkSceneName );
            }

            if ( NetworkClient.isConnected )
            {
                RegisterClientMessages ();
                OnClientSceneChanged ( NetworkClient.connection );
            }
        }

        #endregion

        #region Start Positions

        static int startPositionIndex;

        public List<Transform> startPositions = new List<Transform> ();

        public void RegisterStartPosition ( Transform start )
        {
            if ( LogFilter.Debug ) Debug.Log ( "RegisterStartPosition: (" + start.gameObject.name + ") " + start.position );
            startPositions.Add ( start );

            startPositions = startPositions.OrderBy ( transform => transform.GetSiblingIndex () ).ToList ();
        }

        public void UnRegisterStartPosition ( Transform start )
        {
            if ( LogFilter.Debug ) Debug.Log ( "UnRegisterStartPosition: (" + start.gameObject.name + ") " + start.position );
            startPositions.Remove ( start );
        }

        #endregion

        #region Server Internal Message Handlers

        void OnServerConnectInternal ( NetworkConnection conn, ConnectMessage connectMsg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerConnectInternal" );

            if ( authenticator != null )
            {
                authenticator.OnServerAuthenticateInternal ( conn );
            }
            else
            {
                OnServerAuthenticated ( conn );
            }
        }

        void OnServerAuthenticated ( NetworkConnection conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerAuthenticated" );

            conn.isAuthenticated = true;

            if ( networkSceneName != "" && networkSceneName != offlineScene )
            {
                SceneMessage msg = new SceneMessage () { sceneName = networkSceneName };
                conn.Send ( msg );
            }

            OnServerConnect ( conn );
        }

        void OnServerDisconnectInternal ( NetworkConnection conn, DisconnectMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerDisconnectInternal" );
            OnServerDisconnect ( conn );
        }

        void OnServerReadyMessageInternal ( NetworkConnection conn, ReadyMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerReadyMessageInternal" );
            OnServerReady ( conn );
        }

        void OnServerAddPlayerInternal ( NetworkConnection conn, AddPlayerMessage extraMessage )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerAddPlayer" );

            if ( autoCreatePlayer && playerPrefab == null )
            {
                Debug.LogError ( "The PlayerPrefab is empty on the NetworkManager. Please setup a PlayerPrefab object." );
                return;
            }

            if ( autoCreatePlayer && playerPrefab.GetComponent<NetworkIdentity> () == null )
            {
                Debug.LogError ( "The PlayerPrefab does not have a NetworkIdentity. Please add a NetworkIdentity to the player prefab." );
                return;
            }

            if ( conn.identity != null )
            {
                Debug.LogError ( "There is already a player for this connection." );
                return;
            }

#pragma warning disable CS0618 
            OnServerAddPlayer ( conn, extraMessage );
#pragma warning restore CS0618 
        }

        void OnServerRemovePlayerMessageInternal ( NetworkConnection conn, RemovePlayerMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerRemovePlayerMessageInternal" );

            if ( conn.identity != null )
            {
                OnServerRemovePlayer ( conn, conn.identity );
                conn.identity = null;
            }
        }

        void OnServerErrorInternal ( NetworkConnection conn, ErrorMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnServerErrorInternal" );
            OnServerError ( conn, msg.value );
        }

        #endregion

        #region Client Internal Message Handlers

        void OnClientConnectInternal ( NetworkConnection conn, ConnectMessage message )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnClientConnectInternal" );

            if ( authenticator != null )
            {
                authenticator.OnClientAuthenticateInternal ( conn );
            }
            else
            {
                OnClientAuthenticated ( conn );
            }
        }

        void OnClientAuthenticated ( NetworkConnection conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnClientAuthenticated" );

            conn.isAuthenticated = true;

            string loadedSceneName = SceneManager.GetActiveScene ().name;
            if ( string.IsNullOrEmpty ( onlineScene ) || onlineScene == offlineScene || loadedSceneName == onlineScene )
            {
                clientLoadedScene = false;
                OnClientConnect ( conn );
            }
            else
            {
                clientReadyConnection = conn;
            }
        }

        void OnClientDisconnectInternal ( NetworkConnection conn, DisconnectMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnClientDisconnectInternal" );
            OnClientDisconnect ( conn );
        }

        void OnClientNotReadyMessageInternal ( NetworkConnection conn, NotReadyMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnClientNotReadyMessageInternal" );

            ClientScene.ready = false;
            OnClientNotReady ( conn );

        }

        void OnClientErrorInternal ( NetworkConnection conn, ErrorMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager:OnClientErrorInternal" );
            OnClientError ( conn, msg.value );
        }

        void OnClientSceneInternal ( NetworkConnection conn, SceneMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkManager.OnClientSceneInternal" );

            if ( NetworkClient.isConnected && !NetworkServer.active )
            {
                ClientChangeScene ( msg.sceneName, msg.sceneOperation );
            }
        }

        #endregion

        #region Server System Callbacks

        public virtual void OnServerConnect ( NetworkConnection conn ) { OnServerConnectCallback?.Invoke ( conn ); }

        public virtual void OnServerDisconnect ( NetworkConnection conn )
        {
            NetworkServer.DestroyPlayerForConnection ( conn );
            if ( LogFilter.Debug ) Debug.Log ( "OnServerDisconnect: Client disconnected." );

            OnServerDisconnectCallback?.Invoke ( conn );
        }

        public virtual void OnServerReady ( NetworkConnection conn )
        {
            if ( conn.identity == null )
            {
                if ( LogFilter.Debug ) Debug.Log ( "Ready with no player object" );
            }
            NetworkServer.SetClientReady ( conn );

            OnServerReadyCallback?.Invoke ( conn );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Override OnServerAddPlayer(NetworkConnection conn) instead. See https://mirror-networking.com/docs/Guides/GameObjects/SpawnPlayerCustom.html for details." )]
        public virtual void OnServerAddPlayer ( NetworkConnection conn, AddPlayerMessage extraMessage )
        {
            OnServerAddPlayer ( conn );
        }

        public virtual void OnServerAddPlayer ( NetworkConnection conn )
        {
            Transform startPos = GetStartPosition ();
            GameObject player = startPos != null
                ? Instantiate ( playerPrefab, startPos.position, startPos.rotation )
                : Instantiate ( playerPrefab );

            NetworkServer.AddPlayerForConnection ( conn, player );

            OnServerAddPlayerCallback?.Invoke ( conn, player.GetComponent<NetworkIdentity> () );
        }

        public Transform GetStartPosition ()
        {
            startPositions.RemoveAll ( t => t == null );

            if ( startPositions.Count == 0 )
                return null;

            if ( playerSpawnMethod == PlayerSpawnMethod.Random )
            {
                return startPositions [ UnityEngine.Random.Range ( 0, startPositions.Count ) ];
            }
            else
            {
                Transform startPosition = startPositions [ startPositionIndex ];
                startPositionIndex = ( startPositionIndex + 1 ) % startPositions.Count;
                return startPosition;
            }
        }

        public virtual void OnServerRemovePlayer ( NetworkConnection conn, NetworkIdentity player )
        {
            if ( player.gameObject != null )
            {
                NetworkServer.Destroy ( player.gameObject );

                OnServerRemovePlayerCallback?.Invoke ( conn, player );
            }
        }

        public virtual void OnServerError ( NetworkConnection conn, int errorCode ) { OnServerErrorCallback?.Invoke ( conn, errorCode ); }

        public virtual void OnServerChangeScene ( string newSceneName ) { OnServerChangeSceneCallback?.Invoke ( newSceneName ); }

        public virtual void OnServerSceneChanged ( string sceneName ) { OnServerSceneChangedCallback?.Invoke ( sceneName ); }

        #endregion

        #region Client System Callbacks

        public virtual void OnClientConnect ( NetworkConnection conn )
        {
            if ( !clientLoadedScene )
            {
                if ( !ClientScene.ready ) ClientScene.Ready ( conn );
                if ( autoCreatePlayer )
                {
                    ClientScene.AddPlayer ();

                    OnClientConnectCallback?.Invoke ( conn );
                }
            }
        }

        public virtual void OnClientDisconnect ( NetworkConnection conn )
        {
            StopClient ();

            OnClientDisconnectCallback?.Invoke ( conn );
        }

        public virtual void OnClientError ( NetworkConnection conn, int errorCode )
        {
            OnClientErrorCallback?.Invoke ( conn, errorCode );
        }

        public virtual void OnClientNotReady ( NetworkConnection conn ) { }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Override OnClientChangeScene(string newSceneName, SceneOperation sceneOperation) instead" )]
        public virtual void OnClientChangeScene ( string newSceneName )
        {
            OnClientChangeScene ( newSceneName, SceneOperation.Normal );
        }

        public virtual void OnClientChangeScene ( string newSceneName, SceneOperation sceneOperation ) { OnClientChangeSceneCallback?.Invoke ( newSceneName, sceneOperation ); }

        public virtual void OnClientSceneChanged ( NetworkConnection conn )
        {
            if ( !ClientScene.ready ) ClientScene.Ready ( conn );

            if ( autoCreatePlayer && ClientScene.localPlayer == null )
            {
                ClientScene.AddPlayer ();

                OnClientSceneChangedCallback?.Invoke ( conn );
            }
        }

        #endregion

        #region Start & Stop callbacks


        public virtual void OnStartHost () { OnStartHostCallback?.Invoke (); }

        public virtual void OnStartServer () { OnStartServerCallback?.Invoke (); }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use OnStartClient() instead of OnStartClient(NetworkClient client). All NetworkClient functions are static now, so you can use NetworkClient.Send(message) instead of client.Send(message) directly now." )]
        public virtual void OnStartClient ( NetworkClient client ) { }

        public virtual void OnStartClient ()
        {
#pragma warning disable CS0618 
            OnStartClient ( NetworkClient.singleton );
#pragma warning restore CS0618

            OnStartClientCallback?.Invoke ();
        }

        public virtual void OnStopServer () { OnStopServerCallback?.Invoke (); }

        public virtual void OnStopClient () { OnStopClientCallback?.Invoke (); }

        public virtual void OnStopHost () { OnStopHostCallback?.Invoke (); }

        #endregion
    }
}
