using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public enum ConnectState
    {
        None,
        Connecting,
        Connected,
        Disconnected
    }

    public class NetworkClient
    {
        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient directly. Singleton isn't needed anymore, all functions are static now. For example: Humanlights.Unity.UNet.NetworkClient.Send(message) instead of NetworkClient.singleton.Send(message)." )]
        public static NetworkClient singleton = new NetworkClient ();

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient directly instead. There is always exactly one client." )]
        public static List<NetworkClient> allClients => new List<NetworkClient> { singleton };

        public static readonly Dictionary<int, NetworkMessageDelegate> handlers = new Dictionary<int, NetworkMessageDelegate> ();

        public static NetworkConnection connection { get; internal set; }

        internal static ConnectState connectState = ConnectState.None;

        public static string serverIp => connection.address;

        public static bool active => connectState == ConnectState.Connecting || connectState == ConnectState.Connected;

        public static bool isConnected => connectState == ConnectState.Connected;

        public static bool isLocalClient => connection is ULocalConnectionToServer;

        public static void Connect ( string address )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Client Connect: " + address );

            RegisterSystemHandlers ( false );
            Transport.activeTransport.enabled = true;
            InitializeTransportHandlers ();

            connectState = ConnectState.Connecting;
            Transport.activeTransport.ClientConnect ( address );

            connection = new NetworkConnectionToServer ();
            connection.SetHandlers ( handlers );
        }

        internal static void ConnectLocalServer ()
        {
            if ( LogFilter.Debug ) Debug.Log ( "Client Connect Local Server" );

            RegisterSystemHandlers ( true );

            connectState = ConnectState.Connected;

            ULocalConnectionToServer connectionToServer = new ULocalConnectionToServer ();
            ULocalConnectionToClient connectionToClient = new ULocalConnectionToClient ();
            connectionToServer.connectionToClient = connectionToClient;
            connectionToClient.connectionToServer = connectionToServer;

            connection = connectionToServer;
            connection.SetHandlers ( handlers );

            NetworkServer.SetLocalConnection ( connectionToClient );
            connectionToClient.Send ( new ConnectMessage () );
        }

        internal static void AddLocalPlayer ( NetworkIdentity localPlayer )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Local client AddLocalPlayer " + localPlayer.gameObject.name + " " + connection );
            connection.isReady = true;
            connection.identity = localPlayer;
            if ( localPlayer != null )
            {
                localPlayer.isClient = true;
                NetworkIdentity.spawned [ localPlayer.netId ] = localPlayer;
                localPlayer.connectionToServer = connection;
            }
            ClientScene.InternalAddPlayer ( localPlayer );
        }

        static void InitializeTransportHandlers ()
        {
            Transport.activeTransport.OnClientConnected.AddListener ( OnConnected );
            Transport.activeTransport.OnClientDataReceived.AddListener ( OnDataReceived );
            Transport.activeTransport.OnClientDisconnected.AddListener ( OnDisconnected );
            Transport.activeTransport.OnClientError.AddListener ( OnError );
        }

        static void OnError ( Exception exception )
        {
            Debug.LogException ( exception );
        }

        static void OnDisconnected ()
        {
            connectState = ConnectState.Disconnected;

            ClientScene.HandleClientDisconnect ( connection );

            connection?.InvokeHandler ( new DisconnectMessage (), -1 );
        }

        internal static void OnDataReceived ( ArraySegment<byte> data, int channelId )
        {
            if ( connection != null )
            {
                connection.TransportReceive ( data, channelId );
            }
            else Debug.LogError ( "Skipped Data message handling because connection is null." );
        }

        static void OnConnected ()
        {
            if ( connection != null )
            {
                NetworkTime.Reset ();

                connectState = ConnectState.Connected;
                NetworkTime.UpdateClient ();
                connection.InvokeHandler ( new ConnectMessage (), -1 );
            }
            else Debug.LogError ( "Skipped Connect message handling because connection is null." );
        }

        public static void Disconnect ()
        {
            connectState = ConnectState.Disconnected;
            ClientScene.HandleClientDisconnect ( connection );

            if ( isLocalClient )
            {
                if ( isConnected )
                {
                    NetworkServer.localConnection.Send ( new DisconnectMessage () );
                }
                NetworkServer.RemoveLocalConnection ();
            }
            else
            {
                if ( connection != null )
                {
                    connection.Disconnect ();
                    connection = null;
                    RemoveTransportHandlers ();
                }
            }
        }

        static void RemoveTransportHandlers ()
        {
            Transport.activeTransport.OnClientConnected.RemoveListener ( OnConnected );
            Transport.activeTransport.OnClientDataReceived.RemoveListener ( OnDataReceived );
            Transport.activeTransport.OnClientDisconnected.RemoveListener ( OnDisconnected );
            Transport.activeTransport.OnClientError.RemoveListener ( OnError );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use SendMessage<T> instead with no message id instead" )]
        public static bool Send ( short msgType, MessageBase msg )
        {
            if ( connection != null )
            {
                if ( connectState != ConnectState.Connected )
                {
                    Debug.LogError ( "NetworkClient Send when not connected to a server" );
                    return false;
                }
                return connection.Send ( msgType, msg );
            }
            Debug.LogError ( "NetworkClient Send with no connection" );
            return false;
        }

        public static bool Send<T> ( T message, int channelId = Channels.DefaultReliable ) where T : IMessageBase
        {
            if ( connection != null )
            {
                if ( connectState != ConnectState.Connected )
                {
                    Debug.LogError ( "NetworkClient Send when not connected to a server" );
                    return false;
                }
                return connection.Send ( message, channelId );
            }
            Debug.LogError ( "NetworkClient Send with no connection" );
            return false;
        }

        internal static void Update ()
        {
            if ( connection is ULocalConnectionToServer localConnection )
            {
                localConnection.Update ();
            }
            else
            {
                if ( active && connectState == ConnectState.Connected )
                {
                    NetworkTime.UpdateClient ();
                }
            }
        }

        /* TODO use or remove
        void GenerateConnectError(byte error)
        {
            Debug.LogError("Humanlights.Unity.Network Client Error Connect Error: " + error);
            GenerateError(error);
        }

        void GenerateDataError(byte error)
        {
            NetworkError dataError = (NetworkError)error;
            Debug.LogError("Humanlights.Unity.Network Client Data Error: " + dataError);
            GenerateError(error);
        }

        void GenerateDisconnectError(byte error)
        {
            NetworkError disconnectError = (NetworkError)error;
            Debug.LogError("Humanlights.Unity.Network Client Disconnect Error: " + disconnectError);
            GenerateError(error);
        }

        void GenerateError(byte error)
        {
            int msgId = MessageBase.GetId<ErrorMessage>();
            if (handlers.TryGetValue(msgId, out NetworkMessageDelegate msgDelegate))
            {
                ErrorMessage msg = new ErrorMessage
                {
                    value = error
                };

                NetworkWriter writer = new NetworkWriter();
                msg.Serialize(writer);

                NetworkMessage netMsg = new NetworkMessage
                {
                    msgType = msgId,
                    reader = new NetworkReader(writer.ToArray()),
                    conn = connection
                };
                msgDelegate(netMsg);
            }
        }
        */

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkTime.rtt instead" )]
        public static float GetRTT ()
        {
            return ( float ) NetworkTime.rtt;
        }

        internal static void RegisterSystemHandlers ( bool localClient )
        {
            if ( localClient )
            {
                RegisterHandler<ObjectDestroyMessage> ( ClientScene.OnLocalClientObjectDestroy );
                RegisterHandler<ObjectHideMessage> ( ClientScene.OnLocalClientObjectHide );
                RegisterHandler<NetworkPongMessage> ( ( conn, msg ) => { }, false );
                RegisterHandler<SpawnMessage> ( ClientScene.OnLocalClientSpawn );
                RegisterHandler<ObjectSpawnStartedMessage> ( ( conn, msg ) => { } );
                RegisterHandler<ObjectSpawnFinishedMessage> ( ( conn, msg ) => { } );
                RegisterHandler<UpdateVarsMessage> ( ( conn, msg ) => { } );
            }
            else
            {
                RegisterHandler<ObjectDestroyMessage> ( ClientScene.OnObjectDestroy );
                RegisterHandler<ObjectHideMessage> ( ClientScene.OnObjectHide );
                RegisterHandler<NetworkPongMessage> ( NetworkTime.OnClientPong, false );
                RegisterHandler<SpawnMessage> ( ClientScene.OnSpawn );
                RegisterHandler<ObjectSpawnStartedMessage> ( ClientScene.OnObjectSpawnStarted );
                RegisterHandler<ObjectSpawnFinishedMessage> ( ClientScene.OnObjectSpawnFinished );
                RegisterHandler<UpdateVarsMessage> ( ClientScene.OnUpdateVarsMessage );
            }
            RegisterHandler<ClientAuthorityMessage> ( ClientScene.OnClientAuthority );
            RegisterHandler<RpcMessage> ( ClientScene.OnRPCMessage );
            RegisterHandler<SyncEventMessage> ( ClientScene.OnSyncEventMessage );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use RegisterHandler<T> instead" )]
        public static void RegisterHandler ( int msgType, NetworkMessageDelegate handler )
        {
            if ( handlers.ContainsKey ( msgType ) )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkClient.RegisterHandler replacing " + handler + " - " + msgType );
            }
            handlers [ msgType ] = handler;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use RegisterHandler<T> instead" )]
        public static void RegisterHandler ( MsgType msgType, NetworkMessageDelegate handler )
        {
            RegisterHandler ( ( int ) msgType, handler );
        }

        public static void RegisterHandler<T> ( Action<NetworkConnection, T> handler, bool requireAuthentication = true ) where T : IMessageBase, new()
        {
            int msgType = MessagePacker.GetId<T> ();
            if ( handlers.ContainsKey ( msgType ) )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkClient.RegisterHandler replacing " + handler + " - " + msgType );
            }
            handlers [ msgType ] = MessagePacker.MessageHandler<T> ( handler, requireAuthentication );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use UnregisterHandler<T> instead" )]
        public static void UnregisterHandler ( int msgType )
        {
            handlers.Remove ( msgType );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use UnregisterHandler<T> instead" )]
        public static void UnregisterHandler ( MsgType msgType )
        {
            UnregisterHandler ( ( int ) msgType );
        }

        public static void UnregisterHandler<T> () where T : IMessageBase
        {
            int msgType = MessagePacker.GetId<T> ();
            handlers.Remove ( msgType );
        }

        public static void Shutdown ()
        {
            if ( LogFilter.Debug ) Debug.Log ( "Shutting down client." );
            ClientScene.Shutdown ();
            connectState = ConnectState.None;
            handlers.Clear ();
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Call NetworkClient.Shutdown() instead. There is only one client." )]
        public static void ShutdownAll ()
        {
            Shutdown ();
        }
    }
}
