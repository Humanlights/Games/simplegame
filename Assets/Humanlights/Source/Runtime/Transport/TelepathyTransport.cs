using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Serialization;

namespace Humanlights.Unity.UNet
{
    [HelpURL ( "https://github.com/vis2k/Telepathy/blob/master/README.md" )]
    public class TelepathyTransport : Transport
    {
        public long ServerDataReceivedCount { get; set; }
        public long ServerDataReceivedTotalCount { get; set; }
        public long ClientDataReceivedCount { get; set; }
        public long ClientDataReceivedTotalCount { get; set; }

        public long ClientDataSentCount { get; set; }
        public long ClientDataSentTotalCount { get; set; }
        public long ServerDataSentCount { get; set; }
        public long ServerDataSentTotalCount { get; set; }

        public ushort port = 7777;

        [Tooltip ( "Nagle Algorithm can be disabled by enabling NoDelay" )]
        public bool NoDelay = true;

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use MaxMessageSizeFromClient or MaxMessageSizeFromServer instead." )]
        public int MaxMessageSize
        {
            get => serverMaxMessageSize;
            set => serverMaxMessageSize = clientMaxMessageSize = value;
        }

        [Tooltip ( "Protect against allocation attacks by keeping the max message size small. Otherwise an attacker might send multiple fake packets with 2GB headers, causing the server to run out of memory after allocating multiple large packets." )]
        [FormerlySerializedAs ( "MaxMessageSize" )] public int serverMaxMessageSize = 16 * 1024;

        [Tooltip ( "Protect against allocation attacks by keeping the max message size small. Otherwise an attacker host might send multiple fake packets with 2GB headers, causing the connected clients to run out of memory after allocating multiple large packets." )]
        [FormerlySerializedAs ( "MaxMessageSize" )] public int clientMaxMessageSize = 16 * 1024;

        public Telepathy.Client client = new Telepathy.Client ();
        public Telepathy.Server server = new Telepathy.Server ();

        void Awake ()
        {
            Telepathy.Logger.Log = Debug.Log;
            Telepathy.Logger.LogWarning = Debug.LogWarning;
            Telepathy.Logger.LogError = Debug.LogError;

            client.NoDelay = NoDelay;
            client.MaxMessageSize = clientMaxMessageSize;
            server.NoDelay = NoDelay;
            server.MaxMessageSize = serverMaxMessageSize;

            // Debug.Log("TelepathyTransport initialized!");
        }

        public void Reset ()
        {
            ServerDataReceivedCount =
            ServerDataReceivedTotalCount =
            ClientDataReceivedCount =
            ClientDataReceivedTotalCount =
            ClientDataSentCount =
            ClientDataSentTotalCount =
            ServerDataSentCount =
            ServerDataSentTotalCount = 0;
        }

        public override bool Available ()
        {
            return Application.platform != RuntimePlatform.WebGLPlayer;
        }

        public override bool ClientConnected () => client.Connected;
        public override void ClientConnect ( string address ) => client.Connect ( address, port );
        public override bool ClientSend ( int channelId, ArraySegment<byte> segment )
        {
            byte [] data = new byte [ segment.Count ];
            ClientDataSentCount = data.Length;
            ClientDataSentTotalCount += ClientDataSentCount;

            Array.Copy ( segment.Array, segment.Offset, data, 0, segment.Count );
            return client.Send ( data );
        }

        bool ProcessClientMessage ()
        {
            if ( client.GetNextMessage ( out Telepathy.Message message ) )
            {
                switch ( message.eventType )
                {
                    case Telepathy.EventType.Connected:
                        OnClientConnected.Invoke ();
                        break;
                    case Telepathy.EventType.Data:
                        OnClientDataReceived.Invoke ( new ArraySegment<byte> ( message.data ), Channels.DefaultReliable );
                        break;
                    case Telepathy.EventType.Disconnected:
                        OnClientDisconnected.Invoke ();
                        break;
                    default:
                        OnClientDisconnected.Invoke ();
                        break;
                }

                ClientDataReceivedCount = message.data != null ? message.data.Length : 0;
                ClientDataReceivedTotalCount += ClientDataReceivedCount;

                return true;
            }
            return false;
        }
        public override void ClientDisconnect () => client.Disconnect ();

        public void LateUpdate ()
        {
            while ( enabled && ProcessClientMessage () ) { }
            while ( enabled && ProcessServerMessage () ) { }
        }

        public override bool ServerActive () => server.Active;
        public override void ServerStart () => server.Start ( port );
        public override bool ServerSend ( List<int> connectionIds, int channelId, ArraySegment<byte> segment )
        {
            byte [] data = new byte [ segment.Count ];
            Array.Copy ( segment.Array, segment.Offset, data, 0, segment.Count );

            ServerDataSentCount = data.Length;
            ServerDataSentTotalCount += ServerDataSentCount;

            bool result = true;
            foreach ( int connectionId in connectionIds )
                result &= server.Send ( connectionId, data );

            return result;
        }
        public bool ProcessServerMessage ()
        {
            if ( server.GetNextMessage ( out Telepathy.Message message ) )
            {
                switch ( message.eventType )
                {
                    case Telepathy.EventType.Connected:
                        OnServerConnected.Invoke ( message.connectionId );
                        break;
                    case Telepathy.EventType.Data:
                        OnServerDataReceived.Invoke ( message.connectionId, new ArraySegment<byte> ( message.data ), Channels.DefaultReliable );
                        break;
                    case Telepathy.EventType.Disconnected:
                        OnServerDisconnected.Invoke ( message.connectionId );
                        break;
                    default:
                        OnServerDisconnected.Invoke ( message.connectionId );
                        break;
                }

                ServerDataReceivedCount = message.data != null ? message.data.Length : 0;
                ServerDataReceivedTotalCount += ServerDataReceivedCount;

                return true;
            }
            return false;
        }
        public override bool ServerDisconnect ( int connectionId ) => server.Disconnect ( connectionId );
        public override string ServerGetClientAddress ( int connectionId )
        {
            try
            {
                return server.GetClientAddress ( connectionId );
            }
            catch ( SocketException )
            {
                return "unknown";
            }
        }
        public override void ServerStop () => server.Stop ();

        public override void Shutdown ()
        {
            // Debug.Log ( "TelepathyTransport Shutdown()" );
            client.Disconnect ();
            server.Stop ();
        }

        public override int GetMaxPacketSize ( int channelId )
        {
            return serverMaxMessageSize;
        }

        public override string ToString ()
        {
            if ( server.Active && server.listener != null )
            {
                return "Telepathy Server port: " + port;
            }
            else if ( client.Connecting || client.Connected )
            {
                return "Telepathy Client ip: " + client.client.Client.RemoteEndPoint;
            }
            return "Telepathy (inactive/disconnected)";
        }
    }
}
