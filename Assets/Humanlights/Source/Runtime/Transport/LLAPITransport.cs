// https://docs.unity3d.com/Manual/PlatformDependentCompilation.html

#if !(UNITY_WSA || UNITY_WSA_10_0 || UNITY_WINRT || UNITY_WINRT_10_0 || NETFX_CORE)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

namespace Humanlights.Unity.UNet
{
    [EditorBrowsable(EditorBrowsableState.Never), Obsolete("LLAPI is obsolete and will be removed from future versions of Unity")]
    public class LLAPITransport : Transport
    {
        public ushort port = 7777;

        [Tooltip("Enable for WebGL games. Can only do either WebSockets or regular Sockets, not both (yet).")]
        public bool useWebsockets;

        public ConnectionConfig connectionConfig = new ConnectionConfig
        {
            PacketSize = 1500,
            FragmentSize = 500,
            ResendTimeout = 1200,
            DisconnectTimeout = 6000,
            ConnectTimeout = 6000,
            MinUpdateTimeout = 1,
            PingTimeout = 2000,
            ReducedPingTimeout = 100,
            AllCostTimeout = 20,
            NetworkDropThreshold = 80,
            OverflowDropThreshold = 80,
            MaxConnectionAttempt = 10,
            AckDelay = 33,
            SendDelay = 10,
            MaxCombinedReliableMessageSize = 100,
            MaxCombinedReliableMessageCount = 10,
            MaxSentMessageQueueSize = 512,
            AcksType = ConnectionAcksType.Acks128,
            InitialBandwidth = 0,
            BandwidthPeakFactor = 2,
            WebSocketReceiveBufferMaxSize = 0,
            UdpSocketReceiveBufferMaxSize = 0
        };

        public GlobalConfig globalConfig = new GlobalConfig
        {
            ReactorModel = ReactorModel.SelectReactor,
            ThreadAwakeTimeout = 1,
            ReactorMaximumSentMessages = 4096,
            ReactorMaximumReceivedMessages = 4096,
            MaxPacketSize = 2000,
            MaxHosts = 16,
            ThreadPoolSize = 3,
            MinTimerTimeout = 1,
            MaxTimerTimeout = 12000
        };

        readonly int channelId; 
        byte error;

        int clientId = -1;
        int clientConnectionId = -1;
        readonly byte[] clientReceiveBuffer = new byte[4096];
        byte[] clientSendBuffer;

        int serverHostId = -1;
        readonly byte[] serverReceiveBuffer = new byte[4096];
        byte[] serverSendBuffer;

        void OnValidate()
        {
            if (connectionConfig.Channels.Count == 0)
            {
                connectionConfig.AddChannel(QosType.ReliableFragmentedSequenced);
                connectionConfig.AddChannel(QosType.Unreliable);
            }
        }

        void Awake()
        {
            NetworkTransport.Init(globalConfig);
            Debug.Log("LLAPITransport initialized!");

            clientSendBuffer = new byte[globalConfig.MaxPacketSize];
            serverSendBuffer = new byte[globalConfig.MaxPacketSize];
        }

        public override bool Available()
        {
            return true;
        }

        #region client
        public override bool ClientConnected()
        {
            return clientConnectionId != -1;
        }

        public override void ClientConnect(string address)
        {
            if (address.ToLower() == "localhost") address = "127.0.0.1";

            HostTopology hostTopology = new HostTopology(connectionConfig, 1);

            clientId = NetworkTransport.AddHost(hostTopology, 0);

            clientConnectionId = NetworkTransport.Connect(clientId, address, port, 0, out error);
            NetworkError networkError = (NetworkError)error;
            if (networkError != NetworkError.Ok)
            {
                Debug.LogWarning("NetworkTransport.Connect failed: clientId=" + clientId + " address= " + address + " port=" + port + " error=" + error);
                clientConnectionId = -1;
            }
        }

        public override bool ClientSend(int channelId, ArraySegment<byte> segment)
        {
            if (segment.Count <= clientSendBuffer.Length)
            {
                Array.Copy(segment.Array, segment.Offset, clientSendBuffer, 0, segment.Count);
                return NetworkTransport.Send(clientId, clientConnectionId, channelId, clientSendBuffer, segment.Count, out error);
            }
            Debug.LogError("LLAPI.ClientSend: buffer( " + clientSendBuffer.Length + ") too small for: " + segment.Count);
            return false;
        }

        public bool ProcessClientMessage()
        {
            if (clientId == -1) return false;

            NetworkEventType networkEvent = NetworkTransport.ReceiveFromHost(clientId, out int connectionId, out int channel, clientReceiveBuffer, clientReceiveBuffer.Length, out int receivedSize, out error);

            NetworkError networkError = (NetworkError)error;
            if (networkError != NetworkError.Ok)
            {
                string message = "NetworkTransport.Receive failed: hostid=" + clientId + " connId=" + connectionId + " channelId=" + channel + " error=" + networkError;
                OnClientError.Invoke(new Exception(message));
            }

            switch (networkEvent)
            {
                case NetworkEventType.ConnectEvent:
                    OnClientConnected.Invoke();
                    break;
                case NetworkEventType.DataEvent:
                    ArraySegment<byte> data = new ArraySegment<byte>(clientReceiveBuffer, 0, receivedSize);
                    OnClientDataReceived.Invoke(data, channel);
                    break;
                case NetworkEventType.DisconnectEvent:
                    OnClientDisconnected.Invoke();
                    break;
                default:
                    return false;
            }

            return true;
        }

        public string ClientGetAddress()
        {
            NetworkTransport.GetConnectionInfo(serverHostId, clientId, out string address, out int port, out NetworkID networkId, out NodeID node, out error);
            return address;
        }

        public override void ClientDisconnect()
        {
            if (clientId != -1)
            {
                NetworkTransport.RemoveHost(clientId);
                clientId = -1;
            }
        }
        #endregion

        #region server
        public override bool ServerActive()
        {
            return serverHostId != -1;
        }

        public override void ServerStart()
        {
            if (useWebsockets)
            {
                HostTopology topology = new HostTopology(connectionConfig, ushort.MaxValue - 1);
                serverHostId = NetworkTransport.AddWebsocketHost(topology, port);
            }
            else
            {
                HostTopology topology = new HostTopology(connectionConfig, ushort.MaxValue - 1);
                serverHostId = NetworkTransport.AddHost(topology, port);
            }
        }

        public override bool ServerSend(List<int> connectionIds, int channelId, ArraySegment<byte> segment)
        {
            if (segment.Count <= serverSendBuffer.Length)
            {
                Array.Copy(segment.Array, segment.Offset, serverSendBuffer, 0, segment.Count);

                bool result = true;
                foreach (int connectionId in connectionIds)
                {
                    result &= NetworkTransport.Send(serverHostId, connectionId, channelId, serverSendBuffer, segment.Count, out error);
                }
                return result;
            }
            Debug.LogError("LLAPI.ServerSend: buffer( " + serverSendBuffer.Length + ") too small for: " + segment.Count);
            return false;
        }

        public bool ProcessServerMessage()
        {
            if (serverHostId == -1) return false;

            NetworkEventType networkEvent = NetworkTransport.ReceiveFromHost(serverHostId, out int connectionId, out int channel, serverReceiveBuffer, serverReceiveBuffer.Length, out int receivedSize, out error);

            NetworkError networkError = (NetworkError)error;
            if (networkError != NetworkError.Ok)
            {
                string message = "NetworkTransport.Receive failed: hostid=" + serverHostId + " connId=" + connectionId + " channelId=" + channel + " error=" + networkError;

                OnServerError.Invoke(connectionId, new Exception(message));
            }

            /*if (channel != channelId)
            {
                return false;
            }*/

            switch (networkEvent)
            {
                case NetworkEventType.ConnectEvent:
                    OnServerConnected.Invoke(connectionId);
                    break;
                case NetworkEventType.DataEvent:
                    ArraySegment<byte> data = new ArraySegment<byte>(serverReceiveBuffer, 0, receivedSize);
                    OnServerDataReceived.Invoke(connectionId, data, channel);
                    break;
                case NetworkEventType.DisconnectEvent:
                    OnServerDisconnected.Invoke(connectionId);
                    break;
                default:
                    return false;
            }

            return true;
        }

        public override bool ServerDisconnect(int connectionId)
        {
            return NetworkTransport.Disconnect(serverHostId, connectionId, out error);
        }

        public override string ServerGetClientAddress(int connectionId)
        {
            NetworkTransport.GetConnectionInfo(serverHostId, connectionId, out string address, out int port, out NetworkID networkId, out NodeID node, out error);
            return address;
        }

        public override void ServerStop()
        {
            NetworkTransport.RemoveHost(serverHostId);
            serverHostId = -1;
            Debug.Log("LLAPITransport.ServerStop");
        }
        #endregion

        #region common
        public void LateUpdate()
        {
            while (ProcessClientMessage()) {}
            while (ProcessServerMessage()) {}
        }

        public override void Shutdown()
        {
            NetworkTransport.Shutdown();
            serverHostId = -1;
            clientConnectionId = -1;
            Debug.Log("LLAPITransport.Shutdown");
        }

        public override int GetMaxPacketSize(int channelId)
        {
            return globalConfig.MaxPacketSize;
        }

        public override string ToString()
        {
            if (ServerActive())
            {
                return "LLAPI Server port: " + port;
            }
            else if (ClientConnected())
            {
                string ip = ClientGetAddress();
                return "LLAPI Client ip: " + ip + " port: " + port;
            }
            return "LLAPI (inactive/disconnected)";
        }
        #endregion
    }
}
#endif
