
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Ninja.WebSockets.Exceptions;
using Ninja.WebSockets.Internal;

namespace Ninja.WebSockets
{
    public class WebSocketServerFactory : IWebSocketServerFactory
    {
        readonly Func<MemoryStream> _bufferFactory;
        readonly IBufferPool _bufferPool;

        public WebSocketServerFactory()
        {
            _bufferPool = new BufferPool();
            _bufferFactory = _bufferPool.GetBuffer;
        }

        public WebSocketServerFactory(Func<MemoryStream> bufferFactory)
        {
            _bufferFactory = bufferFactory;
        }

        public async Task<WebSocketHttpContext> ReadHttpHeaderFromStreamAsync(TcpClient client, Stream stream, CancellationToken token = default(CancellationToken))
        {
            string header = await HttpHelper.ReadHttpHeaderAsync(stream, token);
            string path = HttpHelper.GetPathFromHeader(header);
            bool isWebSocketRequest = HttpHelper.IsWebSocketUpgradeRequest(header);
            IList<string> subProtocols = HttpHelper.GetSubProtocols(header);
            return new WebSocketHttpContext(isWebSocketRequest, subProtocols, header, path, client, stream);
        }

        public async Task<WebSocket> AcceptWebSocketAsync(WebSocketHttpContext context, CancellationToken token = default(CancellationToken))
        {
            return await AcceptWebSocketAsync(context, new WebSocketServerOptions(), token);
        }

        public async Task<WebSocket> AcceptWebSocketAsync(WebSocketHttpContext context, WebSocketServerOptions options, CancellationToken token = default(CancellationToken))
        {
            Guid guid = Guid.NewGuid();
            Events.Log.AcceptWebSocketStarted(guid);
            await PerformHandshakeAsync(guid, context.HttpHeader, options.SubProtocol, context.Stream, token);
            Events.Log.ServerHandshakeSuccess(guid);
            string secWebSocketExtensions = null;
            return new WebSocketImplementation(guid, _bufferFactory, context.Stream, options.KeepAliveInterval, secWebSocketExtensions, options.IncludeExceptionInCloseResponse, false, options.SubProtocol)
            {
                Context = context
            };
        }

        static void CheckWebSocketVersion(string httpHeader)
        {
            Regex webSocketVersionRegex = new Regex("Sec-WebSocket-Version: (.*)", RegexOptions.IgnoreCase);

            const int WebSocketVersion = 13;
            Match match = webSocketVersionRegex.Match(httpHeader);
            if (match.Success)
            {
                int secWebSocketVersion = Convert.ToInt32(match.Groups[1].Value.Trim());
                if (secWebSocketVersion < WebSocketVersion)
                {
                    throw new WebSocketVersionNotSupportedException(string.Format("WebSocket Version {0} not suported. Must be {1} or above", secWebSocketVersion, WebSocketVersion));
                }
            }
            else
            {
                throw new WebSocketVersionNotSupportedException("Cannot find \"Sec-WebSocket-Version\" in http header");
            }
        }

        static async Task PerformHandshakeAsync(Guid guid, String httpHeader, string subProtocol, Stream stream, CancellationToken token)
        {
            try
            {
                Regex webSocketKeyRegex = new Regex("Sec-WebSocket-Key: (.*)", RegexOptions.IgnoreCase);
                CheckWebSocketVersion(httpHeader);

                Match match = webSocketKeyRegex.Match(httpHeader);
                if (match.Success)
                {
                    string secWebSocketKey = match.Groups[1].Value.Trim();
                    string setWebSocketAccept = HttpHelper.ComputeSocketAcceptString(secWebSocketKey);
                    string response = ("HTTP/1.1 101 Switching Protocols\r\n"
                                       + "Connection: Upgrade\r\n"
                                       + "Upgrade: websocket\r\n"
                                       + (subProtocol != null ? $"Sec-WebSocket-Protocol: {subProtocol}\r\n" : "")
                                       + $"Sec-WebSocket-Accept: {setWebSocketAccept}");

                    Events.Log.SendingHandshakeResponse(guid, response);
                    await HttpHelper.WriteHttpHeaderAsync(response, stream, token);
                }
                else
                {
                    throw new SecWebSocketKeyMissingException("Unable to read \"Sec-WebSocket-Key\" from http header");
                }
            }
            catch (WebSocketVersionNotSupportedException ex)
            {
                Events.Log.WebSocketVersionNotSupported(guid, ex.ToString());
                string response = "HTTP/1.1 426 Upgrade Required\r\nSec-WebSocket-Version: 13" + ex.Message;
                await HttpHelper.WriteHttpHeaderAsync(response, stream, token);
                throw;
            }
            catch (Exception ex)
            {
                Events.Log.BadRequest(guid, ex.ToString());
                await HttpHelper.WriteHttpHeaderAsync("HTTP/1.1 400 Bad Request", stream, token);
                throw;
            }
        }
    }
}
