
using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Ninja.WebSockets.Internal;

namespace Ninja.WebSockets
{
    public class PingPongManager : IPingPongManager
    {
        readonly WebSocketImplementation _webSocket;
        readonly Guid _guid;
        readonly TimeSpan _keepAliveInterval;
        readonly Task _pingTask;
        readonly CancellationToken _cancellationToken;
        Stopwatch _stopwatch;
        long _pingSentTicks;

        public event EventHandler<PongEventArgs> Pong;

        public PingPongManager(Guid guid, WebSocket webSocket, TimeSpan keepAliveInterval, CancellationToken cancellationToken)
        {
            WebSocketImplementation webSocketImpl = webSocket as WebSocketImplementation;
            _webSocket = webSocketImpl;
            if (_webSocket == null)
                throw new InvalidCastException("Cannot cast WebSocket to an instance of WebSocketImplementation. Please use the web socket factories to create a web socket");
            _guid = guid;
            _keepAliveInterval = keepAliveInterval;
            _cancellationToken = cancellationToken;
            webSocketImpl.Pong += WebSocketImpl_Pong;
            _stopwatch = Stopwatch.StartNew();

            if (keepAliveInterval != TimeSpan.Zero)
            {
                Task.Run(PingForever, cancellationToken);
            }
        }

        public async Task SendPing(ArraySegment<byte> payload, CancellationToken cancellation)
        {
            await _webSocket.SendPingAsync(payload, cancellation);
        }

        protected virtual void OnPong(PongEventArgs e)
        {
            Pong?.Invoke(this, e);
        }

        async Task PingForever()
        {
            Events.Log.PingPongManagerStarted(_guid, (int)_keepAliveInterval.TotalSeconds);

            try
            {
                while (!_cancellationToken.IsCancellationRequested)
                {
                    await Task.Delay(_keepAliveInterval, _cancellationToken);

                    if (_webSocket.State != WebSocketState.Open)
                    {
                        break;
                    }

                    if (_pingSentTicks != 0)
                    {
                        Events.Log.KeepAliveIntervalExpired(_guid, (int)_keepAliveInterval.TotalSeconds);
                        await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, $"No Pong message received in response to a Ping after KeepAliveInterval {_keepAliveInterval}", _cancellationToken);
                        break;
                    }

                    if (!_cancellationToken.IsCancellationRequested)
                    {
                        _pingSentTicks = _stopwatch.Elapsed.Ticks;
                        ArraySegment<byte> buffer = new ArraySegment<byte>(BitConverter.GetBytes(_pingSentTicks));
                        await SendPing(buffer, _cancellationToken);
                    }
                }
            }
            catch (OperationCanceledException)
            {
            }

            Events.Log.PingPongManagerEnded(_guid);
        }

        void WebSocketImpl_Pong(object sender, PongEventArgs e)
        {
            _pingSentTicks = 0;
            OnPong(e);
        }
    }
}
