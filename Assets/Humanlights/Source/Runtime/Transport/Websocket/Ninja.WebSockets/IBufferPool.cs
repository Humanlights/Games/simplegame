using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ninja.WebSockets
{
    public interface IBufferPool
    {
        MemoryStream GetBuffer();
    }
}
