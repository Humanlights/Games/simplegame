using System;

namespace Ninja.WebSockets
{
    public class WebSocketServerOptions
    {
        public TimeSpan KeepAliveInterval { get; set; }

        public bool IncludeExceptionInCloseResponse { get; set; }

        public string SubProtocol { get; set; }

        public WebSocketServerOptions()
        {
            KeepAliveInterval = TimeSpan.FromSeconds(60);
            IncludeExceptionInCloseResponse = false;
            SubProtocol = null;
        }
    }
}
