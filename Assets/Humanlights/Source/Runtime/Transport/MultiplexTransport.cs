using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public class MultiplexTransport : Transport
    {
        public Transport[] transports;

        List<int>[] recipientsCache;

        public void Awake()
        {
            if (transports == null || transports.Length == 0)
            {
                Debug.LogError("Multiplex transport requires at least 1 underlying transport");
            }
            InitClient();
            InitServer();
        }

        public override bool Available()
        {
            foreach (Transport transport in transports)
            {
                if (transport.Available())
                {
                    return true;
                }
            }
            return false;
        }

        #region Client
        void InitClient()
        {
            foreach (Transport transport in transports)
            {
                transport.OnClientConnected.AddListener(OnClientConnected.Invoke );
                transport.OnClientDataReceived.AddListener(OnClientDataReceived.Invoke);
                transport.OnClientError.AddListener(OnClientError.Invoke );
                transport.OnClientDisconnected.AddListener(OnClientDisconnected.Invoke);
            }
        }

        Transport GetAvailableTransport()
        {
            foreach (Transport transport in transports)
            {
                if (transport.Available())
                {
                    return transport;
                }
            }
            throw new Exception("No transport suitable for this platform");
        }

        public override void ClientConnect(string address)
        {
            GetAvailableTransport().ClientConnect(address);
        }

        public override bool ClientConnected()
        {
            return GetAvailableTransport().ClientConnected();
        }

        public override void ClientDisconnect()
        {
            GetAvailableTransport().ClientDisconnect();
        }

        public override bool ClientSend(int channelId, ArraySegment<byte> segment)
        {
            return GetAvailableTransport().ClientSend(channelId, segment);
        }

        public override int GetMaxPacketSize(int channelId = 0)
        {
            return GetAvailableTransport().GetMaxPacketSize(channelId);
        }

        #endregion


        #region Server
        int FromBaseId(int transportId, int connectionId)
        {
            return connectionId * transports.Length + transportId;
        }

        int ToBaseId(int connectionId)
        {
            return connectionId / transports.Length;
        }

        int ToTransportId(int connectionId)
        {
            return connectionId % transports.Length;
        }

        void InitServer()
        {
            recipientsCache = new List<int>[transports.Length];

            for (int i = 0; i < transports.Length; i++)
            {
                recipientsCache[i] = new List<int>();

                int locali = i;
                Transport transport = transports[i];

                transport.OnServerConnected.AddListener(baseConnectionId =>
                {
                    OnServerConnected.Invoke(FromBaseId(locali, baseConnectionId));
                });

                transport.OnServerDataReceived.AddListener((baseConnectionId, data, channel) =>
                {
                    OnServerDataReceived.Invoke(FromBaseId(locali, baseConnectionId), data, channel);
                });

                transport.OnServerError.AddListener((baseConnectionId, error) =>
                {
                    OnServerError.Invoke(FromBaseId(locali, baseConnectionId), error);
                });
                transport.OnServerDisconnected.AddListener(baseConnectionId =>
                {
                    OnServerDisconnected.Invoke(FromBaseId(locali, baseConnectionId));
                });
            }
        }

        public override bool ServerActive()
        {
            foreach (Transport transport in transports)
            {
                if (!transport.ServerActive())
                {
                    return false;
                }
            }
            return true;
        }

        public override string ServerGetClientAddress(int connectionId)
        {
            int baseConnectionId = ToBaseId(connectionId);
            int transportId = ToTransportId(connectionId);
            return transports[transportId].ServerGetClientAddress(baseConnectionId);
        }

        public override bool ServerDisconnect(int connectionId)
        {
            int baseConnectionId = ToBaseId(connectionId);
            int transportId = ToTransportId(connectionId);
            return transports[transportId].ServerDisconnect(baseConnectionId);
        }

        public override bool ServerSend(List<int> connectionIds, int channelId, ArraySegment<byte> segment)
        {
            foreach (List<int> list in recipientsCache)
            {
                list.Clear();
            }

            foreach (int connectionId in connectionIds)
            {
                int baseConnectionId = ToBaseId(connectionId);
                int transportId = ToTransportId(connectionId);
                recipientsCache[transportId].Add(baseConnectionId);
            }

            bool result = true;
            for (int i = 0; i < transports.Length; ++i)
            {
                List<int> baseRecipients = recipientsCache[i];
                if (baseRecipients.Count > 0)
                {
                    result &= transports[i].ServerSend(baseRecipients, channelId, segment);
                }
            }
            return result;
        }

        public override void ServerStart()
        {
            foreach (Transport transport in transports)
            {
                transport.ServerStart();
            }
        }

        public override void ServerStop()
        {
            foreach (Transport transport in transports)
            {
                transport.ServerStop();
            }
        }
        #endregion

        public override void Shutdown()
        {
            foreach (Transport transport in transports)
            {
                transport.Shutdown();
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (Transport transport in transports)
            {
                builder.AppendLine(transport.ToString());
            }
            return builder.ToString().Trim();
        }
    }
}
