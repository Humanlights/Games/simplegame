using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Events;

namespace Humanlights.Unity.UNet
{
    [Serializable] public class ClientDataReceivedEvent : UnityEvent<ArraySegment<byte>, int> { }
    [Serializable] public class UnityEventException : UnityEvent<Exception> { }
    [Serializable] public class UnityEventInt : UnityEvent<int> { }
    [Serializable] public class ServerDataReceivedEvent : UnityEvent<int, ArraySegment<byte>, int> { }
    [Serializable] public class UnityEventIntException : UnityEvent<int, Exception> { }

    public abstract class Transport : MonoBehaviour
    {
        public static Transport activeTransport;

        public abstract bool Available ();

        #region Client
        [HideInInspector] public UnityEvent OnClientConnected = new UnityEvent ();

        [HideInInspector] public ClientDataReceivedEvent OnClientDataReceived = new ClientDataReceivedEvent ();

        [HideInInspector] public UnityEventException OnClientError = new UnityEventException ();

        [HideInInspector] public UnityEvent OnClientDisconnected = new UnityEvent ();

        public abstract bool ClientConnected ();

        public abstract void ClientConnect ( string address );

        public abstract bool ClientSend ( int channelId, ArraySegment<byte> segment );

        public abstract void ClientDisconnect ();

        #endregion

        #region Server

        [HideInInspector] public UnityEventInt OnServerConnected = new UnityEventInt ();

        [HideInInspector] public ServerDataReceivedEvent OnServerDataReceived = new ServerDataReceivedEvent ();

        [HideInInspector] public UnityEventIntException OnServerError = new UnityEventIntException ();

        [HideInInspector] public UnityEventInt OnServerDisconnected = new UnityEventInt ();

        public abstract bool ServerActive ();

        public abstract void ServerStart ();

        public abstract bool ServerSend ( List<int> connectionIds, int channelId, ArraySegment<byte> segment );

        public abstract bool ServerDisconnect ( int connectionId );

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use ServerGetClientAddress(int connectionId) instead" )]
        public virtual bool GetConnectionInfo ( int connectionId, out string address )
        {
            address = ServerGetClientAddress ( connectionId );
            return true;
        }

        public abstract string ServerGetClientAddress ( int connectionId );

        public abstract void ServerStop ();


        #endregion

        public abstract void Shutdown ();

        public abstract int GetMaxPacketSize ( int channelId = Channels.DefaultReliable );

        public void Update () { }
    }
}
