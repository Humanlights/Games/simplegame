using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public static class NetworkServer
    {
        static bool initialized;
        static int maxConnections;

        public static NetworkConnectionToClient localConnection { get; private set; }

        public static Dictionary<int, NetworkConnectionToClient> connections = new Dictionary<int, NetworkConnectionToClient> ();

        public static Dictionary<int, NetworkMessageDelegate> handlers = new Dictionary<int, NetworkMessageDelegate> ();

        public static bool dontListen;

        public static bool active { get; private set; }

        public static bool localClientActive { get; private set; }

        static readonly List<int> connectionIdsCache = new List<int> ();

        public static void Reset ()
        {
            active = false;
        }

        public static void Shutdown ()
        {
            if ( initialized )
            {
                DisconnectAll ();

                if ( dontListen )
                {
                }
                else
                {
                    Transport.activeTransport.ServerStop ();
                }

                Transport.activeTransport.OnServerDisconnected.RemoveListener ( OnDisconnected );
                Transport.activeTransport.OnServerConnected.RemoveListener ( OnConnected );
                Transport.activeTransport.OnServerDataReceived.RemoveListener ( OnDataReceived );
                Transport.activeTransport.OnServerError.RemoveListener ( OnError );

                initialized = false;
            }
            dontListen = false;
            active = false;

            NetworkIdentity.ResetNextNetworkId ();
        }

        static void Initialize ()
        {
            if ( initialized )
                return;

            initialized = true;
            if ( LogFilter.Debug ) Debug.Log ( "NetworkServer Created version " + Version.Current );

            connections.Clear ();
            Transport.activeTransport.OnServerDisconnected.AddListener ( OnDisconnected );
            Transport.activeTransport.OnServerConnected.AddListener ( OnConnected );
            Transport.activeTransport.OnServerDataReceived.AddListener ( OnDataReceived );
            Transport.activeTransport.OnServerError.AddListener ( OnError );
        }

        internal static void RegisterMessageHandlers ()
        {
            RegisterHandler<ReadyMessage> ( OnClientReadyMessage );
            RegisterHandler<CommandMessage> ( OnCommandMessage );
            RegisterHandler<RemovePlayerMessage> ( OnRemovePlayerMessage );
            RegisterHandler<NetworkPingMessage> ( NetworkTime.OnServerPing, false );
        }

        public static bool Listen ( int maxConns )
        {
            Initialize ();
            maxConnections = maxConns;

            if ( !dontListen )
            {
                Transport.activeTransport.ServerStart ();
                if ( LogFilter.Debug ) Debug.Log ( "Server started listening" );
            }

            active = true;
            RegisterMessageHandlers ();
            return true;
        }

        public static bool AddConnection ( NetworkConnectionToClient conn )
        {
            if ( !connections.ContainsKey ( conn.connectionId ) )
            {
                connections [ conn.connectionId ] = conn;
                conn.SetHandlers ( handlers );
                return true;
            }
            return false;
        }

        public static bool RemoveConnection ( int connectionId )
        {
            return connections.Remove ( connectionId );
        }

        internal static void SetLocalConnection ( ULocalConnectionToClient conn )
        {
            if ( localConnection != null )
            {
                // Debug.LogError("Local Connection already exists");
                return;
            }

            localConnection = conn;
            OnConnected ( localConnection );
        }

        internal static void RemoveLocalConnection ()
        {
            if ( localConnection != null )
            {
                localConnection.Disconnect ();
                localConnection = null;
            }
            localClientActive = false;
            RemoveConnection ( 0 );
        }

        internal static void ActivateLocalClientScene ()
        {
            if ( localClientActive )
                return;

            localClientActive = true;
            foreach ( NetworkIdentity identity in NetworkIdentity.spawned.Values )
            {
                if ( !identity.isClient )
                {
                    if ( LogFilter.Debug ) Debug.Log ( "ActivateClientScene " + identity.netId + " " + identity );

                    identity.OnStartClient ();
                }
            }
        }

        static bool SendToObservers<T> ( NetworkIdentity identity, T msg ) where T : IMessageBase
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server.SendToObservers id:" + typeof ( T ) );

            if ( identity != null && identity.observers != null )
            {
                NetworkWriter writer = NetworkWriterPool.GetWriter ();

                MessagePacker.Pack ( msg, writer );
                ArraySegment<byte> segment = writer.ToArraySegment ();

                connectionIdsCache.Clear ();
                bool result = true;
                foreach ( KeyValuePair<int, NetworkConnection> kvp in identity.observers )
                {
                    if ( kvp.Value is ULocalConnectionToClient )
                        result &= localConnection.Send ( segment );
                    else
                        connectionIdsCache.Add ( kvp.Key );
                }

                if ( connectionIdsCache.Count > 0 )
                    result &= NetworkConnectionToClient.Send ( connectionIdsCache, segment );
                NetworkDiagnostics.OnSend ( msg, Channels.DefaultReliable, segment.Count, identity.observers.Count );

                NetworkWriterPool.Recycle ( writer );
                return result;
            }
            return false;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use SendToAll<T> instead." )]
        public static bool SendToAll ( int msgType, MessageBase msg, int channelId = Channels.DefaultReliable )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server.SendToAll id:" + msgType );

            byte [] bytes = MessagePacker.PackMessage ( ( ushort ) msgType, msg );

            bool result = true;
            foreach ( KeyValuePair<int, NetworkConnectionToClient> kvp in connections )
            {
                result &= kvp.Value.Send ( new ArraySegment<byte> ( bytes ), channelId );
            }
            return result;
        }

        public static bool SendToAll<T> ( T msg, int channelId = Channels.DefaultReliable ) where T : IMessageBase
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server.SendToAll id:" + typeof ( T ) );


            NetworkWriter writer = NetworkWriterPool.GetWriter ();

            MessagePacker.Pack ( msg, writer );
            ArraySegment<byte> segment = writer.ToArraySegment ();

            connectionIdsCache.Clear ();
            bool result = true;
            foreach ( KeyValuePair<int, NetworkConnectionToClient> kvp in connections )
            {
                if ( kvp.Value is ULocalConnectionToClient )
                    result &= localConnection.Send ( segment );
                else
                    connectionIdsCache.Add ( kvp.Key );
            }

            if ( connectionIdsCache.Count > 0 )
                result &= NetworkConnectionToClient.Send ( connectionIdsCache, segment );
            NetworkDiagnostics.OnSend ( msg, channelId, segment.Count, connections.Count );

            NetworkWriterPool.Recycle ( writer );
            return result;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use SendToReady<T> instead." )]
        public static bool SendToReady ( NetworkIdentity identity, short msgType, MessageBase msg, int channelId = Channels.DefaultReliable )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server.SendToReady msgType:" + msgType );

            if ( identity != null && identity.observers != null )
            {
                byte [] bytes = MessagePacker.PackMessage ( ( ushort ) msgType, msg );

                bool result = true;
                foreach ( KeyValuePair<int, NetworkConnection> kvp in identity.observers )
                {
                    if ( kvp.Value.isReady )
                    {
                        result &= kvp.Value.Send ( new ArraySegment<byte> ( bytes ), channelId );
                    }
                }
                return result;
            }
            return false;
        }

        public static bool SendToReady<T> ( NetworkIdentity identity, T msg, bool includeOwner = true, int channelId = Channels.DefaultReliable ) where T : IMessageBase
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server.SendToReady msgType:" + typeof ( T ) );

            if ( identity != null && identity.observers != null )
            {
                NetworkWriter writer = NetworkWriterPool.GetWriter ();

                MessagePacker.Pack ( msg, writer );
                ArraySegment<byte> segment = writer.ToArraySegment ();

                connectionIdsCache.Clear ();
                bool result = true;
                int count = 0;
                foreach ( KeyValuePair<int, NetworkConnection> kvp in identity.observers )
                {
                    bool isOwner = kvp.Value == identity.connectionToClient;
                    if ( ( !isOwner || includeOwner ) && kvp.Value.isReady )
                    {
                        count++;

                        if ( kvp.Value is ULocalConnectionToClient )
                            result &= localConnection.Send ( segment );
                        else
                            connectionIdsCache.Add ( kvp.Key );
                    }
                }

                if ( connectionIdsCache.Count > 0 )
                    result &= NetworkConnectionToClient.Send ( connectionIdsCache, segment );
                NetworkDiagnostics.OnSend ( msg, channelId, segment.Count, count );

                NetworkWriterPool.Recycle ( writer );
                return result;
            }
            return false;
        }

        public static bool SendToReady<T> ( NetworkIdentity identity, T msg, int channelId = Channels.DefaultReliable ) where T : IMessageBase
        {
            return SendToReady ( identity, msg, true, channelId );
        }

        public static void DisconnectAll ()
        {
            DisconnectAllConnections ();
            localConnection = null;

            active = false;
            localClientActive = false;
        }

        public static void DisconnectAllConnections ()
        {
            foreach ( NetworkConnection conn in connections.Values )
            {
                conn.Disconnect ();
                if ( conn.connectionId != 0 )
                    OnDisconnected ( conn );
            }
            connections.Clear ();
        }

        internal static void Update ()
        {
            if ( !active )
                return;

            foreach ( KeyValuePair<uint, NetworkIdentity> kvp in NetworkIdentity.spawned )
            {
                if ( kvp.Value != null && kvp.Value.gameObject != null )
                {
                    kvp.Value.MirrorUpdate ();
                }
                else
                {
                    Debug.LogWarning ( "Found 'null' entry in spawned list for netId=" + kvp.Key + ". Please call NetworkServer.Destroy to destroy networked objects. Don't use GameObject.Destroy." );
                }
            }
        }

        static void OnConnected ( int connectionId )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server accepted client:" + connectionId );

            if ( connectionId <= 0 )
            {
                Debug.LogError ( "Server.HandleConnect: invalid connectionId: " + connectionId + " . Needs to be >0, because 0 is reserved for local player." );
                Transport.activeTransport.ServerDisconnect ( connectionId );
                return;
            }

            if ( connections.ContainsKey ( connectionId ) )
            {
                Transport.activeTransport.ServerDisconnect ( connectionId );
                if ( LogFilter.Debug ) Debug.Log ( "Server connectionId " + connectionId + " already in use. kicked client:" + connectionId );
                return;
            }

            if ( connections.Count < maxConnections )
            {
                NetworkConnectionToClient conn = new NetworkConnectionToClient ( connectionId );
                OnConnected ( conn );
            }
            else
            {
                Transport.activeTransport.ServerDisconnect ( connectionId );
                if ( LogFilter.Debug ) Debug.Log ( "Server full, kicked client:" + connectionId );
            }
        }

        static void OnConnected ( NetworkConnectionToClient conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server accepted client:" + conn );

            AddConnection ( conn );
            conn.InvokeHandler ( new ConnectMessage (), -1 );
        }

        static void OnDisconnected ( int connectionId )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Server disconnect client:" + connectionId );

            if ( connections.TryGetValue ( connectionId, out NetworkConnectionToClient conn ) )
            {
                conn.Disconnect ();
                RemoveConnection ( connectionId );
                if ( LogFilter.Debug ) Debug.Log ( "Server lost client:" + connectionId );

                OnDisconnected ( conn );
            }
        }

        static void OnDisconnected ( NetworkConnection conn )
        {
            conn.InvokeHandler ( new DisconnectMessage (), -1 );
            if ( LogFilter.Debug ) Debug.Log ( "Server lost client:" + conn );
        }

        static void OnDataReceived ( int connectionId, ArraySegment<byte> data, int channelId )
        {
            if ( connections.TryGetValue ( connectionId, out NetworkConnectionToClient conn ) )
            {
                conn.TransportReceive ( data, channelId );
            }
            else
            {
                Debug.LogError ( "HandleData Unknown connectionId:" + connectionId );
            }
        }

        static void OnError ( int connectionId, Exception exception )
        {
            Debug.LogException ( exception );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use RegisterHandler<T>(Action<NetworkConnection, T>, bool) instead." )]
        public static void RegisterHandler ( int msgType, NetworkMessageDelegate handler )
        {
            if ( handlers.ContainsKey ( msgType ) )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkServer.RegisterHandler replacing " + msgType );
            }
            handlers [ msgType ] = handler;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use RegisterHandler<T>(Action<NetworkConnection, T>, bool) instead." )]
        public static void RegisterHandler ( MsgType msgType, NetworkMessageDelegate handler )
        {
            RegisterHandler ( ( int ) msgType, handler );
        }

        public static void RegisterHandler<T> ( Action<NetworkConnection, T> handler, bool requireAuthentication = true ) where T : IMessageBase, new()
        {
            int msgType = MessagePacker.GetId<T> ();
            if ( handlers.ContainsKey ( msgType ) )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkServer.RegisterHandler replacing " + msgType );
            }
            handlers [ msgType ] = MessagePacker.MessageHandler<T> ( handler, requireAuthentication );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use UnregisterHandler<T> instead." )]
        public static void UnregisterHandler ( int msgType )
        {
            handlers.Remove ( msgType );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use UnregisterHandler<T> instead." )]
        public static void UnregisterHandler ( MsgType msgType )
        {
            UnregisterHandler ( ( int ) msgType );
        }

        public static void UnregisterHandler<T> () where T : IMessageBase
        {
            int msgType = MessagePacker.GetId<T> ();
            handlers.Remove ( msgType );
        }

        public static void ClearHandlers ()
        {
            handlers.Clear ();
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use connection.Send(msg) instead." )]
        public static void SendToClient ( int connectionId, int msgType, MessageBase msg )
        {
            if ( connections.TryGetValue ( connectionId, out NetworkConnectionToClient conn ) )
            {
                conn.Send ( msgType, msg );
                return;
            }
            Debug.LogError ( "Failed to send message to connection ID '" + connectionId + ", not found in connection list" );
        }

        [Obsolete ( "Use connection.Send(msg) instead" )]
        public static void SendToClient<T> ( int connectionId, T msg ) where T : IMessageBase
        {
            if ( connections.TryGetValue ( connectionId, out NetworkConnectionToClient conn ) )
            {
                conn.Send ( msg );
                return;
            }
            Debug.LogError ( "Failed to send message to connection ID '" + connectionId + ", not found in connection list" );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use SendToClientOfPlayer<T> instead." )]
        public static void SendToClientOfPlayer ( NetworkIdentity identity, int msgType, MessageBase msg )
        {
            if ( identity != null )
            {
                identity.connectionToClient.Send ( msgType, msg );
            }
            else
            {
                Debug.LogError ( "SendToClientOfPlayer: player has no NetworkIdentity: " + identity.name );
            }
        }

        public static void SendToClientOfPlayer<T> ( NetworkIdentity identity, T msg ) where T : IMessageBase
        {
            if ( identity != null )
            {
                identity.connectionToClient.Send ( msg );
            }
            else
            {
                Debug.LogError ( "SendToClientOfPlayer: player has no NetworkIdentity: " + identity.name );
            }
        }

        public static bool ReplacePlayerForConnection ( NetworkConnection conn, GameObject player, Guid assetId )
        {
            if ( GetNetworkIdentity ( player, out NetworkIdentity identity ) )
            {
                identity.assetId = assetId;
            }
            return InternalReplacePlayerForConnection ( conn, player );
        }

        public static bool ReplacePlayerForConnection ( NetworkConnection conn, GameObject player )
        {
            return InternalReplacePlayerForConnection ( conn, player );
        }

        public static bool AddPlayerForConnection ( NetworkConnection conn, GameObject player, Guid assetId )
        {
            if ( GetNetworkIdentity ( player, out NetworkIdentity identity ) )
            {
                identity.assetId = assetId;
            }
            return AddPlayerForConnection ( conn, player );
        }

        static void SpawnObserversForConnection ( NetworkConnection conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Spawning " + NetworkIdentity.spawned.Count + " objects for conn " + conn );

            if ( !conn.isReady )
            {
                return;
            }

            conn.Send ( new ObjectSpawnStartedMessage () );

            foreach ( NetworkIdentity identity in NetworkIdentity.spawned.Values )
            {
                if ( identity.gameObject.activeSelf )
                {
                    if ( LogFilter.Debug ) Debug.Log ( "Sending spawn message for current server objects name='" + identity.name + "' netId=" + identity.netId + " sceneId=" + identity.sceneId );

                    bool visible = identity.OnCheckObserver ( conn );
                    if ( visible )
                    {
                        identity.AddObserver ( conn );
                    }
                }
            }

            conn.Send ( new ObjectSpawnFinishedMessage () );
        }

        public static bool AddPlayerForConnection ( NetworkConnection conn, GameObject player )
        {
            NetworkIdentity identity = player.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.Log ( "AddPlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + player );
                return false;
            }
            identity.Reset ();

            if ( conn.identity != null )
            {
                Debug.Log ( "AddPlayer: player object already exists" );
                return false;
            }

            conn.identity = identity;

            identity.connectionToClient = conn;

            SetClientReady ( conn );

            if ( SetupLocalPlayerForConnection ( conn, identity ) )
            {
                return true;
            }

            if ( LogFilter.Debug ) Debug.Log ( "Adding new playerGameObject object netId: " + identity.netId + " asset ID " + identity.assetId );

            FinishPlayerForConnection ( identity, player );
            identity.SetClientOwner ( conn );
            return true;
        }

        static bool SetupLocalPlayerForConnection ( NetworkConnection conn, NetworkIdentity identity )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkServer SetupLocalPlayerForConnection netID:" + identity.netId );

            if ( conn is ULocalConnectionToClient )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkServer AddPlayer handling ULocalConnectionToClient" );

                if ( identity.netId == 0 )
                {
                    identity.OnStartServer ( true );
                }
                identity.RebuildObservers ( true );
                SendSpawnMessage ( identity, null );

                NetworkClient.AddLocalPlayer ( identity );
                identity.SetClientOwner ( conn );

                identity.SetLocalPlayer ();
                return true;
            }
            return false;
        }

        static void FinishPlayerForConnection ( NetworkIdentity identity, GameObject playerGameObject )
        {
            if ( identity.netId == 0 )
            {
                Spawn ( playerGameObject );
            }
        }

        internal static bool InternalReplacePlayerForConnection ( NetworkConnection conn, GameObject player )
        {
            NetworkIdentity identity = player.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "ReplacePlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + player );
                return false;
            }

            if ( LogFilter.Debug ) Debug.Log ( "NetworkServer ReplacePlayer" );

            if ( conn.identity != null )
            {
                conn.identity.SetNotLocalPlayer ();
                conn.identity.clientAuthorityOwner = null;
            }

            conn.identity = identity;

            identity.connectionToClient = conn;


            SpawnObserversForConnection ( conn );

            if ( LogFilter.Debug ) Debug.Log ( "NetworkServer ReplacePlayer setup local" );

            if ( SetupLocalPlayerForConnection ( conn, identity ) )
            {
                return true;
            }

            if ( LogFilter.Debug ) Debug.Log ( "Replacing playerGameObject object netId: " + player.GetComponent<NetworkIdentity> ().netId + " asset ID " + player.GetComponent<NetworkIdentity> ().assetId );

            FinishPlayerForConnection ( identity, player );
            identity.SetClientOwner ( conn );
            return true;
        }

        static bool GetNetworkIdentity ( GameObject go, out NetworkIdentity identity )
        {
            identity = go.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "GameObject " + go.name + " doesn't have NetworkIdentity." );
                return false;
            }
            return true;
        }

        public static void SetClientReady ( NetworkConnection conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "SetClientReadyInternal for conn:" + conn );

            conn.isReady = true;

            if ( conn.identity != null )
                SpawnObserversForConnection ( conn );
        }

        internal static void ShowForConnection ( NetworkIdentity identity, NetworkConnection conn )
        {
            if ( conn.isReady )
                SendSpawnMessage ( identity, conn );
        }

        internal static void HideForConnection ( NetworkIdentity identity, NetworkConnection conn )
        {
            ObjectHideMessage msg = new ObjectHideMessage
            {
                netId = identity.netId
            };
            conn.Send ( msg );
        }

        public static void SetAllClientsNotReady ()
        {
            foreach ( NetworkConnection conn in connections.Values )
            {
                SetClientNotReady ( conn );
            }
        }

        public static void SetClientNotReady ( NetworkConnection conn )
        {
            if ( conn.isReady )
            {
                if ( LogFilter.Debug ) Debug.Log ( "PlayerNotReady " + conn );
                conn.isReady = false;
                conn.RemoveObservers ();

                conn.Send ( new NotReadyMessage () );
            }
        }

        static void OnClientReadyMessage ( NetworkConnection conn, ReadyMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "Default handler for ready message from " + conn );
            SetClientReady ( conn );
        }

        static void OnRemovePlayerMessage ( NetworkConnection conn, RemovePlayerMessage msg )
        {
            if ( conn.identity != null )
            {
                Destroy ( conn.identity.gameObject );
                conn.identity = null;
            }
            else
            {
                Debug.LogError ( "Received remove player message but connection has no player" );
            }
        }

        static void OnCommandMessage ( NetworkConnection conn, CommandMessage msg )
        {
            if ( !NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity identity ) )
            {
                // Debug.LogWarning ( "Spawned object not found when handling Command message [netId=" + msg.netId + "]" );
                return;
            }

            if ( conn.identity != null && conn.identity.netId != identity.netId )
            {
                if ( identity.clientAuthorityOwner != conn )
                {
                    // Debug.LogWarning ( "Command for object without authority [netId=" + msg.netId + "]" );
                    return;
                }
            }

            if ( LogFilter.Debug ) Debug.Log ( "OnCommandMessage for netId=" + msg.netId + " conn=" + conn );
            identity.HandleCommand ( msg.componentIndex, msg.functionHash, new NetworkReader ( msg.payload ) );
        }

        internal static void SpawnObject ( GameObject obj )
        {
            if ( !active )
            {
                Debug.LogError ( "SpawnObject for " + obj + ", NetworkServer is not active. Cannot spawn objects without an active server." );
                return;
            }

            NetworkIdentity identity = obj.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "SpawnObject " + obj + " has no NetworkIdentity. Please add a NetworkIdentity to " + obj );
                return;
            }
            identity.Reset ();

            identity.OnStartServer ( false );

            if ( LogFilter.Debug ) Debug.Log ( "SpawnObject instance ID " + identity.netId + " asset ID " + identity.assetId );

            identity.RebuildObservers ( true );
        }

        internal static void SendSpawnMessage ( NetworkIdentity identity, NetworkConnection conn )
        {
            if ( identity.serverOnly )
                return;

            if ( LogFilter.Debug ) Debug.Log ( "Server SendSpawnMessage: name=" + identity.name + " sceneId=" + identity.sceneId.ToString ( "X" ) + " netid=" + identity.netId );

            NetworkWriter ownerWriter = NetworkWriterPool.GetWriter ();
            NetworkWriter observersWriter = NetworkWriterPool.GetWriter ();


            identity.OnSerializeAllSafely ( true, ownerWriter, out int ownerWritten, observersWriter, out int observersWritten );

            ArraySegment<byte> ownerSegment = ownerWritten > 0 ? ownerWriter.ToArraySegment () : default;
            ArraySegment<byte> observersSegment = observersWritten > 0 ? observersWriter.ToArraySegment () : default;

            if ( identity.sceneId == 0 )
            {
                SpawnMessage msg = new SpawnMessage
                {
                    netId = identity.netId,
                    isLocalPlayer = conn?.identity == identity,
                    assetId = identity.assetId,
                    position = identity.transform.localPosition,
                    rotation = identity.transform.localRotation,
                    scale = identity.transform.localScale
                };

                if ( conn != null )
                {
                    bool isOwner = identity.connectionToClient == conn;
                    msg.payload = isOwner ? ownerSegment : observersSegment;

                    conn.Send ( msg );
                }
                else
                {
                    msg.payload = ownerSegment;
                    SendToClientOfPlayer ( identity, msg );

                    msg.payload = observersSegment;
                    SendToReady ( identity, msg, false );
                }
            }
            else
            {
                SpawnMessage msg = new SpawnMessage
                {
                    netId = identity.netId,
                    isLocalPlayer = conn?.identity == identity,
                    sceneId = identity.sceneId,
                    position = identity.transform.localPosition,
                    rotation = identity.transform.localRotation,
                    scale = identity.transform.localScale
                };

                if ( conn != null )
                {
                    bool isOwner = identity.connectionToClient == conn;
                    msg.payload = isOwner ? ownerSegment : observersSegment;

                    conn.Send ( msg );
                }
                else
                {
                    msg.payload = ownerSegment;
                    SendToClientOfPlayer ( identity, msg );

                    msg.payload = observersSegment;
                    SendToReady ( identity, msg, false );
                }
            }

            NetworkWriterPool.Recycle ( ownerWriter );
            NetworkWriterPool.Recycle ( observersWriter );
        }

        public static void DestroyPlayerForConnection ( NetworkConnection conn )
        {
            if ( conn.identity != null )
            {
                DestroyObject ( conn.identity, true );
                conn.identity = null;
            }
        }

        public static void Spawn ( GameObject obj )
        {
            if ( VerifyCanSpawn ( obj ) )
            {
                SpawnObject ( obj );
            }
        }

        static bool CheckForPrefab ( GameObject obj )
        {
#if UNITY_EDITOR
#if UNITY_2018_3_OR_NEWER
            return UnityEditor.PrefabUtility.IsPartOfPrefabAsset ( obj );
#elif UNITY_2018_2_OR_NEWER
            return (UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(obj) == null) && (UnityEditor.PrefabUtility.GetPrefabObject(obj) != null);
#else
            return (UnityEditor.PrefabUtility.GetPrefabParent(obj) == null) && (UnityEditor.PrefabUtility.GetPrefabObject(obj) != null);
#endif
#else
            return false;
#endif
        }

        static bool VerifyCanSpawn ( GameObject obj )
        {
            if ( CheckForPrefab ( obj ) )
            {
                Debug.LogErrorFormat ( "GameObject {0} is a prefab, it can't be spawned. This will cause errors in builds.", obj.name );
                return false;
            }

            return true;
        }

        public static bool SpawnWithClientAuthority ( GameObject obj, GameObject player )
        {
            NetworkIdentity identity = player.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "SpawnWithClientAuthority player object has no NetworkIdentity" );
                return false;
            }

            if ( identity.connectionToClient == null )
            {
                Debug.LogError ( "SpawnWithClientAuthority player object is not a player." );
                return false;
            }

            return SpawnWithClientAuthority ( obj, identity.connectionToClient );
        }

        public static bool SpawnWithClientAuthority ( GameObject obj, NetworkConnection conn )
        {
            if ( !conn.isReady )
            {
                Debug.LogError ( "SpawnWithClientAuthority NetworkConnection is not ready!" );
                return false;
            }

            Spawn ( obj );

            NetworkIdentity identity = obj.GetComponent<NetworkIdentity> ();
            if ( identity == null || !identity.isServer )
            {
                return false;
            }

            return identity.AssignClientAuthority ( conn );
        }

        public static bool SpawnWithClientAuthority ( GameObject obj, Guid assetId, NetworkConnection conn )
        {
            Spawn ( obj, assetId );

            NetworkIdentity identity = obj.GetComponent<NetworkIdentity> ();
            if ( identity == null || !identity.isServer )
            {
                return false;
            }

            return identity.AssignClientAuthority ( conn );
        }

        public static void Spawn ( GameObject obj, Guid assetId )
        {
            if ( VerifyCanSpawn ( obj ) )
            {
                if ( GetNetworkIdentity ( obj, out NetworkIdentity identity ) )
                {
                    identity.assetId = assetId;
                }
                SpawnObject ( obj );
            }
        }

        static void DestroyObject ( NetworkIdentity identity, bool destroyServerObject )
        {
            if ( LogFilter.Debug ) Debug.Log ( "DestroyObject instance:" + identity.netId );
            NetworkIdentity.spawned.Remove ( identity.netId );

            identity.clientAuthorityOwner?.RemoveOwnedObject ( identity );

            ObjectDestroyMessage msg = new ObjectDestroyMessage
            {
                netId = identity.netId
            };
            SendToObservers ( identity, msg );

            identity.ClearObservers ();
            if ( NetworkClient.active && localClientActive )
            {
                identity.OnNetworkDestroy ();
            }

            if ( destroyServerObject )
            {
                UnityEngine.Object.Destroy ( identity.gameObject );
            }
            identity.MarkForReset ();
        }

        public static void Destroy ( GameObject obj )
        {
            if ( obj == null )
            {
                // if ( LogFilter.Debug ) Debug.Log ( "NetworkServer DestroyObject is null" );
                return;
            }

            if ( GetNetworkIdentity ( obj, out NetworkIdentity identity ) )
            {
                DestroyObject ( identity, true );
            }
        }

        public static void UnSpawn ( GameObject obj )
        {
            if ( obj == null )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkServer UnspawnObject is null" );
                return;
            }

            if ( GetNetworkIdentity ( obj, out NetworkIdentity identity ) )
            {
                DestroyObject ( identity, false );
            }
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkIdentity.spawned[netId] instead." )]
        public static GameObject FindLocalObject ( uint netId )
        {
            if ( NetworkIdentity.spawned.TryGetValue ( netId, out NetworkIdentity identity ) )
            {
                return identity.gameObject;
            }
            return null;
        }

        static bool ValidateSceneObject ( NetworkIdentity identity )
        {
            if ( identity.gameObject.hideFlags == HideFlags.NotEditable || identity.gameObject.hideFlags == HideFlags.HideAndDontSave )
                return false;

#if UNITY_EDITOR
            if ( UnityEditor.EditorUtility.IsPersistent ( identity.gameObject ) )
                return false;
#endif

            return identity.sceneId != 0;
        }

        public static bool SpawnObjects ()
        {
            if ( !active )
                return true;

            NetworkIdentity [] identities = Resources.FindObjectsOfTypeAll<NetworkIdentity> ();
            foreach ( NetworkIdentity identity in identities )
            {
                if ( ValidateSceneObject ( identity ) )
                {
                    if ( LogFilter.Debug ) Debug.Log ( "SpawnObjects sceneId:" + identity.sceneId.ToString ( "X" ) + " name:" + identity.gameObject.name );
                    identity.Reset ();
                    identity.gameObject.SetActive ( true );
                }
            }

            foreach ( NetworkIdentity identity in identities )
            {
                if ( ValidateSceneObject ( identity ) )
                    Spawn ( identity.gameObject );
            }
            return true;
        }
    }
}
