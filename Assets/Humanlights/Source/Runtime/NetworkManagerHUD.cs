using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [AddComponentMenu ( "Network/NetworkManagerHUD" )]
    [RequireComponent ( typeof ( NetworkManager ) )]
    [EditorBrowsable ( EditorBrowsableState.Never )]
    public class NetworkManagerHUD : MonoBehaviour
    {
        NetworkManager manager;

        public bool showGUI = true;

        public int offsetX;

        public int offsetY;

        void Awake ()
        {
            manager = GetComponent<NetworkManager> ();
        }

        void OnGUI ()
        {
            if ( !showGUI )
                return;

            GUILayout.BeginArea ( new Rect ( 10 + offsetX, 40 + offsetY, 215, 9999 ) );
            if ( !NetworkClient.isConnected && !NetworkServer.active )
            {
                if ( !NetworkClient.active )
                {
                    if ( Application.platform != RuntimePlatform.WebGLPlayer )
                    {
                        if ( GUILayout.Button ( "LAN Host" ) )
                        {
                            manager.StartHost ();
                        }
                    }

                    GUILayout.BeginHorizontal ();
                    if ( GUILayout.Button ( "LAN Client" ) )
                    {
                        manager.StartClient ();
                    }
                    manager.networkAddress = GUILayout.TextField ( manager.networkAddress );
                    GUILayout.EndHorizontal ();

                    if ( Application.platform == RuntimePlatform.WebGLPlayer )
                    {
                        GUILayout.Box ( "(  WebGL cannot be server  )" );
                    }
                    else
                    {
                        if ( GUILayout.Button ( "LAN Server Only" ) ) manager.StartServer ();
                    }
                }
                else
                {
                    GUILayout.Label ( "Connecting to " + manager.networkAddress + ".." );
                    if ( GUILayout.Button ( "Cancel Connection Attempt" ) )
                    {
                        manager.StopClient ();
                    }
                }
            }
            else
            {
                if ( NetworkServer.active )
                {
                    GUILayout.Label ( "Server: active. Transport: " + Transport.activeTransport );
                }
                if ( NetworkClient.isConnected )
                {
                    GUILayout.Label ( "Client: address=" + manager.networkAddress );
                }
            }

            if ( NetworkClient.isConnected && !ClientScene.ready )
            {
                if ( GUILayout.Button ( "Client Ready" ) )
                {
                    ClientScene.Ready ( NetworkClient.connection );

                    if ( ClientScene.localPlayer == null )
                    {
                        ClientScene.AddPlayer ();
                    }
                }
            }

            if ( NetworkServer.active || NetworkClient.isConnected )
            {
                if ( GUILayout.Button ( "Stop" ) )
                {
                    manager.StopHost ();
                }
            }

            GUILayout.EndArea ();
        }
    }
}
