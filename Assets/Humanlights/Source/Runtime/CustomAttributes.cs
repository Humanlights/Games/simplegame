using System;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkBehaviour.syncInterval field instead. Can be modified in the Inspector too." )]
    [AttributeUsage ( AttributeTargets.Class )]
    public class NetworkSettingsAttribute : Attribute
    {
        public float sendInterval = 0.1f;
    }

    [AttributeUsage ( AttributeTargets.Field )]
    public class SyncVarAttribute : Attribute
    {
        public string hook;
    }

    /// <summary>
    /// Command which is executed on server. The server is executing.
    /// </summary>
    [AttributeUsage ( AttributeTargets.Method )]
    public class ClientCmdAttribute : Attribute
    {
        public int channel = Channels.DefaultReliable;
    }

    /// <summary>
    /// Command which server executes on the clients. What clients execute.
    /// </summary>
    [AttributeUsage ( AttributeTargets.Method )]
    public class ServerCmdAttribute : Attribute
    {
        public int channel = Channels.DefaultReliable;
    }

    [AttributeUsage ( AttributeTargets.Method )]
    public class TargetRpcAttribute : Attribute
    {
        public int channel = Channels.DefaultReliable;
    }

    [AttributeUsage ( AttributeTargets.Event )]
    public class SyncEventAttribute : Attribute
    {
        public int channel = Channels.DefaultReliable;
    }

    [AttributeUsage ( AttributeTargets.Method )]
    public class ServerAttribute : Attribute { }

    [AttributeUsage ( AttributeTargets.Method )]
    public class ServerCallbackAttribute : Attribute { }

    [AttributeUsage ( AttributeTargets.Method )]
    public class ClientAttribute : Attribute { }

    [AttributeUsage ( AttributeTargets.Method )]
    public class ClientCallbackAttribute : Attribute { }

    public class SceneAttribute : PropertyAttribute { }
}
