using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public abstract class NetworkConnection
    {
        public readonly HashSet<NetworkIdentity> visList = new HashSet<NetworkIdentity> ();

        Dictionary<int, NetworkMessageDelegate> messageHandlers;

        public readonly int connectionId;

        public bool isAuthenticated;

        public object authenticationData;

        public bool isReady;

        public abstract string address { get; }

        public float lastMessageTime;

        [Obsolete ( "Use NetworkConnection.identity instead" )]
        public NetworkIdentity playerController
        {
            get
            {
                return identity;
            }
            internal set
            {
                identity = value;
            }
        }

        public NetworkIdentity identity { get; internal set; }

        public bool logNetworkMessages;

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "isConnected will be removed because it's pointless. A NetworkConnection is always connected." )]
        public bool isConnected { get; protected set; }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "hostId will be removed because it's not needed ever since we removed LLAPI as default. It's always 0 for regular connections and -1 for local connections. Use connection.GetType() == typeof(NetworkConnection) to check if it's a regular or local connection." )]
        public int hostId = -1;

        internal NetworkConnection ()
        {
        }

        internal NetworkConnection ( int networkConnectionId )
        {
            connectionId = networkConnectionId;
#pragma warning disable 618
            isConnected = true;
            hostId = 0;
#pragma warning restore 618
        }

        public abstract void Disconnect ();

        internal void SetHandlers ( Dictionary<int, NetworkMessageDelegate> handlers )
        {
            messageHandlers = handlers;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient/NetworkServer.RegisterHandler<T> instead" )]
        public void RegisterHandler ( short msgType, NetworkMessageDelegate handler )
        {
            if ( messageHandlers.ContainsKey ( msgType ) )
            {
                if ( LogFilter.Debug ) Debug.Log ( "NetworkConnection.RegisterHandler replacing " + msgType );
            }
            messageHandlers [ msgType ] = handler;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkClient/NetworkServer.UnregisterHandler<T> instead" )]
        public void UnregisterHandler ( short msgType )
        {
            messageHandlers.Remove ( msgType );
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "use Send<T> instead" )]
        public bool Send ( int msgType, MessageBase msg, int channelId = Channels.DefaultReliable )
        {
            byte [] message = MessagePacker.PackMessage ( msgType, msg );
            return Send ( new ArraySegment<byte> ( message ), channelId );
        }


        public bool Send<T> ( T msg, int channelId = Channels.DefaultReliable ) where T : IMessageBase
        {
            NetworkWriter writer = NetworkWriterPool.GetWriter ();

            MessagePacker.Pack ( msg, writer );
            NetworkDiagnostics.OnSend ( msg, channelId, writer.Position, 1 );
            bool result = Send ( writer.ToArraySegment (), channelId );

            NetworkWriterPool.Recycle ( writer );
            return result;
        }

        protected internal static bool ValidatePacketSize ( ArraySegment<byte> segment, int channelId )
        {
            if ( segment.Count > Transport.activeTransport.GetMaxPacketSize ( channelId ) )
            {
                Debug.LogError ( "NetworkConnection.ValidatePacketSize: cannot send packet larger than " + Transport.activeTransport.GetMaxPacketSize ( channelId ) + " bytes" );
                return false;
            }

            if ( segment.Count == 0 )
            {
                Debug.LogError ( "NetworkConnection.ValidatePacketSize: cannot send zero bytes" );
                return false;
            }

            return true;
        }

        List<int> singleConnectionId = new List<int> { -1 };
        internal abstract bool Send ( ArraySegment<byte> segment, int channelId = Channels.DefaultReliable );

        public override string ToString ()
        {
            return $"connection({connectionId})";
        }

        internal void AddToVisList ( NetworkIdentity identity )
        {
            visList.Add ( identity );

            NetworkServer.ShowForConnection ( identity, this );
        }

        internal void RemoveFromVisList ( NetworkIdentity identity, bool isDestroyed )
        {
            visList.Remove ( identity );

            if ( !isDestroyed )
            {
                NetworkServer.HideForConnection ( identity, this );
            }
        }

        internal void RemoveObservers ()
        {
            foreach ( NetworkIdentity identity in visList )
            {
                identity.RemoveObserverInternal ( this );
            }
            visList.Clear ();
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use InvokeHandler<T> instead" )]
        public bool InvokeHandlerNoData ( int msgType )
        {
            return InvokeHandler ( msgType, null, -1 );
        }

        internal bool InvokeHandler ( int msgType, NetworkReader reader, int channelId )
        {
            if ( messageHandlers.TryGetValue ( msgType, out NetworkMessageDelegate msgDelegate ) )
            {
                NetworkMessage message = new NetworkMessage
                {
                    msgType = msgType,
                    reader = reader,
                    conn = this,
                    channelId = channelId
                };

                msgDelegate ( message );
                return true;
            }
            // Debug.LogError("Unknown message ID " + msgType + " " + this);
            return false;
        }

        public bool InvokeHandler<T> ( T msg, int channelId ) where T : IMessageBase
        {
            NetworkWriter writer = NetworkWriterPool.GetWriter ();

            int msgType = MessagePacker.GetId ( typeof ( T ).IsValueType ? typeof ( T ) : msg.GetType () );

            MessagePacker.Pack ( msg, writer );
            ArraySegment<byte> segment = writer.ToArraySegment ();
            bool result = InvokeHandler ( msgType, new NetworkReader ( segment ), channelId );

            NetworkWriterPool.Recycle ( writer );
            return result;
        }

        internal void TransportReceive ( ArraySegment<byte> buffer, int channelId )
        {
            NetworkReader reader = new NetworkReader ( buffer );
            if ( MessagePacker.UnpackMessage ( reader, out int msgType ) )
            {
                if ( logNetworkMessages ) Debug.Log ( "ConnectionRecv " + this + " msgType:" + msgType + " content:" + BitConverter.ToString ( buffer.Array, buffer.Offset, buffer.Count ) );

                if ( InvokeHandler ( msgType, reader, channelId ) )
                {
                    lastMessageTime = Time.time;
                }
            }
            else
            {
                Debug.LogError ( "Closed connection: " + this + ". Invalid message header." );
                Disconnect ();
            }
        }
    }
}
