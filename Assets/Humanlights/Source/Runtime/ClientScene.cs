using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using Guid = System.Guid;
using Object = UnityEngine.Object;

namespace Humanlights.Unity.UNet
{
    public static class ClientScene
    {
        static bool isSpawnFinished;

        public static NetworkIdentity localPlayer { get; private set; }

        public static bool ready { get; internal set; }

        public static NetworkConnection readyConnection { get; private set; }

        public static Dictionary<Guid, GameObject> prefabs = new Dictionary<Guid, GameObject> ();

        public static Dictionary<ulong, NetworkIdentity> spawnableObjects;

        static readonly Dictionary<Guid, SpawnDelegate> spawnHandlers = new Dictionary<Guid, SpawnDelegate> ();
        static readonly Dictionary<Guid, UnSpawnDelegate> unspawnHandlers = new Dictionary<Guid, UnSpawnDelegate> ();

        internal static void Shutdown ()
        {
            ClearSpawners ();
            spawnableObjects = null;
            readyConnection = null;
            ready = false;
            isSpawnFinished = false;
            DestroyAllClientObjects ();
        }

        internal static void InternalAddPlayer ( NetworkIdentity identity )
        {
            if ( LogFilter.Debug ) Debug.LogWarning ( "ClientScene.InternalAddPlayer" );

            localPlayer = identity;
            if ( readyConnection != null )
            {
                readyConnection.identity = identity;
            }
            else
            {
                Debug.LogWarning ( "No ready connection found for setting player controller during InternalAddPlayer" );
            }
        }

        public static bool AddPlayer () => AddPlayer ( null );

        public static bool AddPlayer ( NetworkConnection readyConn ) => AddPlayer ( readyConn, null );

        public static bool AddPlayer ( NetworkConnection readyConn, byte [] extraData )
        {
            if ( readyConn != null )
            {
                ready = true;
                readyConnection = readyConn;
            }

            if ( !ready )
            {
                // Debug.LogError("Must call AddPlayer() with a connection the first time to become ready.");
                return false;
            }

            if ( readyConnection.identity != null )
            {
                // Debug.LogError("ClientScene.AddPlayer: a PlayerController was already added. Did you call AddPlayer twice?");
                return false;
            }

            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.AddPlayer() called with connection [" + readyConnection + "]" );

            AddPlayerMessage message = new AddPlayerMessage ()
            {
#pragma warning disable CS0618 
                value = extraData
#pragma warning restore CS0618 
            };
            readyConnection.Send ( message );
            return true;
        }

        public static bool RemovePlayer ()
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.RemovePlayer() called with connection [" + readyConnection + "]" );

            if ( readyConnection.identity != null )
            {
                readyConnection.Send ( new RemovePlayerMessage () );

                Object.Destroy ( readyConnection.identity.gameObject );

                readyConnection.identity = null;
                localPlayer = null;

                return true;
            }
            return false;
        }

        public static bool Ready ( NetworkConnection conn )
        {
            if ( ready )
            {
                Debug.LogError ( "A connection has already been set as ready. There can only be one." );
                return false;
            }

            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.Ready() called with connection [" + conn + "]" );

            if ( conn != null )
            {
                conn.Send ( new ReadyMessage () );
                ready = true;
                readyConnection = conn;
                readyConnection.isReady = true;
                return true;
            }
            Debug.LogError ( "Ready() called with invalid connection object: conn=null" );
            return false;
        }

        internal static void HandleClientDisconnect ( NetworkConnection conn )
        {
            if ( readyConnection == conn && ready )
            {
                ready = false;
                readyConnection = null;
            }
        }

        static bool ConsiderForSpawning ( NetworkIdentity identity )
        {
            return !identity.gameObject.activeSelf &&
                   identity.gameObject.hideFlags != HideFlags.NotEditable &&
                   identity.gameObject.hideFlags != HideFlags.HideAndDontSave &&
                   identity.sceneId != 0;
        }

        public static void PrepareToSpawnSceneObjects ()
        {
            spawnableObjects = Resources.FindObjectsOfTypeAll<NetworkIdentity> ()
                               .Where ( ConsiderForSpawning )
                               .ToDictionary ( identity => identity.sceneId, identity => identity );
        }

        static NetworkIdentity SpawnSceneObject ( ulong sceneId )
        {
            if ( spawnableObjects.TryGetValue ( sceneId, out NetworkIdentity identity ) )
            {
                spawnableObjects.Remove ( sceneId );
                return identity;
            }
            Debug.LogWarning ( "Could not find scene object with sceneid:" + sceneId.ToString ( "X" ) );
            return null;
        }

        public static bool GetPrefab ( Guid assetId, out GameObject prefab )
        {
            prefab = null;
            return assetId != Guid.Empty &&
                   prefabs.TryGetValue ( assetId, out prefab ) && prefab != null;
        }

        public static void RegisterPrefab ( GameObject prefab, Guid newAssetId )
        {
            NetworkIdentity identity = prefab.GetComponent<NetworkIdentity> ();
            if ( identity )
            {
                identity.assetId = newAssetId;

                if ( LogFilter.Debug ) Debug.Log ( "Registering prefab '" + prefab.name + "' as asset:" + identity.assetId );
                prefabs [ identity.assetId ] = prefab;
            }
            else
            {
                Debug.LogError ( "Could not register '" + prefab.name + "' since it contains no NetworkIdentity component" );
            }
        }

        public static void RegisterPrefab ( GameObject prefab )
        {
            NetworkIdentity identity = prefab.GetComponent<NetworkIdentity> ();
            if ( identity )
            {
                if ( LogFilter.Debug ) Debug.Log ( "Registering prefab '" + prefab.name + "' as asset:" + identity.assetId );
                prefabs [ identity.assetId ] = prefab;

                NetworkIdentity [] identities = prefab.GetComponentsInChildren<NetworkIdentity> ();
                if ( identities.Length > 1 )
                {
                    Debug.LogWarning ( "The prefab '" + prefab.name +
                                     "' has multiple NetworkIdentity components. There can only be one NetworkIdentity on a prefab, and it must be on the root object." );
                }
            }
            else
            {
                Debug.LogError ( "Could not register '" + prefab.name + "' since it contains no NetworkIdentity component" );
            }
        }

        public static void RegisterPrefab ( GameObject prefab, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler )
        {
            NetworkIdentity identity = prefab.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "Could not register '" + prefab.name + "' since it contains no NetworkIdentity component" );
                return;
            }

            if ( spawnHandler == null || unspawnHandler == null )
            {
                Debug.LogError ( "RegisterPrefab custom spawn function null for " + identity.assetId );
                return;
            }

            if ( identity.assetId == Guid.Empty )
            {
                Debug.LogError ( "RegisterPrefab game object " + prefab.name + " has no prefab. Use RegisterSpawnHandler() instead?" );
                return;
            }

            if ( LogFilter.Debug ) Debug.Log ( "Registering custom prefab '" + prefab.name + "' as asset:" + identity.assetId + " " + spawnHandler.GetMethodName () + "/" + unspawnHandler.GetMethodName () );

            spawnHandlers [ identity.assetId ] = spawnHandler;
            unspawnHandlers [ identity.assetId ] = unspawnHandler;
        }

        public static void UnregisterPrefab ( GameObject prefab )
        {
            NetworkIdentity identity = prefab.GetComponent<NetworkIdentity> ();
            if ( identity == null )
            {
                Debug.LogError ( "Could not unregister '" + prefab.name + "' since it contains no NetworkIdentity component" );
                return;
            }
            spawnHandlers.Remove ( identity.assetId );
            unspawnHandlers.Remove ( identity.assetId );
        }

        public static void RegisterSpawnHandler ( Guid assetId, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler )
        {
            if ( spawnHandler == null || unspawnHandler == null )
            {
                Debug.LogError ( "RegisterSpawnHandler custom spawn function null for " + assetId );
                return;
            }

            if ( LogFilter.Debug ) Debug.Log ( "RegisterSpawnHandler asset '" + assetId + "' " + spawnHandler.GetMethodName () + "/" + unspawnHandler.GetMethodName () );

            spawnHandlers [ assetId ] = spawnHandler;
            unspawnHandlers [ assetId ] = unspawnHandler;
        }

        public static void UnregisterSpawnHandler ( Guid assetId )
        {
            spawnHandlers.Remove ( assetId );
            unspawnHandlers.Remove ( assetId );
        }

        public static void ClearSpawners ()
        {
            prefabs.Clear ();
            spawnHandlers.Clear ();
            unspawnHandlers.Clear ();
        }

        static bool InvokeUnSpawnHandler ( Guid assetId, GameObject obj )
        {
            if ( unspawnHandlers.TryGetValue ( assetId, out UnSpawnDelegate handler ) && handler != null )
            {
                handler ( obj );
                return true;
            }
            return false;
        }

        public static void DestroyAllClientObjects ()
        {
            foreach ( NetworkIdentity identity in NetworkIdentity.spawned.Values )
            {
                if ( identity != null && identity.gameObject != null )
                {
                    if ( !InvokeUnSpawnHandler ( identity.assetId, identity.gameObject ) )
                    {
                        if ( identity.sceneId == 0 )
                        {
                            Object.Destroy ( identity.gameObject );
                        }
                        else
                        {
                            identity.MarkForReset ();
                            identity.gameObject.SetActive ( false );
                        }
                    }
                }
            }
            NetworkIdentity.spawned.Clear ();
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use NetworkIdentity.spawned[netId] instead." )]
        public static GameObject FindLocalObject ( uint netId )
        {
            if ( NetworkIdentity.spawned.TryGetValue ( netId, out NetworkIdentity identity ) )
            {
                return identity.gameObject;
            }
            return null;
        }

        static void ApplySpawnPayload ( NetworkIdentity identity, SpawnMessage msg )
        {
            identity.Reset ();
            identity.pendingLocalPlayer = msg.isLocalPlayer;
            identity.assetId = msg.assetId;

            if ( !identity.gameObject.activeSelf )
            {
                identity.gameObject.SetActive ( true );
            }

            identity.transform.localPosition = msg.position;
            identity.transform.localRotation = msg.rotation;
            identity.transform.localScale = msg.scale;

            if ( msg.payload.Count > 0 )
            {
                NetworkReader payloadReader = new NetworkReader ( msg.payload );
                identity.OnUpdateVars ( payloadReader, true );
            }

            identity.netId = msg.netId;
            NetworkIdentity.spawned [ msg.netId ] = identity;

            if ( isSpawnFinished )
            {
                identity.OnStartClient ();
                CheckForLocalPlayer ( identity );
            }
        }

        internal static void OnSpawn ( NetworkConnection _, SpawnMessage msg )
        {
            if ( msg.assetId == Guid.Empty && msg.sceneId == 0 )
            {
                Debug.LogError ( "OnObjSpawn netId: " + msg.netId + " has invalid asset Id" );
                return;
            }
            if ( LogFilter.Debug ) Debug.Log ( $"Client spawn handler instantiating netId={msg.netId} assetID={msg.assetId} sceneId={msg.sceneId} pos={msg.position}" );

            if ( msg.isLocalPlayer )
            {
                OnSpawnMessageForLocalPlayer ( msg.netId );
            }

            NetworkIdentity identity = GetExistingObject ( msg.netId );

            if ( identity == null )
            {
                identity = msg.sceneId == 0 ? SpawnPrefab ( msg ) : SpawnSceneObject ( msg );
            }

            if ( identity == null )
            {
                Debug.LogError ( $"Could not spawn assetId={msg.assetId} scene={msg.sceneId} netId={msg.netId}" );
                return;
            }

            ApplySpawnPayload ( identity, msg );
        }

        static NetworkIdentity GetExistingObject ( uint netid )
        {
            NetworkIdentity.spawned.TryGetValue ( netid, out NetworkIdentity localObject );
            return localObject;
        }

        static NetworkIdentity SpawnPrefab ( SpawnMessage msg )
        {
            if ( GetPrefab ( msg.assetId, out GameObject prefab ) )
            {
                GameObject obj = Object.Instantiate ( prefab, msg.position, msg.rotation );
                if ( LogFilter.Debug )
                {
                    Debug.Log ( "Client spawn handler instantiating [netId:" + msg.netId + " asset ID:" + msg.assetId + " pos:" + msg.position + " rotation: " + msg.rotation + "]" );
                }

                return obj.GetComponent<NetworkIdentity> ();
            }
            if ( spawnHandlers.TryGetValue ( msg.assetId, out SpawnDelegate handler ) )
            {
                GameObject obj = handler ( msg.position, msg.assetId );
                if ( obj == null )
                {
                    Debug.LogWarning ( "Client spawn handler for " + msg.assetId + " returned null" );
                    return null;
                }
                return obj.GetComponent<NetworkIdentity> ();
            }
            Debug.LogError ( "Failed to spawn server object, did you forget to add it to the NetworkManager? assetId=" + msg.assetId + " netId=" + msg.netId );
            return null;
        }

        static NetworkIdentity SpawnSceneObject ( SpawnMessage msg )
        {
            NetworkIdentity spawnedId = SpawnSceneObject ( msg.sceneId );
            if ( spawnedId == null )
            {
                Debug.LogError ( "Spawn scene object not found for " + msg.sceneId.ToString ( "X" ) + " SpawnableObjects.Count=" + spawnableObjects.Count );

                if ( LogFilter.Debug )
                {
                    foreach ( KeyValuePair<ulong, NetworkIdentity> kvp in spawnableObjects )
                        Debug.Log ( "Spawnable: SceneId=" + kvp.Key + " name=" + kvp.Value.name );
                }
            }

            if ( LogFilter.Debug ) Debug.Log ( "Client spawn for [netId:" + msg.netId + "] [sceneId:" + msg.sceneId + "] obj:" + spawnedId.gameObject.name );
            return spawnedId;
        }

        internal static void OnObjectSpawnStarted ( NetworkConnection _, ObjectSpawnStartedMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "SpawnStarted" );

            PrepareToSpawnSceneObjects ();
            isSpawnFinished = false;
        }

        internal static void OnObjectSpawnFinished ( NetworkConnection _, ObjectSpawnFinishedMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "SpawnFinished" );

            foreach ( NetworkIdentity identity in NetworkIdentity.spawned.Values.OrderBy ( uv => uv.netId ) )
            {
                if ( !identity.isClient )
                {
                    identity.OnStartClient ();
                    CheckForLocalPlayer ( identity );
                }
            }
            isSpawnFinished = true;
        }

        internal static void OnObjectHide ( NetworkConnection _, ObjectHideMessage msg )
        {
            DestroyObject ( msg.netId );
        }

        internal static void OnObjectDestroy ( NetworkConnection _, ObjectDestroyMessage msg )
        {
            DestroyObject ( msg.netId );
        }

        static void DestroyObject ( uint netId )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnObjDestroy netId:" + netId );

            if ( NetworkIdentity.spawned.TryGetValue ( netId, out NetworkIdentity localObject ) && localObject != null )
            {
                localObject.OnNetworkDestroy ();

                if ( !InvokeUnSpawnHandler ( localObject.assetId, localObject.gameObject ) )
                {
                    if ( localObject.sceneId == 0 )
                    {
                        Object.Destroy ( localObject.gameObject );
                    }
                    else
                    {
                        localObject.gameObject.SetActive ( false );
                        spawnableObjects [ localObject.sceneId ] = localObject;
                    }
                }
                NetworkIdentity.spawned.Remove ( netId );
                localObject.MarkForReset ();
            }
            else
            {
                if ( LogFilter.Debug ) Debug.LogWarning ( "Did not find target for destroy message for " + netId );
            }
        }

        internal static void OnLocalClientObjectDestroy ( NetworkConnection _, ObjectDestroyMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnLocalObjectObjDestroy netId:" + msg.netId );

            NetworkIdentity.spawned.Remove ( msg.netId );
        }

        internal static void OnLocalClientObjectHide ( NetworkConnection _, ObjectHideMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene::OnLocalObjectObjHide netId:" + msg.netId );

            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity localObject ) && localObject != null )
            {
                localObject.OnSetLocalVisibility ( false );
            }
        }

        internal static void OnLocalClientSpawn ( NetworkConnection _, SpawnMessage msg )
        {
            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity localObject ) && localObject != null )
            {
                localObject.OnSetLocalVisibility ( true );
            }
        }

        internal static void OnUpdateVarsMessage ( NetworkConnection _, UpdateVarsMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnUpdateVarsMessage " + msg.netId );

            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity localObject ) && localObject != null )
            {
                localObject.OnUpdateVars ( new NetworkReader ( msg.payload ), false );
            }
            else
            {
                Debug.LogWarning ( "Did not find target for sync message for " + msg.netId + " . Note: this can be completely normal because UDP messages may arrive out of order, so this message might have arrived after a Destroy message." );
            }
        }

        internal static void OnRPCMessage ( NetworkConnection _, RpcMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnRPCMessage hash:" + msg.functionHash + " netId:" + msg.netId );

            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity identity ) )
            {
                identity.HandleRPC ( msg.componentIndex, msg.functionHash, new NetworkReader ( msg.payload ) );
            }
        }

        internal static void OnSyncEventMessage ( NetworkConnection _, SyncEventMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnSyncEventMessage " + msg.netId );

            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity identity ) )
            {
                identity.HandleSyncEvent ( msg.componentIndex, msg.functionHash, new NetworkReader ( msg.payload ) );
            }
            else
            {
                Debug.LogWarning ( "Did not find target for SyncEvent message for " + msg.netId );
            }
        }

        internal static void OnClientAuthority ( NetworkConnection _, ClientAuthorityMessage msg )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnClientAuthority for netId: " + msg.netId );

            if ( NetworkIdentity.spawned.TryGetValue ( msg.netId, out NetworkIdentity identity ) )
            {
                identity.ForceAuthority ( msg.authority );
            }
        }

        internal static void OnSpawnMessageForLocalPlayer ( uint netId )
        {
            if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnSpawnMessageForLocalPlayer - " + readyConnection + " netId: " + netId );

            if ( readyConnection.identity != null )
            {
                readyConnection.identity.SetNotLocalPlayer ();
            }

            if ( NetworkIdentity.spawned.TryGetValue ( netId, out NetworkIdentity localObject ) && localObject != null )
            {
                localObject.connectionToServer = readyConnection;
                localObject.SetLocalPlayer ();
                InternalAddPlayer ( localObject );
            }
        }

        static void CheckForLocalPlayer ( NetworkIdentity identity )
        {
            if ( identity.pendingLocalPlayer )
            {

                identity.connectionToServer = readyConnection;
                identity.SetLocalPlayer ();

                if ( LogFilter.Debug ) Debug.Log ( "ClientScene.OnOwnerMessage - player=" + identity.name );
                InternalAddPlayer ( identity );

                identity.pendingLocalPlayer = false;
            }
        }
    }
}
