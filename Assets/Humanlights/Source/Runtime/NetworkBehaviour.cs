using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public enum SyncMode { Observers, Owner }

    [AddComponentMenu ( "" )]
    public class NetworkBehaviour : HumanlightsBehaviour
    {
        internal float lastSyncTime;

        [HideInInspector] public SyncMode syncMode = SyncMode.Observers;

        [HideInInspector] public float syncInterval = 0.1f;

        public bool isServer => netIdentity.isServer;

        public bool isClient => netIdentity.isClient;

        public bool isLocalPlayer => netIdentity.isLocalPlayer;

        public bool isServerOnly => isServer && !isClient;

        public bool isClientOnly => isClient && !isServer;

        public bool hasAuthority => netIdentity.hasAuthority;

        public uint netId => netIdentity.netId;

        public NetworkConnection connectionToServer => netIdentity.connectionToServer;

        public NetworkConnection connectionToClient => netIdentity.connectionToClient;

        protected ulong syncVarDirtyBits { get; private set; }
        ulong syncVarHookGuard;

        protected bool getSyncVarHookGuard ( ulong dirtyBit )
        {
            return ( syncVarHookGuard & dirtyBit ) != 0UL;
        }

        protected void setSyncVarHookGuard ( ulong dirtyBit, bool value )
        {
            if ( value )
                syncVarHookGuard |= dirtyBit;
            else
                syncVarHookGuard &= ~dirtyBit;
        }

        [EditorBrowsable ( EditorBrowsableState.Never ), Obsolete ( "Use syncObjects instead." )]
        protected List<SyncObject> m_SyncObjects => syncObjects;

        protected readonly List<SyncObject> syncObjects = new List<SyncObject> ();

        NetworkIdentity netIdentityCache;

        public NetworkIdentity netIdentity
        {
            get
            {
                if ( netIdentityCache == null )
                {
                    netIdentityCache = GetComponent<NetworkIdentity> ();
                }
                if ( netIdentityCache == null )
                {
                    Debug.LogError ( "There is no NetworkIdentity on " + name + ". Please add one." );
                }
                return netIdentityCache;
            }
        }

        public int ComponentIndex
        {
            get
            {
                for ( int i = 0; i < netIdentity.NetworkBehaviours.Length; i++ )
                {
                    NetworkBehaviour component = netIdentity.NetworkBehaviours [ i ];
                    if ( component == this )
                        return i;
                }

                Debug.LogError ( "Could not find component in GameObject. You should not add/remove components in networked objects dynamically", this );

                return -1;
            }
        }

        protected void InitSyncObject ( SyncObject syncObject )
        {
            syncObjects.Add ( syncObject );
        }

        #region Commands

        static int GetMethodHash ( Type invokeClass, string methodName )
        {
            unchecked
            {
                int hash = invokeClass.FullName.GetStableHashCode ();
                return hash * 503 + methodName.GetStableHashCode ();
            }
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SendCommandInternal ( Type invokeClass, string cmdName, NetworkWriter writer, int channelId )
        {
            if ( !NetworkClient.active )
            {
                // Debug.LogError("Command Function " + cmdName + " called on server without an active client.");
                return;
            }
            if ( !( isLocalPlayer || hasAuthority ) )
            {
                // Debug.LogWarning("Trying to send command for object without authority.");
                return;
            }

            if ( ClientScene.readyConnection == null )
            {
                // Debug.LogError("Send command attempted with no client running [client=" + connectionToServer + "].");
                return;
            }

            CommandMessage message = new CommandMessage
            {
                netId = netId,
                componentIndex = ComponentIndex,
                functionHash = GetMethodHash ( invokeClass, cmdName ),
                payload = writer.ToArraySegment ()
            };

            ClientScene.readyConnection.Send ( message, channelId );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public virtual bool InvokeCommand ( int cmdHash, NetworkReader reader )
        {
            return InvokeHandlerDelegate ( cmdHash, MirrorInvokeType.Command, reader );
        }
        #endregion

        #region Client RPCs
        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SendRPCInternal ( Type invokeClass, string rpcName, NetworkWriter writer, int channelId )
        {
            if ( !NetworkServer.active )
            {
                Debug.LogError ( "RPC Function " + rpcName + " called on Client." );
                return;
            }
            if ( !isServer )
            {
                Debug.LogWarning ( "ClientRpc " + rpcName + " called on un-spawned object: " + name );
                return;
            }

            RpcMessage message = new RpcMessage
            {
                netId = netId,
                componentIndex = ComponentIndex,
                functionHash = GetMethodHash ( invokeClass, rpcName ),
                payload = writer.ToArraySegment ()
            };

            NetworkServer.SendToReady ( netIdentity, message, channelId );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SendTargetRPCInternal ( NetworkConnection conn, Type invokeClass, string rpcName, NetworkWriter writer, int channelId )
        {
            if ( !NetworkServer.active )
            {
                Debug.LogError ( "TargetRPC Function " + rpcName + " called on client." );
                return;
            }
            if ( conn == null )
            {
                conn = connectionToClient;
            }
            if ( conn is ULocalConnectionToServer )
            {
                Debug.LogError ( "TargetRPC Function " + rpcName + " called on connection to server" );
                return;
            }
            if ( !isServer )
            {
                Debug.LogWarning ( "TargetRpc " + rpcName + " called on un-spawned object: " + name );
                return;
            }

            RpcMessage message = new RpcMessage
            {
                netId = netId,
                componentIndex = ComponentIndex,
                functionHash = GetMethodHash ( invokeClass, rpcName ),
                payload = writer.ToArraySegment ()
            };

            conn.Send ( message, channelId );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public virtual bool InvokeRPC ( int rpcHash, NetworkReader reader )
        {
            return InvokeHandlerDelegate ( rpcHash, MirrorInvokeType.ClientRpc, reader );
        }
        #endregion

        #region Sync Events
        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SendEventInternal ( Type invokeClass, string eventName, NetworkWriter writer, int channelId )
        {
            if ( !NetworkServer.active )
            {
                Debug.LogWarning ( "SendEvent no server?" );
                return;
            }

            SyncEventMessage message = new SyncEventMessage
            {
                netId = netId,
                componentIndex = ComponentIndex,
                functionHash = GetMethodHash ( invokeClass, eventName ),
                payload = writer.ToArraySegment ()
            };

            NetworkServer.SendToReady ( netIdentity, message, channelId );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public virtual bool InvokeSyncEvent ( int eventHash, NetworkReader reader )
        {
            return InvokeHandlerDelegate ( eventHash, MirrorInvokeType.SyncEvent, reader );
        }
        #endregion

        #region Code Gen Path Helpers
        public delegate void CmdDelegate ( NetworkBehaviour obj, NetworkReader reader );

        protected class Invoker
        {
            public MirrorInvokeType invokeType;
            public Type invokeClass;
            public CmdDelegate invokeFunction;
        }

        static readonly Dictionary<int, Invoker> cmdHandlerDelegates = new Dictionary<int, Invoker> ();

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected static void RegisterDelegate ( Type invokeClass, string cmdName, MirrorInvokeType invokerType, CmdDelegate func )
        {
            int cmdHash = GetMethodHash ( invokeClass, cmdName );

            if ( cmdHandlerDelegates.ContainsKey ( cmdHash ) )
            {
                Invoker oldInvoker = cmdHandlerDelegates [ cmdHash ];
                if ( oldInvoker.invokeClass == invokeClass && oldInvoker.invokeType == invokerType && oldInvoker.invokeFunction == func )
                {
                    return;
                }

                Debug.LogError ( $"Function {oldInvoker.invokeClass}.{oldInvoker.invokeFunction.GetMethodName ()} and {invokeClass}.{oldInvoker.invokeFunction.GetMethodName ()} have the same hash.  Please rename one of them" );
            }
            Invoker invoker = new Invoker
            {
                invokeType = invokerType,
                invokeClass = invokeClass,
                invokeFunction = func
            };
            cmdHandlerDelegates [ cmdHash ] = invoker;
            if ( LogFilter.Debug ) Debug.Log ( "RegisterDelegate hash:" + cmdHash + " invokerType: " + invokerType + " method:" + func.GetMethodName () );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected static void RegisterCommandDelegate ( Type invokeClass, string cmdName, CmdDelegate func )
        {
            RegisterDelegate ( invokeClass, cmdName, MirrorInvokeType.Command, func );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected static void RegisterRpcDelegate ( Type invokeClass, string rpcName, CmdDelegate func )
        {
            RegisterDelegate ( invokeClass, rpcName, MirrorInvokeType.ClientRpc, func );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected static void RegisterEventDelegate ( Type invokeClass, string eventName, CmdDelegate func )
        {
            RegisterDelegate ( invokeClass, eventName, MirrorInvokeType.SyncEvent, func );
        }

        static bool GetInvokerForHash ( int cmdHash, MirrorInvokeType invokeType, out Invoker invoker )
        {
            if ( cmdHandlerDelegates.TryGetValue ( cmdHash, out invoker ) &&
                invoker != null &&
                invoker.invokeType == invokeType )
            {
                return true;
            }

            if ( LogFilter.Debug ) Debug.Log ( "GetInvokerForHash hash:" + cmdHash + " not found" );
            return false;
        }

        internal bool InvokeHandlerDelegate ( int cmdHash, MirrorInvokeType invokeType, NetworkReader reader )
        {
            if ( GetInvokerForHash ( cmdHash, invokeType, out Invoker invoker ) &&
                invoker.invokeClass.IsInstanceOfType ( this ) )
            {
                invoker.invokeFunction ( this, reader );
                return true;
            }
            return false;
        }

        public static CmdDelegate GetRpcHandler ( int cmdHash )
        {
            if ( cmdHandlerDelegates.TryGetValue ( cmdHash, out Invoker invoker ) )
            {
                return invoker.invokeFunction;
            }
            return null;
        }

        #endregion

        #region Helpers

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected bool SyncVarGameObjectEqual ( GameObject newGameObject, uint netIdField )
        {
            uint newNetId = 0;
            if ( newGameObject != null )
            {
                NetworkIdentity identity = newGameObject.GetComponent<NetworkIdentity> ();
                if ( identity != null )
                {
                    newNetId = identity.netId;
                    if ( newNetId == 0 )
                    {
                        Debug.LogWarning ( "SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?" );
                    }
                }
            }

            return newNetId == netIdField;
        }


        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SetSyncVarGameObject ( GameObject newGameObject, ref GameObject gameObjectField, ulong dirtyBit, ref uint netIdField )
        {
            if ( getSyncVarHookGuard ( dirtyBit ) )
                return;

            uint newNetId = 0;
            if ( newGameObject != null )
            {
                NetworkIdentity identity = newGameObject.GetComponent<NetworkIdentity> ();
                if ( identity != null )
                {
                    newNetId = identity.netId;
                    if ( newNetId == 0 )
                    {
                        Debug.LogWarning ( "SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?" );
                    }
                }
            }

            if ( LogFilter.Debug ) Debug.Log ( "SetSyncVar GameObject " + GetType ().Name + " bit [" + dirtyBit + "] netfieldId:" + netIdField + "->" + newNetId );
            SetDirtyBit ( dirtyBit );
            gameObjectField = newGameObject;
            netIdField = newNetId;
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected GameObject GetSyncVarGameObject ( uint netId, ref GameObject gameObjectField )
        {
            if ( isServer )
            {
                return gameObjectField;
            }

            if ( NetworkIdentity.spawned.TryGetValue ( netId, out NetworkIdentity identity ) && identity != null )
                return gameObjectField = identity.gameObject;
            return null;
        }


        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected bool SyncVarNetworkIdentityEqual ( NetworkIdentity newIdentity, uint netIdField )
        {
            uint newNetId = 0;
            if ( newIdentity != null )
            {
                newNetId = newIdentity.netId;
                if ( newNetId == 0 )
                {
                    Debug.LogWarning ( "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?" );
                }
            }

            return newNetId == netIdField;
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SetSyncVarNetworkIdentity ( NetworkIdentity newIdentity, ref NetworkIdentity identityField, ulong dirtyBit, ref uint netIdField )
        {
            if ( getSyncVarHookGuard ( dirtyBit ) )
                return;

            uint newNetId = 0;
            if ( newIdentity != null )
            {
                newNetId = newIdentity.netId;
                if ( newNetId == 0 )
                {
                    Debug.LogWarning ( "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?" );
                }
            }

            if ( LogFilter.Debug ) Debug.Log ( "SetSyncVarNetworkIdentity NetworkIdentity " + GetType ().Name + " bit [" + dirtyBit + "] netIdField:" + netIdField + "->" + newNetId );
            SetDirtyBit ( dirtyBit );
            netIdField = newNetId;
            identityField = newIdentity;
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected NetworkIdentity GetSyncVarNetworkIdentity ( uint netId, ref NetworkIdentity identityField )
        {
            if ( isServer )
            {
                return identityField;
            }

            NetworkIdentity.spawned.TryGetValue ( netId, out identityField );
            return identityField;
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected bool SyncVarEqual<T> ( T value, ref T fieldValue )
        {
            return EqualityComparer<T>.Default.Equals ( value, fieldValue );
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        protected void SetSyncVar<T> ( T value, ref T fieldValue, ulong dirtyBit )
        {
            if ( LogFilter.Debug ) Debug.Log ( "SetSyncVar " + GetType ().Name + " bit [" + dirtyBit + "] " + fieldValue + "->" + value );
            SetDirtyBit ( dirtyBit );
            fieldValue = value;
        }
        #endregion

        public void SetDirtyBit ( ulong dirtyBit )
        {
            syncVarDirtyBits |= dirtyBit;
        }

        public void ClearAllDirtyBits ()
        {
            lastSyncTime = Time.time;
            syncVarDirtyBits = 0L;

            for ( int i = 0; i < syncObjects.Count; ++i )
            {
                syncObjects [ i ].Flush ();
            }
        }

        bool AnySyncObjectDirty ()
        {
            for ( int i = 0; i < syncObjects.Count; ++i )
            {
                if ( syncObjects [ i ].IsDirty )
                {
                    return true;
                }
            }
            return false;
        }

        internal bool IsDirty ()
        {
            if ( Time.time - lastSyncTime >= syncInterval )
            {
                return syncVarDirtyBits != 0L || AnySyncObjectDirty ();
            }
            return false;
        }

        public virtual bool OnSerialize ( NetworkWriter writer, bool initialState )
        {
            if ( initialState )
            {
                return SerializeObjectsAll ( writer );
            }
            return SerializeObjectsDelta ( writer );
        }

        public virtual void OnDeserialize ( NetworkReader reader, bool initialState )
        {
            if ( initialState )
            {
                DeSerializeObjectsAll ( reader );
            }
            else
            {
                DeSerializeObjectsDelta ( reader );
            }
        }

        ulong DirtyObjectBits ()
        {
            ulong dirtyObjects = 0;
            for ( int i = 0; i < syncObjects.Count; i++ )
            {
                SyncObject syncObject = syncObjects [ i ];
                if ( syncObject.IsDirty )
                {
                    dirtyObjects |= 1UL << i;
                }
            }
            return dirtyObjects;
        }

        public bool SerializeObjectsAll ( NetworkWriter writer )
        {
            bool dirty = false;
            for ( int i = 0; i < syncObjects.Count; i++ )
            {
                SyncObject syncObject = syncObjects [ i ];
                syncObject.OnSerializeAll ( writer );
                dirty = true;
            }
            return dirty;
        }

        public bool SerializeObjectsDelta ( NetworkWriter writer )
        {
            bool dirty = false;
            writer.WritePackedUInt64 ( DirtyObjectBits () );
            for ( int i = 0; i < syncObjects.Count; i++ )
            {
                SyncObject syncObject = syncObjects [ i ];
                if ( syncObject.IsDirty )
                {
                    syncObject.OnSerializeDelta ( writer );
                    dirty = true;
                }
            }
            return dirty;
        }

        void DeSerializeObjectsAll ( NetworkReader reader )
        {
            for ( int i = 0; i < syncObjects.Count; i++ )
            {
                SyncObject syncObject = syncObjects [ i ];
                syncObject.OnDeserializeAll ( reader );
            }
        }

        void DeSerializeObjectsDelta ( NetworkReader reader )
        {
            ulong dirty = reader.ReadPackedUInt64 ();
            for ( int i = 0; i < syncObjects.Count; i++ )
            {
                SyncObject syncObject = syncObjects [ i ];
                if ( ( dirty & ( 1UL << i ) ) != 0 )
                {
                    syncObject.OnDeserializeDelta ( reader );
                }
            }
        }

        [EditorBrowsable ( EditorBrowsableState.Never )]
        public virtual void OnNetworkDestroy () { }

        public virtual void OnStartServer () { }

        public virtual void OnStartClient () { }

        public virtual void OnStartLocalPlayer () { }

        public virtual void OnStartAuthority () { }

        public virtual void OnStopAuthority () { }

        public virtual bool OnRebuildObservers ( HashSet<NetworkConnection> observers, bool initialize )
        {
            return false;
        }

        public virtual void OnSetLocalVisibility ( bool vis ) { }

        public virtual bool OnCheckObserver ( NetworkConnection conn )
        {
            return true;
        }
    }
}
