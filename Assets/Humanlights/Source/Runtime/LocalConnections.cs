using System;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    class ULocalConnectionToClient : Humanlights.Unity.UNet.NetworkConnectionToClient
    {
        internal ULocalConnectionToServer connectionToServer;

        public ULocalConnectionToClient() : base (0)
        {
        }

        public override string address => "localhost";

        internal override bool Send(ArraySegment<byte> segment, int channelId = Channels.DefaultReliable)
        {
            byte[] data = new byte[segment.Count];
            Array.Copy(segment.Array, segment.Offset, data, 0, segment.Count);
            connectionToServer.packetQueue.Enqueue(data);
            return true;
        }

        internal void DisconnectInternal()
        {
            isReady = false;
            RemoveObservers();
        }

        public override void Disconnect()
        {
            DisconnectInternal();
            connectionToServer.DisconnectInternal();
        }
    }

    internal class ULocalConnectionToServer : Humanlights.Unity.UNet.NetworkConnectionToServer
    {
        internal ULocalConnectionToClient connectionToClient;

        internal Queue<byte[]> packetQueue = new Queue<byte[]>();

        public override string address => "localhost";

        internal override bool Send(ArraySegment<byte> segment, int channelId = Channels.DefaultReliable)
        {
            if (segment.Count == 0)
            {
                Debug.LogError("LocalConnection.SendBytes cannot send zero bytes");
                return false;
            }

            connectionToClient.TransportReceive(segment, channelId);
            return true;
        }

        internal void Update()
        {
            while (packetQueue.Count > 0)
            {
                byte[] packet = packetQueue.Dequeue();
                TransportReceive(new ArraySegment<byte>(packet), Channels.DefaultReliable);
            }
        }

        internal void DisconnectInternal()
        {
            isReady = false;
            ClientScene.HandleClientDisconnect(this);
        }

        public override void Disconnect()
        {
            connectionToClient.DisconnectInternal();
            DisconnectInternal();
        }
    }
}
