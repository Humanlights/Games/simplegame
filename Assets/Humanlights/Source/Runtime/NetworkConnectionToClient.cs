using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public class NetworkConnectionToClient : Humanlights.Unity.UNet.NetworkConnection
    {
        public readonly HashSet<NetworkIdentity> clientOwnedObjects = new HashSet<NetworkIdentity>();

        public NetworkConnectionToClient(int networkConnectionId) : base(networkConnectionId)
        {
        }

        public override string address => Transport.activeTransport.ServerGetClientAddress(connectionId);

        List<int> singleConnectionId = new List<int> { -1 };

        internal override bool Send(ArraySegment<byte> segment, int channelId = Channels.DefaultReliable)
        {
            if (logNetworkMessages) Debug.Log("ConnectionSend " + this + " bytes:" + BitConverter.ToString(segment.Array, segment.Offset, segment.Count));

            if (ValidatePacketSize(segment, channelId))
            {
                singleConnectionId[0] = connectionId;
                return Transport.activeTransport.ServerSend(singleConnectionId, channelId, segment);
            }
            return false;
        }

        internal static bool Send(List<int> connectionIds, ArraySegment<byte> segment, int channelId = Channels.DefaultReliable)
        {
            if (ValidatePacketSize(segment, channelId))
            {
                if (Transport.activeTransport.ServerActive())
                {
                    return Transport.activeTransport.ServerSend(connectionIds, channelId, segment);
                }
            }
            return false;
        }

        public override void Disconnect()
        {
            isReady = false;
            Transport.activeTransport.ServerDisconnect(connectionId);
            RemoveObservers();
            DestroyOwnedObjects();
        }

        internal void AddOwnedObject(NetworkIdentity obj)
        {
            clientOwnedObjects.Add(obj);
        }

        internal void RemoveOwnedObject(NetworkIdentity obj)
        {
            clientOwnedObjects.Remove(obj);
        }

        protected void DestroyOwnedObjects()
        {
            HashSet<NetworkIdentity> objects = new HashSet<NetworkIdentity>(clientOwnedObjects);
            foreach (NetworkIdentity netId in objects)
            {
                if (netId != null)
                {
                    NetworkServer.Destroy(netId.gameObject);
                }
            }
            clientOwnedObjects.Clear();
        }
    }
}
