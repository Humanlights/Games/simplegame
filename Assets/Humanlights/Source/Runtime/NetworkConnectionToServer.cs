using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public class NetworkConnectionToServer : Humanlights.Unity.UNet.NetworkConnection
    {
        public override string address => "";

        internal override bool Send(ArraySegment<byte> segment, int channelId = Channels.DefaultReliable)
        {
            if (logNetworkMessages) Debug.Log("ConnectionSend " + this + " bytes:" + BitConverter.ToString(segment.Array, segment.Offset, segment.Count));

            if (ValidatePacketSize(segment, channelId))
            {
                return Transport.activeTransport.ClientSend(channelId, segment);
            }
            return false;
        }

        public override void Disconnect()
        {
            isReady = false;
            ClientScene.HandleClientDisconnect(this);
            Transport.activeTransport.ClientDisconnect();
        }
    }
}
