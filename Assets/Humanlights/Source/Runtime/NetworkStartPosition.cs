using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkStartPosition" )]
    public class NetworkStartPosition : MonoBehaviour
    {
        public void Awake ()
        {
            // NetworkManager.RegisterStartPosition ( transform );
        }

        public void OnDestroy ()
        {
            // NetworkManager.UnRegisterStartPosition ( transform );
        }
    }
}
