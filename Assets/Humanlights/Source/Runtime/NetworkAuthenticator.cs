using System;
using UnityEngine;
using UnityEngine.Events;

namespace Humanlights.Unity.UNet
{
    [Serializable] public class UnityEventNetworkConnection : UnityEvent<NetworkConnection> { }

    public abstract class NetworkAuthenticator : MonoBehaviour
    {
        [Header ( "Event Listeners (optional)" )]

        [Tooltip ( "Humanlights.Unity.UNet has an internal subscriber to this event. You can add your own here." )]
        public UnityEventNetworkConnection OnServerAuthenticated = new UnityEventNetworkConnection ();

        [Tooltip ( "Humanlights.Unity.UNet has an internal subscriber to this event. You can add your own here." )]
        public UnityEventNetworkConnection OnClientAuthenticated = new UnityEventNetworkConnection ();

        #region server

        public virtual void OnStartServer () { }

        internal void OnServerAuthenticateInternal ( NetworkConnection conn )
        {
            OnServerAuthenticate ( conn );
        }

        public abstract void OnServerAuthenticate ( NetworkConnection conn );

        #endregion

        #region client

        public virtual void OnStartClient () { }

        internal void OnClientAuthenticateInternal ( NetworkConnection conn )
        {
            OnClientAuthenticate ( conn );
        }

        public abstract void OnClientAuthenticate ( NetworkConnection conn );

        #endregion

        void OnValidate ()
        {
#if UNITY_EDITOR
            NetworkManager manager = GetComponent<NetworkManager> ();
            if ( manager != null && manager.authenticator == null )
            {
                manager.authenticator = this;
                UnityEditor.Undo.RecordObject ( gameObject, "Assigned NetworkManager authenticator" );
            }
#endif
        }
    }
}
