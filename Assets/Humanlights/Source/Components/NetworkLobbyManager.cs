using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Humanlights.Unity.UNet
{
    [AddComponentMenu ( "Network/NetworkLobbyManager" )]
    [Obsolete ( "Use / inherit from NetworkRoomManager instead" )]
    public class NetworkLobbyManager : Humanlights.Unity.UNet.NetworkRoomManager { }
}
