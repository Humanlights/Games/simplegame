using UnityEngine;

namespace Humanlights.Unity.UNet
{
    public abstract class NetworkTransformBase : NetworkBehaviour
    {
        [Tooltip ( "Compresses 16 Byte Quaternion into None=12, Much=3, Lots=2 Byte" )]
        [SerializeField] Compression compressRotation = Compression.Much;
        public enum Compression { None, Much, Lots, NoRotation };

        Vector3 lastPosition;
        Quaternion lastRotation;
        Vector3 lastScale;

        public class DataPoint
        {
            public float timeStamp;
            public Vector3 localPosition;
            public Quaternion localRotation;
            public Vector3 localScale;
            public float movementSpeed;
        }
        DataPoint start;
        DataPoint goal;

        float lastClientSendTime;

        protected abstract Transform targetComponent { get; }

        static void SerializeIntoWriter ( NetworkWriter writer, Vector3 position, Quaternion rotation, Compression compressRotation, Vector3 scale )
        {
            writer.WriteVector3 ( position );

            Vector3 euler = rotation.eulerAngles;
            if ( compressRotation == Compression.None )
            {
                writer.WriteSingle ( euler.x );
                writer.WriteSingle ( euler.y );
                writer.WriteSingle ( euler.z );
            }
            else if ( compressRotation == Compression.Much )
            {
                writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.x, 0, 360, byte.MinValue, byte.MaxValue ) );
                writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.y, 0, 360, byte.MinValue, byte.MaxValue ) );
                writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.z, 0, 360, byte.MinValue, byte.MaxValue ) );
            }
            else if ( compressRotation == Compression.Lots )
            {
                writer.WriteUInt16 ( FloatBytePacker.PackThreeFloatsIntoUShort ( euler.x, euler.y, euler.z, 0, 360 ) );
            }

            writer.WriteVector3 ( scale );
        }

        public override bool OnSerialize ( NetworkWriter writer, bool initialState )
        {
            SerializeIntoWriter ( writer, targetComponent.transform.localPosition, targetComponent.transform.localRotation, compressRotation, targetComponent.transform.localScale );
            return true;
        }

        static float EstimateMovementSpeed ( DataPoint from, DataPoint to, Transform transform, float sendInterval )
        {
            Vector3 delta = to.localPosition - ( from != null ? from.localPosition : transform.localPosition );
            float elapsed = from != null ? to.timeStamp - from.timeStamp : sendInterval;
            return elapsed > 0 ? delta.magnitude / elapsed : 0;
        }

        void DeserializeFromReader ( NetworkReader reader )
        {
            DataPoint temp = new DataPoint
            {
                localPosition = reader.ReadVector3 ()
            };

            if ( compressRotation == Compression.None )
            {
                float x = reader.ReadSingle ();
                float y = reader.ReadSingle ();
                float z = reader.ReadSingle ();
                temp.localRotation = Quaternion.Euler ( x, y, z );
            }
            else if ( compressRotation == Compression.Much )
            {
                float x = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                float y = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                float z = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                temp.localRotation = Quaternion.Euler ( x, y, z );
            }
            else if ( compressRotation == Compression.Lots )
            {
                Vector3 xyz = FloatBytePacker.UnpackUShortIntoThreeFloats ( reader.ReadUInt16 (), 0, 360 );
                temp.localRotation = Quaternion.Euler ( xyz.x, xyz.y, xyz.z );
            }

            temp.localScale = reader.ReadVector3 ();

            temp.timeStamp = Time.time;

            temp.movementSpeed = EstimateMovementSpeed ( goal, temp, targetComponent.transform, syncInterval );

            if ( start == null )
            {
                start = new DataPoint
                {
                    timeStamp = Time.time - syncInterval,
                    localPosition = targetComponent.transform.localPosition,
                    localRotation = targetComponent.transform.localRotation,
                    localScale = targetComponent.transform.localScale,
                    movementSpeed = temp.movementSpeed
                };
            }
            else
            {
                float oldDistance = Vector3.Distance ( start.localPosition, goal.localPosition );
                float newDistance = Vector3.Distance ( goal.localPosition, temp.localPosition );

                start = goal;

                if ( Vector3.Distance ( targetComponent.transform.localPosition, start.localPosition ) < oldDistance + newDistance )
                {
                    start.localPosition = targetComponent.transform.localPosition;
                    start.localRotation = targetComponent.transform.localRotation;
                    start.localScale = targetComponent.transform.localScale;
                }
            }

            goal = temp;
        }

        public override void OnDeserialize ( NetworkReader reader, bool initialState )
        {
            DeserializeFromReader ( reader );
        }

        [ClientCmd]
        void CmdClientToServerSync ( byte [] payload )
        {
            NetworkReader reader = new NetworkReader ( payload );
            DeserializeFromReader ( reader );

            if ( isServer && !isClient )
                ApplyPositionRotationScale ( goal.localPosition, goal.localRotation, goal.localScale );

            SetDirtyBit ( 1UL );
        }

        static float CurrentInterpolationFactor ( DataPoint start, DataPoint goal )
        {
            if ( start != null )
            {
                float difference = goal.timeStamp - start.timeStamp;

                float elapsed = Time.time - goal.timeStamp;
                return difference > 0 ? elapsed / difference : 0;
            }
            return 0;
        }

        static Vector3 InterpolatePosition ( DataPoint start, DataPoint goal, Vector3 currentPosition )
        {
            if ( start != null )
            {

                float speed = Mathf.Max ( start.movementSpeed, goal.movementSpeed );
                return Vector3.MoveTowards ( currentPosition, goal.localPosition, speed * Time.deltaTime );
            }
            return currentPosition;
        }

        static Quaternion InterpolateRotation ( DataPoint start, DataPoint goal, Quaternion defaultRotation )
        {
            if ( start != null )
            {
                float t = CurrentInterpolationFactor ( start, goal );
                return Quaternion.Slerp ( start.localRotation, goal.localRotation, t );
            }
            return defaultRotation;
        }

        static Vector3 InterpolateScale ( DataPoint start, DataPoint goal, Vector3 currentScale )
        {
            if ( start != null )
            {
                float t = CurrentInterpolationFactor ( start, goal );
                return Vector3.Lerp ( start.localScale, goal.localScale, t );
            }
            return currentScale;
        }

        bool NeedsTeleport ()
        {
            float startTime = start != null ? start.timeStamp : Time.time - syncInterval;
            float goalTime = goal != null ? goal.timeStamp : Time.time;
            float difference = goalTime - startTime;
            float timeSinceGoalReceived = Time.time - goalTime;
            return timeSinceGoalReceived > difference * 5;
        }

        bool HasEitherMovedRotatedScaled ()
        {
            bool moved = lastPosition != targetComponent.transform.localPosition;
            bool rotated = lastRotation != targetComponent.transform.localRotation;
            bool scaled = lastScale != targetComponent.transform.localScale;

            bool change = moved || rotated || scaled;
            if ( change )
            {
                lastPosition = targetComponent.transform.localPosition;
                lastRotation = targetComponent.transform.localRotation;
                lastScale = targetComponent.transform.localScale;
            }
            return change;
        }

        void ApplyPositionRotationScale ( Vector3 position, Quaternion rotation, Vector3 scale )
        {
            targetComponent.transform.localPosition = position;
            if ( Compression.NoRotation != compressRotation )
            {
                targetComponent.transform.localRotation = rotation;
            }
            targetComponent.transform.localScale = scale;
        }

        void Update ()
        {
            if ( isServer )
            {
                SetDirtyBit ( HasEitherMovedRotatedScaled () ? 1UL : 0UL );
            }

            if ( isClient )
            {
                if ( !isServer && hasAuthority )
                {
                    if ( Time.time - lastClientSendTime >= syncInterval )
                    {
                        if ( HasEitherMovedRotatedScaled () )
                        {
                            NetworkWriter writer = new NetworkWriter ();
                            SerializeIntoWriter ( writer, targetComponent.transform.localPosition, targetComponent.transform.localRotation, compressRotation, targetComponent.transform.localScale );

                            CmdClientToServerSync ( writer.ToArray () );
                        }
                        lastClientSendTime = Time.time;
                    }
                }

                if ( !hasAuthority )
                {
                    if ( goal != null )
                    {
                        if ( NeedsTeleport () )
                        {
                            ApplyPositionRotationScale ( goal.localPosition, goal.localRotation, goal.localScale );
                        }
                        else
                        {
                            ApplyPositionRotationScale ( InterpolatePosition ( start, goal, targetComponent.transform.localPosition ),
                                                       InterpolateRotation ( start, goal, targetComponent.transform.localRotation ),
                                                       InterpolateScale ( start, goal, targetComponent.transform.localScale ) );
                        }
                    }
                }
            }
        }

        static void DrawDataPointGizmo ( DataPoint data, Color color )
        {
            Vector3 offset = Vector3.up * 0.01f;

            Gizmos.color = color;
            Gizmos.DrawSphere ( data.localPosition + offset, 0.5f );

            Gizmos.color = Color.blue;
            Gizmos.DrawRay ( data.localPosition + offset, data.localRotation * Vector3.forward );

            Gizmos.color = Color.green;
            Gizmos.DrawRay ( data.localPosition + offset, data.localRotation * Vector3.up );
        }

        static void DrawLineBetweenDataPoints ( DataPoint data1, DataPoint data2, Color color )
        {
            Gizmos.color = color;
            Gizmos.DrawLine ( data1.localPosition, data2.localPosition );
        }

        void OnDrawGizmos ()
        {
            if ( start != null ) DrawDataPointGizmo ( start, Color.gray );
            if ( goal != null ) DrawDataPointGizmo ( goal, Color.white );

            if ( start != null && goal != null ) DrawLineBetweenDataPoints ( start, goal, Color.cyan );
        }
    }
}
