using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkTransform" )]
    public class NetworkTransform : Humanlights.Unity.UNet.NetworkTransformBase
    {
        protected override Transform targetComponent => transform;
    }
}
