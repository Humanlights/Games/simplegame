using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Humanlights.Unity.UNet
{
    [AddComponentMenu ( "Network/NetworkRoomManager" )]
    public class NetworkRoomManager : Humanlights.Unity.UNet.NetworkManager
    {
        public struct PendingPlayer
        {
            public NetworkConnection conn;
            public GameObject roomPlayer;
        }

        [Header ( "Room Settings" )]

        [FormerlySerializedAs ( "m_ShowRoomGUI" )]
        [SerializeField]
        internal bool showRoomGUI = true;

        [FormerlySerializedAs ( "m_MinPlayers" )]
        [SerializeField]
        int minPlayers = 1;

        [FormerlySerializedAs ( "m_RoomPlayerPrefab" )]
        [SerializeField]
        NetworkRoomPlayer roomPlayerPrefab;

        [Scene]
        public string RoomScene;

        [Scene]
        public string GameplayScene;

        [FormerlySerializedAs ( "m_PendingPlayers" )]
        public List<PendingPlayer> pendingPlayers = new List<PendingPlayer> ();

        public List<NetworkRoomPlayer> roomSlots = new List<NetworkRoomPlayer> ();

        public bool allPlayersReady;

        public override void OnValidate ()
        {
            maxConnections = Mathf.Max ( maxConnections, 0 );

            minPlayers = Mathf.Min ( minPlayers, maxConnections );

            minPlayers = Mathf.Max ( minPlayers, 0 );

            if ( roomPlayerPrefab != null )
            {
                NetworkIdentity identity = roomPlayerPrefab.GetComponent<NetworkIdentity> ();
                if ( identity == null )
                {
                    roomPlayerPrefab = null;
                    Debug.LogError ( "RoomPlayer prefab must have a NetworkIdentity component." );
                }
            }

            base.OnValidate ();
        }

        internal void ReadyStatusChanged ()
        {
            int CurrentPlayers = 0;
            int ReadyPlayers = 0;

            foreach ( NetworkRoomPlayer item in roomSlots )
            {
                if ( item != null )
                {
                    CurrentPlayers++;
                    if ( item.readyToBegin )
                        ReadyPlayers++;
                }
            }

            if ( CurrentPlayers == ReadyPlayers )
                CheckReadyToBegin ();
            else
                allPlayersReady = false;
        }

        public override void OnServerReady ( NetworkConnection conn )
        {
            if ( LogFilter.Debug ) Debug.Log ( "NetworkRoomManager OnServerReady" );
            base.OnServerReady ( conn );

            if ( conn != null && conn.identity != null )
            {
                GameObject roomPlayer = conn.identity.gameObject;

                if ( roomPlayer != null && roomPlayer.GetComponent<NetworkRoomPlayer> () != null )
                    SceneLoadedForPlayer ( conn, roomPlayer );
            }
        }

        void SceneLoadedForPlayer ( NetworkConnection conn, GameObject roomPlayer )
        {
            if ( LogFilter.Debug ) Debug.LogFormat ( "NetworkRoom SceneLoadedForPlayer scene: {0} {1}", SceneManager.GetActiveScene ().name, conn );

            if ( SceneManager.GetActiveScene ().name == RoomScene )
            {
                PendingPlayer pending;
                pending.conn = conn;
                pending.roomPlayer = roomPlayer;
                pendingPlayers.Add ( pending );
                return;
            }

            GameObject gamePlayer = OnRoomServerCreateGamePlayer ( conn );
            if ( gamePlayer == null )
            {
                Transform startPos = GetStartPosition ();
                gamePlayer = startPos != null
                    ? Instantiate ( playerPrefab, startPos.position, startPos.rotation )
                    : Instantiate ( playerPrefab, Vector3.zero, Quaternion.identity );
                gamePlayer.name = playerPrefab.name;
            }

            if ( !OnRoomServerSceneLoadedForPlayer ( roomPlayer, gamePlayer ) )
                return;

            NetworkServer.ReplacePlayerForConnection ( conn, gamePlayer );
        }

        public void CheckReadyToBegin ()
        {
            if ( SceneManager.GetActiveScene ().name != RoomScene ) return;

            if ( minPlayers > 0 && NetworkServer.connections.Count ( conn => conn.Value != null && conn.Value.identity.gameObject.GetComponent<NetworkRoomPlayer> ().readyToBegin ) < minPlayers )
            {
                allPlayersReady = false;
                return;
            }

            pendingPlayers.Clear ();
            allPlayersReady = true;
            OnRoomServerPlayersReady ();
        }

        void CallOnClientEnterRoom ()
        {
            OnRoomClientEnter ();
            foreach ( NetworkRoomPlayer player in roomSlots )
                if ( player != null )
                {
                    player.OnClientEnterRoom ();
                }
        }

        void CallOnClientExitRoom ()
        {
            OnRoomClientExit ();
            foreach ( NetworkRoomPlayer player in roomSlots )
                if ( player != null )
                {
                    player.OnClientExitRoom ();
                }
        }

        #region server handlers

        public override void OnServerConnect ( NetworkConnection conn )
        {
            if ( numPlayers >= maxConnections )
            {
                conn.Disconnect ();
                return;
            }

            if ( SceneManager.GetActiveScene ().name != RoomScene )
            {
                conn.Disconnect ();
                return;
            }

            base.OnServerConnect ( conn );
            OnRoomServerConnect ( conn );
        }

        public override void OnServerDisconnect ( NetworkConnection conn )
        {
            if ( conn.identity != null )
            {
                NetworkRoomPlayer player = conn.identity.GetComponent<NetworkRoomPlayer> ();

                if ( player != null )
                    roomSlots.Remove ( player );
            }

            allPlayersReady = false;

            foreach ( NetworkRoomPlayer player in roomSlots )
            {
                if ( player != null )
                    player.GetComponent<NetworkRoomPlayer> ().readyToBegin = false;
            }

            if ( SceneManager.GetActiveScene ().name == RoomScene )
                RecalculateRoomPlayerIndices ();

            base.OnServerDisconnect ( conn );
            OnRoomServerDisconnect ( conn );
        }

        public override void OnServerAddPlayer ( NetworkConnection conn )
        {
            if ( SceneManager.GetActiveScene ().name != RoomScene ) return;

            if ( roomSlots.Count == maxConnections ) return;

            allPlayersReady = false;

            if ( LogFilter.Debug ) Debug.LogFormat ( "NetworkRoomManager.OnServerAddPlayer playerPrefab:{0}", roomPlayerPrefab.name );

            GameObject newRoomGameObject = OnRoomServerCreateRoomPlayer ( conn );
            if ( newRoomGameObject == null )
                newRoomGameObject = ( GameObject ) Instantiate ( roomPlayerPrefab.gameObject, Vector3.zero, Quaternion.identity );

            NetworkRoomPlayer newRoomPlayer = newRoomGameObject.GetComponent<NetworkRoomPlayer> ();

            roomSlots.Add ( newRoomPlayer );

            RecalculateRoomPlayerIndices ();

            NetworkServer.AddPlayerForConnection ( conn, newRoomGameObject );
        }

        void RecalculateRoomPlayerIndices ()
        {
            if ( roomSlots.Count > 0 )
            {
                for ( int i = 0; i < roomSlots.Count; i++ )
                {
                    roomSlots [ i ].index = i;
                }
            }
        }

        public override void ServerChangeScene ( string sceneName )
        {
            if ( sceneName == RoomScene )
            {
                foreach ( NetworkRoomPlayer roomPlayer in roomSlots )
                {
                    if ( roomPlayer == null ) continue;

                    NetworkIdentity identity = roomPlayer.GetComponent<NetworkIdentity> ();

                    NetworkIdentity playerController = identity.connectionToClient.identity;
                    NetworkServer.Destroy ( playerController.gameObject );

                    if ( NetworkServer.active )
                    {
                        roomPlayer.GetComponent<NetworkRoomPlayer> ().readyToBegin = false;
                        NetworkServer.ReplacePlayerForConnection ( identity.connectionToClient, roomPlayer.gameObject );
                    }
                }
            }

            base.ServerChangeScene ( sceneName );
        }

        public override void OnServerSceneChanged ( string sceneName )
        {
            if ( sceneName != RoomScene )
            {
                foreach ( PendingPlayer pending in pendingPlayers )
                    SceneLoadedForPlayer ( pending.conn, pending.roomPlayer );

                pendingPlayers.Clear ();
            }

            OnRoomServerSceneChanged ( sceneName );
        }

        public override void OnStartServer ()
        {
            if ( string.IsNullOrEmpty ( RoomScene ) )
            {
                Debug.LogError ( "NetworkRoomManager RoomScene is empty. Set the RoomScene in the inspector for the NetworkRoomMangaer" );
                return;
            }

            if ( string.IsNullOrEmpty ( GameplayScene ) )
            {
                Debug.LogError ( "NetworkRoomManager PlayScene is empty. Set the PlayScene in the inspector for the NetworkRoomMangaer" );
                return;
            }

            OnRoomStartServer ();
        }

        public override void OnStartHost ()
        {
            OnRoomStartHost ();
        }

        public override void OnStopServer ()
        {
            roomSlots.Clear ();
            base.OnStopServer ();
        }

        public override void OnStopHost ()
        {
            OnRoomStopHost ();
        }

        #endregion

        #region client handlers

        public override void OnStartClient ()
        {
            if ( roomPlayerPrefab == null || roomPlayerPrefab.gameObject == null )
                Debug.LogError ( "NetworkRoomManager no RoomPlayer prefab is registered. Please add a RoomPlayer prefab." );
            else
                ClientScene.RegisterPrefab ( roomPlayerPrefab.gameObject );

            if ( playerPrefab == null )
                Debug.LogError ( "NetworkRoomManager no GamePlayer prefab is registered. Please add a GamePlayer prefab." );
            else
                ClientScene.RegisterPrefab ( playerPrefab );

            OnRoomStartClient ();
        }

        public override void OnClientConnect ( NetworkConnection conn )
        {
            OnRoomClientConnect ( conn );
            CallOnClientEnterRoom ();
            base.OnClientConnect ( conn );
        }

        public override void OnClientDisconnect ( NetworkConnection conn )
        {
            OnRoomClientDisconnect ( conn );
            base.OnClientDisconnect ( conn );
        }

        public override void OnStopClient ()
        {
            OnRoomStopClient ();
            CallOnClientExitRoom ();

            if ( !string.IsNullOrEmpty ( offlineScene ) )
            {
                SceneManager.MoveGameObjectToScene ( gameObject, SceneManager.GetActiveScene () );
            }
        }

        public override void OnClientSceneChanged ( NetworkConnection conn )
        {
            if ( SceneManager.GetActiveScene ().name == RoomScene )
            {
                if ( NetworkClient.isConnected )
                    CallOnClientEnterRoom ();
            }
            else
                CallOnClientExitRoom ();

            base.OnClientSceneChanged ( conn );
            OnRoomClientSceneChanged ( conn );
        }

        #endregion

        #region room server virtuals

        public virtual void OnRoomStartHost () { }

        public virtual void OnRoomStopHost () { }

        public virtual void OnRoomStartServer () { }

        public virtual void OnRoomServerConnect ( NetworkConnection conn ) { }

        public virtual void OnRoomServerDisconnect ( NetworkConnection conn ) { }

        public virtual void OnRoomServerSceneChanged ( string sceneName ) { }

        public virtual GameObject OnRoomServerCreateRoomPlayer ( NetworkConnection conn )
        {
            return null;
        }

        public virtual GameObject OnRoomServerCreateGamePlayer ( NetworkConnection conn )
        {
            return null;
        }

        public virtual bool OnRoomServerSceneLoadedForPlayer ( GameObject roomPlayer, GameObject gamePlayer )
        {
            return true;
        }

        public virtual void OnRoomServerPlayersReady ()
        {
            ServerChangeScene ( GameplayScene );
        }

        #endregion

        #region room client virtuals

        public virtual void OnRoomClientEnter () { }

        public virtual void OnRoomClientExit () { }

        public virtual void OnRoomClientConnect ( NetworkConnection conn ) { }

        public virtual void OnRoomClientDisconnect ( NetworkConnection conn ) { }

        public virtual void OnRoomStartClient () { }

        public virtual void OnRoomStopClient () { }

        public virtual void OnRoomClientSceneChanged ( NetworkConnection conn ) { }

        public virtual void OnRoomClientAddPlayerFailed () { }

        #endregion

        #region optional UI

        public virtual void OnGUI ()
        {
            if ( !showRoomGUI )
                return;

            if ( SceneManager.GetActiveScene ().name != RoomScene )
                return;

            GUI.Box ( new Rect ( 10f, 180f, 520f, 150f ), "PLAYERS" );
        }

        #endregion
    }
}
