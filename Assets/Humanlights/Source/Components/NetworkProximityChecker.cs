using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [AddComponentMenu ( "Network/NetworkProximityChecker" )]
    [RequireComponent ( typeof ( NetworkIdentity ) )]
    public class NetworkProximityChecker : Humanlights.Unity.UNet.NetworkBehaviour
    {
        public enum CheckMethod
        {
            Physics3D,
            Physics2D
        }

        [Tooltip ( "The maximum range that objects will be visible at." )]
        public int visRange = 10;

        [Tooltip ( "How often (in seconds) that this object should update the list of observers that can see it." )]
        public float visUpdateInterval = 1;

        [Tooltip ( "Which method to use for checking proximity of players.\n\nPhysics3D uses 3D physics to determine proximity.\nPhysics2D uses 2D physics to determine proximity." )]
        public CheckMethod checkMethod = CheckMethod.Physics3D;

        [Tooltip ( "Enable to force this object to be hidden from players." )]
        public bool forceHidden;

        [Tooltip ( "Select only the Player's layer to avoid unnecessary SphereCasts against the Terrain, etc." )]
        public LayerMask castLayers = ~0;

        float lastUpdateTime;

        static Collider [] hitsBuffer3D = new Collider [ 10000 ];
        static Collider2D [] hitsBuffer2D = new Collider2D [ 10000 ];

        void Update ()
        {
            if ( !NetworkServer.active )
                return;

            if ( Time.time - lastUpdateTime > visUpdateInterval )
            {
                netIdentity.RebuildObservers ( false );
                lastUpdateTime = Time.time;
            }
        }

        public override bool OnCheckObserver ( NetworkConnection newObserver )
        {
            if ( forceHidden )
                return false;

            return Vector3.Distance ( newObserver.identity.transform.position, transform.position ) < visRange;
        }

        public override bool OnRebuildObservers ( HashSet<NetworkConnection> observers, bool initial )
        {
            if ( forceHidden )
                return true;

            switch ( checkMethod )
            {
                case CheckMethod.Physics3D:
                    {
                        int hitCount = Physics.OverlapSphereNonAlloc ( transform.position, visRange, hitsBuffer3D, castLayers );
                        if ( hitCount == hitsBuffer3D.Length ) Debug.LogWarning ( "NetworkProximityChecker's OverlapSphere test for " + name + " has filled the whole buffer(" + hitsBuffer3D.Length + "). Some results might have been omitted. Consider increasing buffer size." );

                        for ( int i = 0; i < hitCount; i++ )
                        {
                            Collider hit = hitsBuffer3D [ i ];
                            NetworkIdentity identity = hit.GetComponentInParent<NetworkIdentity> ();
                            if ( identity != null && identity.connectionToClient != null )
                            {
                                observers.Add ( identity.connectionToClient );
                            }
                        }
                        break;
                    }

                case CheckMethod.Physics2D:
                    {
                        int hitCount = Physics2D.OverlapCircleNonAlloc ( transform.position, visRange, hitsBuffer2D, castLayers );
                        if ( hitCount == hitsBuffer2D.Length ) Debug.LogWarning ( "NetworkProximityChecker's OverlapCircle test for " + name + " has filled the whole buffer(" + hitsBuffer2D.Length + "). Some results might have been omitted. Consider increasing buffer size." );

                        for ( int i = 0; i < hitCount; i++ )
                        {
                            Collider2D hit = hitsBuffer2D [ i ];
                            NetworkIdentity identity = hit.GetComponentInParent<NetworkIdentity> ();
                            if ( identity != null && identity.connectionToClient != null )
                            {
                                observers.Add ( identity.connectionToClient );
                            }
                        }
                        break;
                    }
            }

            return true;
        }

        public override void OnSetLocalVisibility ( bool visible )
        {
            foreach ( Renderer rend in GetComponentsInChildren<Renderer> () )
            {
                rend.enabled = visible;
            }
        }
    }
}
