using UnityEngine;
using UnityEngine.Serialization;

namespace Humanlights.Unity.UNet
{
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkAnimator" )]
    [RequireComponent ( typeof ( NetworkIdentity ) )]
    public class NetworkAnimator : Humanlights.Unity.UNet.NetworkBehaviour
    {
        [FormerlySerializedAs ( "m_Animator" )]
        public Animator animator;

        int [] lastIntParameters;
        float [] lastFloatParameters;
        bool [] lastBoolParameters;
        AnimatorControllerParameter [] parameters;

        int [] animationHash;
        int [] transitionHash;
        float sendTimer;

        [Tooltip ( "Set to true if animations come from owner client,  set to false if animations always come from server" )]
        public bool clientAuthority;

        bool sendMessagesAllowed
        {
            get
            {
                if ( isServer )
                {
                    if ( !clientAuthority )
                        return true;

                    if ( netIdentity != null && netIdentity.clientAuthorityOwner == null )
                        return true;
                }

                return hasAuthority;
            }
        }

        void Awake ()
        {
            parameters = animator.parameters;
            lastIntParameters = new int [ parameters.Length ];
            lastFloatParameters = new float [ parameters.Length ];
            lastBoolParameters = new bool [ parameters.Length ];

            animationHash = new int [ animator.layerCount ];
            transitionHash = new int [ animator.layerCount ];
        }

        void FixedUpdate ()
        {
            if ( !sendMessagesAllowed )
                return;

            CheckSendRate ();

            for ( int i = 0; i < animator.layerCount; i++ )
            {
                int stateHash;
                float normalizedTime;
                if ( !CheckAnimStateChanged ( out stateHash, out normalizedTime, i ) )
                {
                    continue;
                }

                NetworkWriter writer = new NetworkWriter ();
                WriteParameters ( writer );

                SendAnimationMessage ( stateHash, normalizedTime, i, writer.ToArray () );
            }
        }

        bool CheckAnimStateChanged ( out int stateHash, out float normalizedTime, int layerId )
        {
            stateHash = 0;
            normalizedTime = 0;

            if ( animator.IsInTransition ( layerId ) )
            {
                AnimatorTransitionInfo tt = animator.GetAnimatorTransitionInfo ( layerId );
                if ( tt.fullPathHash != transitionHash [ layerId ] )
                {
                    transitionHash [ layerId ] = tt.fullPathHash;
                    animationHash [ layerId ] = 0;
                    return true;
                }
                return false;
            }

            AnimatorStateInfo st = animator.GetCurrentAnimatorStateInfo ( layerId );
            if ( st.fullPathHash != animationHash [ layerId ] )
            {
                if ( animationHash [ layerId ] != 0 )
                {
                    stateHash = st.fullPathHash;
                    normalizedTime = st.normalizedTime;
                }
                transitionHash [ layerId ] = 0;
                animationHash [ layerId ] = st.fullPathHash;
                return true;
            }
            return false;
        }

        void CheckSendRate ()
        {
            if ( sendMessagesAllowed && syncInterval != 0 && sendTimer < Time.time )
            {
                sendTimer = Time.time + syncInterval;

                NetworkWriter writer = new NetworkWriter ();
                if ( WriteParameters ( writer ) )
                {
                    SendAnimationParametersMessage ( writer.ToArray () );
                }
            }
        }

        void SendAnimationMessage ( int stateHash, float normalizedTime, int layerId, byte [] parameters )
        {
            if ( isServer )
            {
                RpcOnAnimationClientMessage ( stateHash, normalizedTime, layerId, parameters );
            }
            else if ( ClientScene.readyConnection != null )
            {
                CmdOnAnimationServerMessage ( stateHash, normalizedTime, layerId, parameters );
            }
        }

        void SendAnimationParametersMessage ( byte [] parameters )
        {
            if ( isServer )
            {
                RpcOnAnimationParametersClientMessage ( parameters );
            }
            else if ( ClientScene.readyConnection != null )
            {
                CmdOnAnimationParametersServerMessage ( parameters );
            }
        }

        void HandleAnimMsg ( int stateHash, float normalizedTime, int layerId, NetworkReader reader )
        {
            if ( hasAuthority )
                return;

            if ( stateHash != 0 )
            {
                animator.Play ( stateHash, layerId, normalizedTime );
            }

            ReadParameters ( reader );
        }

        void HandleAnimParamsMsg ( NetworkReader reader )
        {
            if ( hasAuthority )
                return;

            ReadParameters ( reader );
        }

        void HandleAnimTriggerMsg ( int hash )
        {
            animator.SetTrigger ( hash );
        }

        ulong NextDirtyBits ()
        {
            ulong dirtyBits = 0;
            for ( int i = 0; i < parameters.Length; i++ )
            {
                AnimatorControllerParameter par = parameters [ i ];
                bool changed = false;
                if ( par.type == AnimatorControllerParameterType.Int )
                {
                    int newIntValue = animator.GetInteger ( par.nameHash );
                    changed = newIntValue != lastIntParameters [ i ];
                    if ( changed )
                    {
                        lastIntParameters [ i ] = newIntValue;
                    }
                }
                else if ( par.type == AnimatorControllerParameterType.Float )
                {
                    float newFloatValue = animator.GetFloat ( par.nameHash );
                    changed = Mathf.Abs ( newFloatValue - lastFloatParameters [ i ] ) > 0.001f;
                    if ( changed )
                    {
                        lastFloatParameters [ i ] = newFloatValue;
                    }
                }
                else if ( par.type == AnimatorControllerParameterType.Bool )
                {
                    bool newBoolValue = animator.GetBool ( par.nameHash );
                    changed = newBoolValue != lastBoolParameters [ i ];
                    if ( changed )
                    {
                        lastBoolParameters [ i ] = newBoolValue;
                    }
                }
                if ( changed )
                {
                    dirtyBits |= 1ul << i;
                }
            }
            return dirtyBits;
        }

        bool WriteParameters ( NetworkWriter writer, bool forceAll = false )
        {
            ulong dirtyBits = forceAll ? ( ~0ul ) : NextDirtyBits ();
            writer.WritePackedUInt64 ( dirtyBits );
            for ( int i = 0; i < parameters.Length; i++ )
            {
                if ( ( dirtyBits & ( 1ul << i ) ) == 0 )
                    continue;

                AnimatorControllerParameter par = parameters [ i ];
                if ( par.type == AnimatorControllerParameterType.Int )
                {
                    int newIntValue = animator.GetInteger ( par.nameHash );
                    writer.WritePackedInt32 ( newIntValue );
                }
                else if ( par.type == AnimatorControllerParameterType.Float )
                {
                    float newFloatValue = animator.GetFloat ( par.nameHash );
                    writer.WriteSingle ( newFloatValue );
                }
                else if ( par.type == AnimatorControllerParameterType.Bool )
                {
                    bool newBoolValue = animator.GetBool ( par.nameHash );
                    writer.WriteBoolean ( newBoolValue );
                }
            }
            return dirtyBits != 0;
        }

        void ReadParameters ( NetworkReader reader )
        {
            ulong dirtyBits = reader.ReadPackedUInt64 ();
            for ( int i = 0; i < parameters.Length; i++ )
            {
                if ( ( dirtyBits & ( 1ul << i ) ) == 0 )
                    continue;

                AnimatorControllerParameter par = parameters [ i ];
                if ( par.type == AnimatorControllerParameterType.Int )
                {
                    int newIntValue = reader.ReadPackedInt32 ();
                    animator.SetInteger ( par.nameHash, newIntValue );
                }
                else if ( par.type == AnimatorControllerParameterType.Float )
                {
                    float newFloatValue = reader.ReadSingle ();
                    animator.SetFloat ( par.nameHash, newFloatValue );
                }
                else if ( par.type == AnimatorControllerParameterType.Bool )
                {
                    bool newBoolValue = reader.ReadBoolean ();
                    animator.SetBool ( par.nameHash, newBoolValue );
                }
            }
        }

        public override bool OnSerialize ( NetworkWriter writer, bool forceAll )
        {
            if ( forceAll )
            {
                for ( int i = 0; i < animator.layerCount; i++ )
                {
                    if ( animator.IsInTransition ( i ) )
                    {
                        AnimatorStateInfo st = animator.GetNextAnimatorStateInfo ( i );
                        writer.WriteInt32 ( st.fullPathHash );
                        writer.WriteSingle ( st.normalizedTime );
                    }
                    else
                    {
                        AnimatorStateInfo st = animator.GetCurrentAnimatorStateInfo ( i );
                        writer.WriteInt32 ( st.fullPathHash );
                        writer.WriteSingle ( st.normalizedTime );
                    }
                }
                WriteParameters ( writer, forceAll );
                return true;
            }
            return false;
        }

        public override void OnDeserialize ( NetworkReader reader, bool initialState )
        {
            if ( initialState )
            {
                for ( int i = 0; i < animator.layerCount; i++ )
                {
                    int stateHash = reader.ReadInt32 ();
                    float normalizedTime = reader.ReadSingle ();
                    animator.Play ( stateHash, i, normalizedTime );
                }

                ReadParameters ( reader );
            }
        }

        public void SetTrigger ( string triggerName )
        {
            SetTrigger ( Animator.StringToHash ( triggerName ) );
        }

        public void SetTrigger ( int hash )
        {
            if ( hasAuthority && clientAuthority )
            {
                if ( ClientScene.readyConnection != null )
                {
                    CmdOnAnimationTriggerServerMessage ( hash );
                }
                return;
            }

            if ( isServer && !clientAuthority )
            {
                RpcOnAnimationTriggerClientMessage ( hash );
            }
        }

        #region server message handlers
        [ClientCmd]
        void CmdOnAnimationServerMessage ( int stateHash, float normalizedTime, int layerId, byte [] parameters )
        {
            if ( LogFilter.Debug ) Debug.Log ( "OnAnimationMessage for netId=" + netId );

            HandleAnimMsg ( stateHash, normalizedTime, layerId, new NetworkReader ( parameters ) );
            RpcOnAnimationClientMessage ( stateHash, normalizedTime, layerId, parameters );
        }

        [ClientCmd]
        void CmdOnAnimationParametersServerMessage ( byte [] parameters )
        {
            HandleAnimParamsMsg ( new NetworkReader ( parameters ) );
            RpcOnAnimationParametersClientMessage ( parameters );
        }

        [ClientCmd]
        void CmdOnAnimationTriggerServerMessage ( int hash )
        {
            HandleAnimTriggerMsg ( hash );
            RpcOnAnimationTriggerClientMessage ( hash );
        }
        #endregion

        #region client message handlers
        [ServerCmd]
        void RpcOnAnimationClientMessage ( int stateHash, float normalizedTime, int layerId, byte [] parameters )
        {
            HandleAnimMsg ( stateHash, normalizedTime, layerId, new NetworkReader ( parameters ) );
        }

        [ServerCmd]
        void RpcOnAnimationParametersClientMessage ( byte [] parameters )
        {
            HandleAnimParamsMsg ( new NetworkReader ( parameters ) );
        }

        [ServerCmd]
        void RpcOnAnimationTriggerClientMessage ( int hash )
        {
            HandleAnimTriggerMsg ( hash );
        }
        #endregion
    }
}
