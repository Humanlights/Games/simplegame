using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Humanlights.Unity.UNet
{
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkLobbyPlayer" )]
    [Obsolete ( "Use / inherit from NetworkRoomPlayer instead" )]
    public class NetworkLobbyPlayer : Humanlights.Unity.UNet.NetworkRoomPlayer { }
}
