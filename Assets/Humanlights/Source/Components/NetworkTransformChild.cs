using UnityEngine;

namespace Humanlights.Unity.UNet
{
    [AddComponentMenu ( "Network/NetworkTransformChild" )]
    public class NetworkTransformChild : NetworkTransformBase
    {
        public Transform target;
        protected override Transform targetComponent => target;
    }
}
