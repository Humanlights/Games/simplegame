using UnityEngine;
using UnityEngine.SceneManagement;

namespace Humanlights.Unity.UNet
{
    [DisallowMultipleComponent]
    [AddComponentMenu ( "Network/NetworkRoomPlayer" )]
    public class NetworkRoomPlayer : Humanlights.Unity.UNet.NetworkBehaviour
    {
        public bool showRoomGUI = true;

        [SyncVar ( hook = nameof ( ReadyStateChanged ) )]
        public bool readyToBegin;

        [SyncVar]
        public int index;

        #region Unity Callbacks

        public void Start ()
        {
            if ( NetworkManager.singleton is NetworkRoomManager room )
            {
                if ( room.dontDestroyOnLoad )
                    DontDestroyOnLoad ( gameObject );

                OnClientEnterRoom ();
            }
            else
                Debug.LogError ( "RoomPlayer could not find a NetworkRoomManager. The RoomPlayer requires a NetworkRoomManager object to function. Make sure that there is one in the scene." );
        }

        #endregion

        #region Commands

        [ClientCmd]
        public void CmdChangeReadyState ( bool readyState )
        {
            readyToBegin = readyState;
            NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
            if ( room != null )
            {
                room.ReadyStatusChanged ();
            }
        }

        #endregion

        #region SyncVar Hooks

        void ReadyStateChanged ( bool newReadyState )
        {
            OnClientReady ( newReadyState );
        }

        #endregion

        #region Room Client Virtuals

        public virtual void OnClientEnterRoom () { }

        public virtual void OnClientExitRoom () { }

        public virtual void OnClientReady ( bool readyState ) { }

        #endregion

        #region Optional UI

        public virtual void OnGUI ()
        {
            if ( !showRoomGUI )
                return;

            NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
            if ( room )
            {
                if ( !room.showRoomGUI )
                    return;

                if ( SceneManager.GetActiveScene ().name != room.RoomScene )
                    return;

                GUILayout.BeginArea ( new Rect ( 20f + ( index * 100 ), 200f, 90f, 130f ) );

                GUILayout.Label ( $"Player [{index + 1}]" );

                if ( readyToBegin )
                    GUILayout.Label ( "Ready" );
                else
                    GUILayout.Label ( "Not Ready" );

                if ( ( ( isServer && index > 0 ) || isServerOnly ) && GUILayout.Button ( "REMOVE" ) )
                {
                    GetComponent<NetworkIdentity> ().connectionToClient.Disconnect ();
                }

                GUILayout.EndArea ();

                if ( NetworkClient.active && isLocalPlayer )
                {
                    GUILayout.BeginArea ( new Rect ( 20f, 300f, 120f, 20f ) );

                    if ( readyToBegin )
                    {
                        if ( GUILayout.Button ( "Cancel" ) )
                            CmdChangeReadyState ( false );
                    }
                    else
                    {
                        if ( GUILayout.Button ( "Ready" ) )
                            CmdChangeReadyState ( true );
                    }

                    GUILayout.EndArea ();
                }
            }
        }

        #endregion
    }
}
