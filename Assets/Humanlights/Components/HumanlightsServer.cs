﻿using Humanlights.Extensions;
using Humanlights.Unity.Network;
using Humanlights.Unity.Network.Server;
using Humanlights.Unity.UNet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    public class HumanlightsServer : HumanlightsSingleton<HumanlightsServer>
    {
        public static HumanlightsNetworkManager Manager { get { return HumanlightsNetworkManager.Instance; } }

        public bool StartServerOnStart = true;
        public new bool DontDestroyOnLoad = false;

        public const string IpArg = "+ip";
        public const string PortArg = "+port";

        [LargeHeader ( "Console Server" )]
        public bool EnableConsole = true;
        public string ConsoleTitle = "Game Server";
        [Help ( "This is executed only in Standalone builds (NOT Unity Editor)." )]
        public string IP = "localhost";
        public ushort Port = 7777;
        public int MaximumConnections = 15;

        public ConsoleInput Input { get; set; }
        public ConsoleWindow Window { get; set; }

        public void ReadAddress ()
        {
            IP = CommandLineEx.GetArgumentResult ( IpArg, Manager.GetLocalIPAddress () );
            Port = ( ushort ) StringEx.ToInt ( CommandLineEx.GetArgumentResult ( PortArg, Port.ToString () ) );
        }

#if SERVER || (SERVER && CLIENT)

        public void Start ()
        {
            ReadAddress ();

            if ( DontDestroyOnLoad ) DontDestroyOnLoad ( gameObject );

            if ( StartServerOnStart )
            {
                if ( !Application.isEditor )
                {
                    Input = new ConsoleInput ();
                    Window = new ConsoleWindow ();

                    Input.OnInputText += OnInputText;
                    Window.Initialize ();
                    Window.SetTitle ( ConsoleTitle );
                }

                Application.logMessageReceived += HandleLog;

                DebugEx.Log ( $"Console Started\n" );

                HandleServerCallbacks ();

#if SERVER && CLIENT
                Manager.HeadlessMode = true;
                StartHost ();
#else
                StartServer ();
#endif
            }
        }
        public void Update ()
        {
            Input?.Update ();
        }
        public override void OnDestroy ()
        {
            Window?.Shutdown ();

            base.OnDestroy ();
        }

#endif

        public void HandleServerCallbacks ()
        {
            Manager.OnServerConnect += ( NetworkConnection connection ) =>
            {
                DebugEx.Log ( "Server", $"Client connected: {connection.address}/{connection.connectionId}" );
                PrintCurrentConnections ();
            };
            Manager.OnServerDisconnect += ( NetworkConnection connection ) =>
            {
                DebugEx.Log ( "Server", $"Disconnected: {connection.connectionId}" );
                PrintCurrentConnections ();
            };
        }
        public virtual void StartServer ()
        {
            Manager.StartServer ( IP, Port, MaximumConnections );

            DebugEx.Log ( "Server", $"Starting server on {IP}:{Port}" );
        }
        public virtual void StartHost ()
        {
            Manager.StartHost ( port: Port, maximumConnections: MaximumConnections );

            DebugEx.Log ( "Server", $"Starting server & hosting on {Manager.Ip}:{Manager.Port}" );
        }

        private void PrintCurrentConnections ()
        {
            DebugEx.Log ( "Server", $"  Clients: {Manager.ConnectionCount}/{Manager.MaximumConnections} connected" );
        }

        public virtual void OnInputText ( string text ) { /* Process the console input. */ }
        public virtual void HandleLog ( string message, string stackTrace, LogType type )
        {
            if ( type == LogType.Warning ) Console.ForegroundColor = ConsoleColor.Yellow;
            else if ( type == LogType.Error ) Console.ForegroundColor = ConsoleColor.Red;
            else Console.ForegroundColor = ConsoleColor.White;

            //
            // We're half way through typing something, so clear this line ..
            //
            if ( Console.CursorLeft != 0 )
                Input?.ClearLine ();

            Console.WriteLine ( message );

            //
            // If we were typing something re-add it.
            //
            Input?.RedrawInputLine ();
        }

        [ContextMenu ( "Get Local Address" )]
        public void GetLocalAddress ()
        {
            IP = Manager.GetLocalIPAddress ();
        }
        [ContextMenu ( "Get Random Port" )]
        public void GetRandomPort ()
        {
            Port = Manager.GetRandomPort ();
        }
    }
}