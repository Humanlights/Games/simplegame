﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    public class HumanlightsNetworkEditor
    {
        public const string ClientServerName = "Humanlights/Network Switch/Client + Server";
        public const string ServerName = "Humanlights/Network Switch/Server";
        public const string ClientName = "Humanlights/Network Switch/Client";

        [MenuItem ( ClientServerName )]
        public static void SwitchClientServer ()
        {
            SetEnabled ( "CLIENT", true );
            SetEnabled ( "SERVER", true );
        }
        [MenuItem ( ClientServerName, true )]
        private static bool SwitchClientServerValidate ()
        {
            return !( GetEnabled ( "CLIENT" ) && GetEnabled ( "SERVER" ) );
        }

        [MenuItem ( ClientName )]
        public static void SwitchClient ()
        {
            SetEnabled ( "SERVER", false );
            SetEnabled ( "CLIENT", true );
        }
        [MenuItem ( ClientName, true )]
        private static bool SwitchClientValidate ()
        {
            if ( GetEnabled ( "CLIENT" ) && GetEnabled ( "SERVER" ) ) return true;
            return !GetEnabled ( "CLIENT" );
        }

        [MenuItem ( ServerName )]
        public static void SwitchServer ()
        {
            SetEnabled ( "CLIENT", false );
            SetEnabled ( "SERVER", true );
        }
        [MenuItem ( ServerName, true )]
        private static bool SwitchServerValidate ()
        {
            if ( GetEnabled ( "CLIENT" ) && GetEnabled ( "SERVER" ) ) return true;
            return !GetEnabled ( "SERVER" );
        }

        private static BuildTargetGroup [] BuildTargetGroups = new BuildTargetGroup []
            {
                BuildTargetGroup.Standalone,
                BuildTargetGroup.Android,
                BuildTargetGroup.iOS
            };
        private static BuildTargetGroup [] MobileBuildTargetGroups = new BuildTargetGroup []
            {
                BuildTargetGroup.Android,
                BuildTargetGroup.iOS,
                BuildTargetGroup.WSA
            };
        private static List<string> GetDefinesList ( BuildTargetGroup group )
        {
            var list = PlayerSettings.GetScriptingDefineSymbolsForGroup ( group ).Split ( ';' );

            return new List<string> ( list );
        }

        private static void SetEnabled ( string defineName, bool enable, bool mobile = false )
        {
            foreach ( var group in mobile ? MobileBuildTargetGroups : BuildTargetGroups )
            {
                var defines = GetDefinesList ( group );

                if ( enable )
                {
                    if ( defines.Contains ( defineName ) )
                    {
                        return;
                    }
                    defines.Add ( defineName );
                }
                else
                {
                    if ( !defines.Contains ( defineName ) )
                    {
                        return;
                    }
                    while ( defines.Contains ( defineName ) )
                    {
                        defines.Remove ( defineName );
                    }
                }

                var definesString = string.Join ( ";", defines.ToArray () );
                PlayerSettings.SetScriptingDefineSymbolsForGroup ( group, definesString );
            }
        }
        private static bool GetEnabled ( string defineName, bool mobile = false )
        {
            foreach ( var group in mobile ? MobileBuildTargetGroups : BuildTargetGroups )
            {
                if ( PlayerSettings.GetScriptingDefineSymbolsForGroup ( group ).ToLower ().Contains ( defineName.ToLower () ) ) return true;
            }

            return false;
        }
    }
}