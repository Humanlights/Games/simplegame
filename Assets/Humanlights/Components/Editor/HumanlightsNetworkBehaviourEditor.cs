﻿using Humanlights.Unity.Network;
using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor ( typeof ( HumanlightsNetworkBehaviour ), true, isFallback = true )]
public class HumanlightsNetworkBehaviourEditor : Humanlights.Base.HumanlightsEditor<HumanlightsNetworkBehaviour>
{
    public override void CustomDraw ()
    {
        UUI.VerticalGroup ( () =>
        {
            UUI.LabelH1 ( "Network Stats" );
            EditorInstance.SyncInterval = UUI.FloatSliderField ( "Sync Interval (Seconds)", 0f, 2f, EditorInstance.SyncInterval );
            EditorInstance.SyncMode = UUI.EnumField ( "Sync Mode", EditorInstance.SyncMode );

            UUI.LabelH3 ( "Local Stats" );
            UUI.IntField ( "Net Id", ( int ) EditorInstance.NetId, readOnly: true );
            UUI.ToggleField ( "Is Client", EditorInstance.IsClient, readOnly: true );
            UUI.ToggleField ( "Is Server", EditorInstance.IsServer, readOnly: true );
            UUI.ToggleField ( "Is Host", EditorInstance.IsHost, readOnly: true );
            UUI.ToggleField ( "Is Local Player", EditorInstance.IsLocalPlayer, readOnly: true );
            UUI.ToggleField ( "Is Server Only", EditorInstance.IsServerOnly, readOnly: true );
            UUI.ToggleField ( "Is Client Only", EditorInstance.IsClientOnly, readOnly: true );
            UUI.ToggleField ( "Has Authority", EditorInstance.HasAuthority, readOnly: true );
        } );

        UUI.Space ( 10 );

        UUI.VerticalGroup ( () =>
        {
            UUI.LabelH1 ( $"Script Properties" );

            base.CustomDraw ();
        } );

    }
}