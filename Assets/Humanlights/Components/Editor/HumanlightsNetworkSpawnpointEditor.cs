﻿using Humanlights.Unity.Network;
using UnityEditor;
using UnityEngine;

[CustomEditor ( typeof ( HumanlightsNetworkSpawnpoint ) )]
public class HumanlightsNetworkSpawnpointEditor : Humanlights.Base.HumanlightsEditor<HumanlightsNetworkSpawnpoint>
{
    public override void CustomDraw ()
    {
        if ( Application.isPlaying )
        {
            UUI.CenterGroup ( () =>
            {
                UUI.LabelShy ( $"Id #{EditorInstance.Id}" );
            } );
        }
    }
}