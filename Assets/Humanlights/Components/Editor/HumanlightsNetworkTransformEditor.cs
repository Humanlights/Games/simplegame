﻿using Humanlights.Unity.Network;
using UnityEditor;

[CustomEditor ( typeof ( HumanlightsNetworkTransform ) )]
public class HumanlightsNetworkTransformEditor : HumanlightsNetworkBehaviourEditor
{
    public HumanlightsNetworkTransform EditorInstance2 => serializedObject.targetObject as HumanlightsNetworkTransform;

    public override void CustomDraw ()
    {
        EditorInstance2.Target = UUI.ObjectField ( "Target", EditorInstance2.Target );
        EditorInstance2.Compression = UUI.EnumField ( "Compression Type", EditorInstance2.Compression );

        UUI.Help (
            $"No Compression:\n\tCould be used mainly for car controllers, quick and accurate movements.\n\n" +
            $"Low Compression:\n\tIt considers 50% of the position from a start point to an end point (within the synchronization interval).\n\n" +
            $"Medium Compression:\n\tIt considers 25% of the position from a start point to an end point (within the synchronization interval).\n\n" +
            $"No Rotation:\n\tJust does not synchronize the rotation of the object over the network.", MessageType.Info );

        base.CustomDraw ();
    }
}