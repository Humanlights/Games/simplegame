﻿using Humanlights.Unity.Network;
using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor ( typeof ( HumanlightsNetworkable ) )]
public class HumanlightsNetworkableEditor : Humanlights.Base.HumanlightsEditor<HumanlightsNetworkable>
{
    public override void CustomDraw ()
    {
        UUI.VerticalGroup ( () =>
        {
            UUI.LabelH1 ( "Network Stats" );
            UUI.LabelField ( "Asset ID", EditorInstance.AssetId, selectableText: true );
            UUI.LabelField ( "Scene ID", EditorInstance.SceneId, selectableText: true );

            UUI.LabelH3 ( "Local Stats" );
            UUI.IntField ( "Net Id", ( int ) EditorInstance.NetId, readOnly: true );
            UUI.ToggleField ( "Is Client", EditorInstance.IsClient, readOnly: true );
            UUI.ToggleField ( "Is Server", EditorInstance.IsServer, readOnly: true );
            UUI.ToggleField ( "Is Host", EditorInstance.IsHost, readOnly: true );
            UUI.ToggleField ( "Is Local Player", EditorInstance.IsLocalPlayer, readOnly: true );
            UUI.ToggleField ( "Has Authority", EditorInstance.HasAuthority, readOnly: true );
        } );

        UUI.Space ( 10 );

        EditorInstance.ServerOnly = UUI.ToggleField ( "Server Only", EditorInstance.ServerOnly );
    }
}