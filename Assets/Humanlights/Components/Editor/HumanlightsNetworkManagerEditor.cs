﻿using Humanlights.Extensions;
using Humanlights.Unity.Network;
using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    [CustomEditor ( typeof ( HumanlightsNetworkManager ), true, isFallback = true )]
    public class HumanlightsNetworkManagerEditor : Humanlights.Base.HumanlightsEditor<HumanlightsNetworkManager>
    {
        public HumanlightsNetworkStatistics Statistics { get { return EditorInstance.GetStatistics (); } }

        public override void CustomDraw ()
        {
            UUI.VerticalGroup ( () =>
            {
                UUI.LabelH1 ( "Configurations" );

                EditorInstance.DontDestroyOnLoad = UUI.ToggleField ( "Don't Destroy On Load", EditorInstance.DontDestroyOnLoad );
                EditorInstance.RunInBackground = UUI.ToggleField ( "Run In Background", EditorInstance.RunInBackground );
                EditorInstance.HeadlessMode = UUI.ToggleField ( "Headless Mode", EditorInstance.HeadlessMode );
                UUI.Help ( $"Could be used for dedicated servers which run as consoles. Not sure what special qualities it does but it PROBABLY increases performance.", MessageType.Warning );

                UUI.Space ();
                EditorInstance.ServerFrameRate = UUI.IntSliderField ( "Tick Rate (Hz)", 1, 260, EditorInstance.ServerFrameRate );
                UUI.Help ( $"30Hz is the standard of a 'walking around' game. 60Hz to 120Hz is for games like Battlefield V and quick games in general, which synchronize pretty much instantly.\n\n1-10Hz: Low Quality\n30Hz: Medium Quality\n60-120Hz: Highest Quality", MessageType.Info );

                UUI.LabelH3 ( "Connection" );
                {
                    EditorInstance.ServerMaxMessageSize = ( ushort ) UUI.IntField ( "Server Max. Message Size", EditorInstance.ServerMaxMessageSize );
                    EditorInstance.ClientMaxMessageSize = ( ushort ) UUI.IntField ( "Client Max. Message Size", EditorInstance.ClientMaxMessageSize );

                    UUI.VerticalGroup ( () =>
                    {
                        EditorInstance.Ip = UUI.StringField ( "IP", EditorInstance.Ip );
                        EditorInstance.Port = ( ushort ) UUI.IntField ( "Port", EditorInstance.Port );
                        EditorInstance.MaximumConnections = UUI.IntField ( "Maximum Connections", EditorInstance.MaximumConnections );
                    }, box: true );
                }

                UUI.LabelH3 ( "Spawn" );
                {
                    EditorInstance.PlayerSpawnMethod = UUI.EnumField ( "Player Spawn Method", EditorInstance.PlayerSpawnMethod );
                    EditorInstance.PlayerPrefab = UUI.ObjectField ( "Player Prefab", EditorInstance.PlayerPrefab );
                    EditorInstance.AutoCreatePlayer = UUI.ToggleField ( "Auto-Create Player", EditorInstance.AutoCreatePlayer );
                    UUI.IntField ( "Spawn Points", EditorInstance.SpawnPoints.Length, readOnly: true );
                }

                UUI.LabelH3 ( "Spawnable Prefabs" );
                {
                    for ( int i = 0; i < EditorInstance.SpawnablePrefabs.Count; i++ )
                    {
                        var prefab = EditorInstance.SpawnablePrefabs [ i ];

                        UUI.HorizontalGroup ( () =>
                        {
                            EditorInstance.SpawnablePrefabs [ i ] = UUI.ObjectField ( $"Prefab {StringEx.ToNumbered ( i + 1 )}", EditorInstance.SpawnablePrefabs [ i ] );
                            UUI.ButtonImportant ( "X", () => { EditorInstance.SpawnablePrefabs.RemoveAt ( i ); } );
                        } );
                    }

                    UUI.Space ();
                    UUI.HorizontalGroup ( () =>
                    {
                        UUI.ButtonNormal ( "Add Spawnable Prefab", () => { EditorInstance.SpawnablePrefabs.Add ( null ); }, false );
                    } );
                }
            }, true );

            UUI.VerticalGroup ( () =>
            {
                UUI.LabelH1 ( "Network Info" );

                UUI.ToggleField ( "Is Connected", EditorInstance.IsConnected, readOnly: true );
                UUI.EnumField ( "Connection Type", EditorInstance.ConnectionType, readOnly: true );
            }, true );

            base.CustomDraw ();
        }
    }
}