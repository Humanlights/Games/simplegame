﻿using Humanlights.Unity.UNet;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    [DisallowMultipleComponent]
    public class HumanlightsNetworkSpawnpoint : HumanlightsListableBehaviour<HumanlightsNetworkSpawnpoint>
    {
        public HumanlightsNetworkManager Manager { get { return HumanlightsNetworkManager.Instance; } }

        public int Id { get { return Manager.SpawnPoints.ToList ().IndexOf ( this ); } }

        public void Awake ()
        {
            Manager?.RegisterSpawnpoint ( this );
        }

        public override void OnDestroy ()
        {
            Manager?.UnregisterSpawnpoint ( this );

            base.OnDestroy ();
        }
    }
}