﻿using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    public class HumanlightsClient : HumanlightsNetworkBehaviour
    {
        public static HumanlightsClient Instance { get; private set; }

        public new bool DontDestroyOnLoad = false;

        [ReadOnly] [SyncVar ( hook = nameof ( CmdChangeMaximumConnectionCount ) )] public int MaximumConnectionCount;
        [ReadOnly] [SyncVar ( hook = nameof ( CmdChangeConnectedUserCount ) )] public int ConnectedUserCount;
        [ReadOnly] [SyncVar ( hook = nameof ( CmdChangeFrameRate ) )] public int FrameRate;

        public void Awake ()
        {
            Instance = this;
        }

#if SERVER || ( SEVER && CLIENT )

        public void Start ()
        {
            if ( DontDestroyOnLoad ) DontDestroyOnLoad ( gameObject );

            InvokeRepeating ( () =>
            {
                CmdChangeMaximumConnectionCount ( MaximumConnectionCount = HumanlightsNetworkManager.Instance.MaximumConnections );
                CmdChangeConnectedUserCount ( ConnectedUserCount = HumanlightsNetworkManager.Instance.ConnectionCount );
                CmdChangeMaximumConnectionCount ( FrameRate = ( int ) ( 1f / Time.unscaledDeltaTime ) );
            }, SyncInterval, SyncInterval );
        }

#endif

        public void Reset ()
        {
            MaximumConnectionCount = 0;
            ConnectedUserCount = 0;
            FrameRate = 0;
        }

        [ClientCmd] public void CmdChangeMaximumConnectionCount ( int value ) { RpcChangeMaximumConnectionCount ( MaximumConnectionCount = value ); }
        [ServerCmd] public void RpcChangeMaximumConnectionCount ( int value ) { MaximumConnectionCount = value; }

        [ClientCmd] public void CmdChangeConnectedUserCount ( int value ) { RpcChangeConnectedUserCount ( ConnectedUserCount = value ); }
        [ServerCmd] public void RpcChangeConnectedUserCount ( int value ) { ConnectedUserCount = value; }

        [ClientCmd] public void CmdChangeFrameRate ( int value ) { RpcChangeFrameRate ( FrameRate = value ); }
        [ServerCmd] public void RpcChangeFrameRate ( int value ) { FrameRate = value; }
    }
}