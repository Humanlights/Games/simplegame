using System;

namespace Humanlights.Unity.Network.Server
{
    public class ConsoleInput
    {
        public event Action<string> OnInputText;
        public string InputString { get; set; }

        public void ClearLine ()
        {
            Console.CursorLeft = 0;
            Console.Write ( new string ( ' ', Console.BufferWidth ) );
            Console.CursorTop--;
            Console.CursorLeft = 0;
        }

        public void RedrawInputLine ()
        {
            if ( InputString?.Length == 0 ) return;

            if ( Console.CursorLeft > 0 )
                ClearLine ();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write ( InputString );
        }

        internal void OnBackspace ()
        {
            if ( InputString.Length < 1 ) return;

            InputString = InputString.Substring ( 0, InputString.Length - 1 );
            RedrawInputLine ();
        }

        internal void OnEscape ()
        {
            ClearLine ();
            InputString = "";
        }

        internal void OnEnter ()
        {
            ClearLine ();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine ( "> " + InputString );

            var strtext = InputString;
            InputString = "";

            OnInputText?.Invoke ( strtext );
        }

        public void Update ()
        {
            if ( !Console.KeyAvailable ) return;
            var key = Console.ReadKey ();

            if ( key.Key == ConsoleKey.Enter )
            {
                OnEnter ();
                return;
            }

            if ( key.Key == ConsoleKey.Backspace )
            {
                OnBackspace ();
                return;
            }

            if ( key.Key == ConsoleKey.Escape )
            {
                OnEscape ();
                return;
            }

            if ( key.KeyChar != '\u0000' )
            {
                InputString += key.KeyChar;
                RedrawInputLine ();
                return;
            }
        }
    }
}