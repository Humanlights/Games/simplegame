using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;

namespace Humanlights.Unity.Network.Server
{
    public class ConsoleWindow
    {
        private TextWriter OldOutput { get; set; }

        public void Initialize ()
        {
            //
            // Attach to any existing consoles. 
            // If have failing that, create a new one.
            //
            if ( !AttachConsole ( 0x0ffffffff ) )
            {
                AllocConsole ();
            }

            OldOutput = Console.Out;

            try
            {
                var stdHandle = GetStdHandle ( STD_OUTPUT_HANDLE );
                var safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle ( stdHandle, true );
                var fileStream = new FileStream ( safeFileHandle, FileAccess.Write );
                var encoding = System.Text.Encoding.ASCII;
                var sw = new StreamWriter ( fileStream, encoding );
                sw.AutoFlush = true;

                Console.SetOut ( sw );
            }
            catch ( Exception e )
            {
                DebugEx.Error ( "Console", $"Couldn't redirect output: {e.Message}" );
            }
        }

        public void Shutdown ()
        {
            Console.SetOut ( OldOutput );
            FreeConsole ();
        }

        public void SetTitle ( string strName )
        {
            SetConsoleTitle ( strName );
        }

        private const int STD_OUTPUT_HANDLE = -11;

        [DllImport ( "kernel32.dll", SetLastError = true )]
        static extern bool AttachConsole ( uint dwProcessId );

        [DllImport ( "kernel32.dll", SetLastError = true )]
        static extern bool AllocConsole ();

        [DllImport ( "kernel32.dll", SetLastError = true )]
        static extern bool FreeConsole ();

        [DllImport ( "kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall )]
        private static extern IntPtr GetStdHandle ( int nStdHandle );

        [DllImport ( "kernel32.dll" )]
        static extern bool SetConsoleTitle ( string lpConsoleTitle );
    }
}