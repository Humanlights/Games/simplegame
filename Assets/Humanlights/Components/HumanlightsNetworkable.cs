﻿using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    public class HumanlightsNetworkable : NetworkIdentity
    {
        public bool IsServer { get { return isServer; } }
        public bool IsClient { get { return isClient; } }
        public bool IsHost { get { return isClient && isServer; } }
        public bool IsLocalPlayer { get { return isLocalPlayer; } }
        public bool HasAuthority { get { return hasAuthority; } }

        public bool ServerOnly { get { return serverOnly; } set { serverOnly = value; } }

        public System.Guid AssetId { get { return assetId; } }
        public ulong SceneId { get { return sceneId; } }

        public uint NetId { get { return netId; } }
    }
}