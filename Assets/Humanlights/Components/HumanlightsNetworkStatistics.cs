﻿using Humanlights.Unity.UNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Humanlights.Unity.Network
{
    public class HumanlightsNetworkStatistics
    {
        public long CurrentBytesPerSecondSent { get; set; }
        public long CurrentBytesPerSecondReceived { get; set; }
        public long TotalBytesPerSecondSent { get; set; }
        public long TotalBytesPerSecondReceived { get; set; }

        public int TotalMessagesInBuffer { get; set; }
        public DateTick? ConnectionStartTime { get; set; }

        public double ConnectionPassedTime { get { if ( ConnectionStartTime == null ) return 0; return ( DateTime.Now - ConnectionStartTime.Value.Date.DateTime ).TotalSeconds; } }
    }
}
