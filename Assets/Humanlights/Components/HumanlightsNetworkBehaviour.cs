﻿using Humanlights.Managers;
using Humanlights.Unity.Network;
using Humanlights.Unity.UNet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    [RequireComponent ( typeof ( HumanlightsNetworkable ) )]
    public class HumanlightsNetworkBehaviour : NetworkBehaviour
    {
        public HumanlightsNetworkManager Manager { get { return HumanlightsNetworkManager.Instance; } }
        public HumanlightsNetworkable Networkable { get { return netIdentity as HumanlightsNetworkable; } }

        public SyncMode SyncMode { get { return syncMode; } set { syncMode = value; } }
        public float SyncInterval { get { return syncInterval; } set { syncInterval = value; } }

        public bool IsServer { get { return isServer; } }
        public bool IsClient { get { return isClient; } }
        public bool IsHost { get { return isClient && isServer; } }
        public bool IsLocalPlayer { get { return isLocalPlayer; } }
        public bool IsServerOnly { get { return isServerOnly; } }
        public bool IsClientOnly { get { return isClientOnly; } }
        public bool HasAuthority { get { return hasAuthority; } }

        public uint NetId { get { return netId; } }

        #region HumanlightsBehaviour

        #region Invoke

        /// <summary>
        /// Invokes a method after the delay directly from the invoke manager.
        /// </summary>
        /// <param name="method">Action method that will.</param>
        /// <param name="delay">Time to wait until the method is executed.</param>
        /// <param name="register"></param>
        /// <param name="debugs"></param>
        public void Invoke ( Action method, float delay, bool register = true, bool debugs = true )
        {
            InvokeManager.Check ();
            InvokeManager.Invoke ( this, method, delay, register );
        }

        /// <summary>
        /// Invokes a method after the delay directly from the invoke manager.
        /// </summary>
        /// <param name="method">Action method that will.</param>
        /// <param name="delay">Time to wait until the method is executed.</param>
        /// <param name="repeatRate"></param>
        /// <param name="register"></param>
        /// <param name="debugs"></param>
        public void InvokeRepeating ( Action method, float delay, float repeatRate, bool register = true, bool debugs = true )
        {
            InvokeManager.Check ();
            InvokeManager.InvokeRepeating ( this, method, delay, repeatRate, register );
        }

        #endregion

        #region Coroutine

        /// <summary>
        /// Runs a routine with the CoroutineManager.
        /// </summary>
        /// <param name="routine">The aimed routine to run.</param>
        /// <param name="register"></param>
        /// <param name="debugs"></param>
        public IEnumerator RunCoroutine ( IEnumerator routine, bool register = true, bool debugs = true )
        {
            CoroutineManager.Check ();

            if ( routine == null )
            {
                return routine;
            }

            CoroutineManager.Run ( routine, register, this );

            return routine;
        }

        /// <summary>
        /// Cancels a routine from the CoroutineManager.
        /// </summary>
        /// <param name="routine">The aimed routine to run.</param>
        /// <param name="debugs"></param>
        public void CancelCoroutine ( IEnumerator routine, bool debugs = true )
        {
            CoroutineManager.Check ();

            if ( routine == null )
            {
                return;
            }

            CoroutineManager.Cancel ( routine, this );
        }

        #endregion

        #region Task

        public int Task ( string description )
        {
            return TaskManager.CreateTask ( description, gameObject.GetInstanceID () );
        }

        public void CancelTask ( int id )
        {
            TaskManager.RemoveTask ( id );
        }

        #endregion

        public virtual void OnDestroy ()
        {
            StopAllCoroutines ();
            CancelInvoke ();
        }

        #endregion
    }
}