﻿using Humanlights.Unity.UNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    public class HumanlightsNetworkTransform : HumanlightsNetworkBehaviour
    {
        [HideInInspector] public CompressionTypes Compression = CompressionTypes.NoCompression;
        public enum CompressionTypes
        {
            /// <summary>
            /// Could be used mainly for car controllers, quick and accurate movements.
            /// </summary>
            NoCompression,

            /// <summary>
            /// It considers 50% of the position from a start point to an end point (within the synchronization interval).
            /// </summary>
            LowCompression,

            /// <summary>
            /// It considers 25% of the position from a start point to an end point (within the synchronization interval).
            /// </summary>
            MediumCompression,

            /// <summary>
            /// Just does not synchronize the rotation of the object over the network.
            /// </summary>
            NoRotation
        };

        [HideInInspector] public Transform _target;
        public Transform Target { get { return _target != null ? _target : ( _target = GetComponent<Transform> () ); } set { _target = value; } }

        private Vector3 LastPosition;
        private Quaternion LastRotation;
        private Vector3 LastScale;
        private float LastClientSendTime;

        private StatePoint StartPoint;
        private StatePoint EndPoint;

        private static void SerializeIntoWriter ( NetworkWriter writer, Vector3 position, Quaternion rotation, Vector3 scale, CompressionTypes compression )
        {
            writer.WriteVector3 ( position );

            var euler = rotation.eulerAngles;

            switch ( compression )
            {
                case CompressionTypes.NoCompression:
                    writer.WriteSingle ( euler.x );
                    writer.WriteSingle ( euler.y );
                    writer.WriteSingle ( euler.z );
                    break;

                case CompressionTypes.LowCompression:
                    writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.x, 0, 360, byte.MinValue, byte.MaxValue ) );
                    writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.y, 0, 360, byte.MinValue, byte.MaxValue ) );
                    writer.WriteByte ( FloatBytePacker.ScaleFloatToByte ( euler.z, 0, 360, byte.MinValue, byte.MaxValue ) );
                    break;

                case CompressionTypes.MediumCompression:
                    writer.WriteUInt16 ( FloatBytePacker.PackThreeFloatsIntoUShort ( euler.x, euler.y, euler.z, 0, 360 ) );
                    break;
            }

            writer.WriteVector3 ( scale );
        }
        private void DeserializeFromReader ( NetworkReader reader )
        {
            var temp = new StatePoint
            {
                LocalPosition = reader.ReadVector3 ()
            };

            switch ( Compression )
            {
                case CompressionTypes.NoCompression:
                    {
                        var x = reader.ReadSingle ();
                        var y = reader.ReadSingle ();
                        var z = reader.ReadSingle ();
                        temp.LocalRotation = Quaternion.Euler ( x, y, z );
                        break;
                    }

                case CompressionTypes.LowCompression:
                    {
                        var x = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                        var y = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                        var z = FloatBytePacker.ScaleByteToFloat ( reader.ReadByte (), byte.MinValue, byte.MaxValue, 0, 360 );
                        temp.LocalRotation = Quaternion.Euler ( x, y, z );
                        break;
                    }

                case CompressionTypes.MediumCompression:
                    {
                        var xyz = FloatBytePacker.UnpackUShortIntoThreeFloats ( reader.ReadUInt16 (), 0, 360 );
                        temp.LocalRotation = Quaternion.Euler ( xyz.x, xyz.y, xyz.z );
                        break;
                    }
            }

            temp.LocalScale = reader.ReadVector3 ();
            temp.TimeStamp = Time.time;
            temp.MovementSpeed = EstimateMovementSpeed ( EndPoint, temp, Target, syncInterval );

            if ( StartPoint == null )
            {
                StartPoint = new StatePoint
                {
                    TimeStamp = Time.time - syncInterval,
                    LocalPosition = Target.localPosition,
                    LocalRotation = Target.localRotation,
                    LocalScale = Target.localScale,
                    MovementSpeed = temp.MovementSpeed
                };
            }
            else
            {
                var oldDistance = Vector3.Distance ( StartPoint.LocalPosition, EndPoint.LocalPosition );
                var newDistance = Vector3.Distance ( EndPoint.LocalPosition, temp.LocalPosition );

                StartPoint = EndPoint;

                if ( Vector3.Distance ( Target.localPosition, StartPoint.LocalPosition ) < oldDistance + newDistance )
                {
                    StartPoint.LocalPosition = Target.localPosition;
                    StartPoint.LocalRotation = Target.localRotation;
                    StartPoint.LocalScale = Target.localScale;
                }
            }

            EndPoint = temp;
        }

        private static float EstimateMovementSpeed ( StatePoint startPoint, StatePoint endPoint, Transform transform, float sendInterval )
        {
            var delta = endPoint.LocalPosition - ( startPoint != null ? startPoint.LocalPosition : transform.localPosition );
            var elapsed = startPoint != null ? endPoint.TimeStamp - startPoint.TimeStamp : sendInterval;

            return elapsed > 0 ? delta.magnitude / elapsed : 0;
        }
        private static float CurrentInterpolationFactor ( StatePoint startPoint, StatePoint endPoint )
        {
            if ( startPoint != null )
            {
                float difference = endPoint.TimeStamp - startPoint.TimeStamp;

                float elapsed = Time.time - endPoint.TimeStamp;
                return difference > 0 ? elapsed / difference : 0;
            }
            return 0;
        }
        private static Vector3 InterpolatePosition ( StatePoint startPoint, StatePoint endPoint, Vector3 currentPosition )
        {
            if ( startPoint != null )
            {

                float speed = Mathf.Max ( startPoint.MovementSpeed, endPoint.MovementSpeed );
                return Vector3.MoveTowards ( currentPosition, endPoint.LocalPosition, speed * Time.deltaTime );
            }
            return currentPosition;
        }
        private static Quaternion InterpolateRotation ( StatePoint startPoint, StatePoint endPoint, Quaternion defaultRotation )
        {
            if ( startPoint != null )
            {
                float t = CurrentInterpolationFactor ( startPoint, endPoint );
                return Quaternion.Slerp ( startPoint.LocalRotation, endPoint.LocalRotation, t );
            }
            return defaultRotation;
        }
        private static Vector3 InterpolateScale ( StatePoint startPoint, StatePoint endPoint, Vector3 currentScale )
        {
            if ( startPoint != null )
            {
                float t = CurrentInterpolationFactor ( startPoint, endPoint );
                return Vector3.Lerp ( startPoint.LocalScale, endPoint.LocalScale, t );
            }
            return currentScale;
        }

        public override bool OnSerialize ( NetworkWriter writer, bool initialState )
        {
            SerializeIntoWriter ( writer, Target.localPosition, Target.localRotation, Target.localScale, Compression );
            return true;
        }
        public override void OnDeserialize ( NetworkReader reader, bool initialState )
        {
            DeserializeFromReader ( reader );
        }

        private bool NeedsTeleport ()
        {
            var startTime = StartPoint != null ? StartPoint.TimeStamp : Time.time - syncInterval;
            var goalTime = EndPoint != null ? EndPoint.TimeStamp : Time.time;
            var difference = goalTime - startTime;
            var timeSinceGoalReceived = Time.time - goalTime;

            return timeSinceGoalReceived > difference * 5;
        }
        private bool HasEitherMovedRotatedScaled ()
        {
            var hasMoved = LastPosition != Target.localPosition;
            var hasRotated = LastRotation != Target.localRotation;
            var hasScaled = LastScale != Target.localScale;
            var hasChange = hasMoved || hasRotated || hasScaled;

            if ( hasChange )
            {
                LastPosition = Target.localPosition;
                LastRotation = Target.localRotation;
                LastScale = Target.localScale;
            }

            return hasChange;
        }
        private void ApplyPositionRotationScale ( Vector3 position, Quaternion rotation, Vector3 scale )
        {
            Target.localPosition = position;

            if ( Compression != CompressionTypes.NoRotation )
            {
                Target.localRotation = rotation;
            }

            Target.localScale = scale;
        }

        void Update ()
        {
            if ( isServer )
            {
                SetDirtyBit ( HasEitherMovedRotatedScaled () ? 1UL : 0UL );
            }

            if ( isClient )
            {
                if ( !isServer && hasAuthority )
                {
                    if ( Time.time - LastClientSendTime >= SyncInterval )
                    {
                        if ( HasEitherMovedRotatedScaled () )
                        {
                            NetworkWriter writer = new NetworkWriter ();
                            SerializeIntoWriter ( writer, Target.localPosition, Target.localRotation, Target.localScale, Compression );

                            CmdClientToServerSync ( writer.ToArray () );
                        }

                        LastClientSendTime = Time.time;
                    }
                }

                if ( !hasAuthority )
                {
                    if ( EndPoint != null )
                    {
                        if ( NeedsTeleport () )
                        {
                            ApplyPositionRotationScale ( EndPoint.LocalPosition, EndPoint.LocalRotation, EndPoint.LocalScale );
                        }
                        else
                        {
                            ApplyPositionRotationScale ( InterpolatePosition ( StartPoint, EndPoint, Target.localPosition ),
                                                       InterpolateRotation ( StartPoint, EndPoint, Target.localRotation ),
                                                       InterpolateScale ( StartPoint, EndPoint, Target.localScale ) );
                        }
                    }
                }
            }
        }

        [ClientCmd]
        private void CmdClientToServerSync ( byte [] payload )
        {
            NetworkReader reader = new NetworkReader ( payload );
            DeserializeFromReader ( reader );

            if ( isServer && !isClient )
                ApplyPositionRotationScale ( EndPoint.LocalPosition, EndPoint.LocalRotation, EndPoint.LocalScale );

            SetDirtyBit ( 1UL );
        }

        #region Gizmos

        private static void DrawDataPointGizmo ( StatePoint point, Color color )
        {
            Vector3 offset = Vector3.up * 0.01f;

            Gizmos.color = color;
            Gizmos.DrawSphere ( point.LocalPosition + offset, 0.5f );

            Gizmos.color = Color.blue;
            Gizmos.DrawRay ( point.LocalPosition + offset, point.LocalRotation * Vector3.forward );

            Gizmos.color = Color.green;
            Gizmos.DrawRay ( point.LocalPosition + offset, point.LocalRotation * Vector3.up );
        }
        private static void DrawLineBetweenDataPoints ( StatePoint data1, StatePoint data2, Color color )
        {
            Gizmos.color = color;
            Gizmos.DrawLine ( data1.LocalPosition, data2.LocalPosition );
        }

        private void OnDrawGizmos ()
        {
            if ( StartPoint != null ) DrawDataPointGizmo ( StartPoint, Color.gray );
            if ( EndPoint != null ) DrawDataPointGizmo ( EndPoint, Color.white );

            if ( StartPoint != null && EndPoint != null ) DrawLineBetweenDataPoints ( StartPoint, EndPoint, Color.cyan );
        }

        #endregion

        public class StatePoint
        {
            public float TimeStamp;
            public Vector3 LocalPosition;
            public Quaternion LocalRotation;
            public Vector3 LocalScale;
            public float MovementSpeed;
        }
    }
}