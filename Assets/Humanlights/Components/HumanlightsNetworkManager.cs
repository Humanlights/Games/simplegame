﻿using Humanlights.Extensions;
using Humanlights.Unity.UNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Humanlights.Unity.Network
{
    [RequireComponent ( typeof ( NetworkManager ) )]
    public class HumanlightsNetworkManager : HumanlightsSingleton<HumanlightsNetworkManager>
    {
        public NetworkManager _manager;
        public NetworkManager UNetManager { get { return _manager ?? ( _manager = GetComponent<NetworkManager> () ?? ( _manager = gameObject.AddComponent<NetworkManager> () ) ); } set { _manager = value; } }

        public enum ConnectionTypes
        {
            None,
            Server,
            Client,
            Host
        }

        #region Properties

        public bool DontDestroyOnLoad { get { return UNetManager.dontDestroyOnLoad; } set { UNetManager.dontDestroyOnLoad = value; } }
        public bool RunInBackground { get { return UNetManager.runInBackground; } set { UNetManager.runInBackground = value; } }
        public bool HeadlessMode { get { return UNetManager.startOnHeadless; } set { UNetManager.startOnHeadless = value; } }

        public int ServerFrameRate { get { return UNetManager.serverTickRate; } set { UNetManager.serverTickRate = value; } }
        public int PreviousServerFrameRate { get; set; } = -1;
        public int MaximumConnections { get { return UNetManager.maxConnections; } set { UNetManager.maxConnections = value; } }
        public int ConnectionCount { get; set; }

        public string Ip { get { return UNetManager.networkAddress; } set { UNetManager.networkAddress = value; } }
        public ushort Port { get { if ( Transport == null ) return 7777; return Transport.port; } set { if ( Transport != null ) Transport.port = value; } }

        public TelepathyTransport Transport { get { return UNetManager.transport as TelepathyTransport; } set { UNetManager.transport = value; } }
        public int ServerMaxMessageSize { get { return Transport.serverMaxMessageSize; } set { Transport.serverMaxMessageSize = value; } }
        public int ClientMaxMessageSize { get { return Transport.clientMaxMessageSize; } set { Transport.clientMaxMessageSize = value; } }

        public GameObject PlayerPrefab { get { return UNetManager.playerPrefab; } set { UNetManager.playerPrefab = value; } }
        public bool AutoCreatePlayer { get { return UNetManager.autoCreatePlayer; } set { UNetManager.autoCreatePlayer = value; } }
        public PlayerSpawnMethod PlayerSpawnMethod { get { return UNetManager.playerSpawnMethod; } set { UNetManager.playerSpawnMethod = value; } }
        public List<GameObject> SpawnablePrefabs { get { return UNetManager.spawnPrefabs; } set { UNetManager.spawnPrefabs = value; } }

        public HumanlightsNetworkSpawnpoint [] SpawnPoints { get { return UNetManager.startPositions.Select ( x => x.GetComponent<HumanlightsNetworkSpawnpoint> () ).ToArray (); } }

        public bool IsConnected { get { return NetworkClient.isConnected; } }
        public ConnectionTypes ConnectionType { get; private set; }

        public HumanlightsNetworkStatistics Statistics { get; private set; } = new HumanlightsNetworkStatistics ();

        #endregion

        #region Methods

        public override void Awake ()
        {
            base.Awake ();

            Reset ();

            UNetManager.OnServerConnectCallback += ( NetworkConnection connection ) =>
            {
                ConnectionCount++;
            };
            UNetManager.OnServerDisconnectCallback += ( NetworkConnection connection ) =>
            {
                ConnectionCount--;
            };

            UNetManager.OnServerDisconnectCallback += ( NetworkConnection connection ) =>
            {
                ConnectionType = ConnectionTypes.None;
                Reset ();
            };
            UNetManager.OnServerErrorCallback += ( NetworkConnection connection, int errorCode ) =>
            {
                ConnectionType = ConnectionTypes.None;
                Reset ();
            };

            UNetManager.OnStopServerCallback += () =>
            {
                ConnectionType = ConnectionTypes.None;
                Reset ();
            };

            UNetManager.OnClientDisconnectCallback += ( NetworkConnection connection ) =>
            {
                if ( ConnectionType == ConnectionTypes.Client && ConnectionType != ConnectionTypes.Host )
                {
                    ConnectionType = ConnectionTypes.None;
                    Reset ();
                }
            };
            UNetManager.OnClientErrorCallback += ( NetworkConnection connection, int errorCode ) =>
            {
                if ( ConnectionType == ConnectionTypes.Client && ConnectionType != ConnectionTypes.Host )
                {
                    ConnectionType = ConnectionTypes.None;
                    Reset ();
                }
            };

            UNetManager.OnStartHostCallback += () => { };
            UNetManager.OnStopHostCallback += () =>
            {
                if ( ConnectionType == ConnectionTypes.Host )
                {
                    ConnectionType = ConnectionTypes.None;
                    Reset ();
                }
            };
        }

        public void RegisterSpawnpoint ( HumanlightsNetworkSpawnpoint spawnpoint )
        {
            UNetManager?.RegisterStartPosition ( spawnpoint.transform );
        }
        public void UnregisterSpawnpoint ( HumanlightsNetworkSpawnpoint spawnpoint )
        {
            UNetManager?.UnRegisterStartPosition ( spawnpoint.transform );
        }

        public virtual void SetAddress ( string ip, ushort port )
        {
            Ip = ip;
            Port = port;
        }

        public virtual void StartServer ()
        {
            UNetManager.StartServer ();

            ConnectionType = ConnectionTypes.Server;
            Statistics.ConnectionStartTime = DateTick.Current;

            ApplyServerFrameRateRate ();
        }
        public virtual void StopServer ()
        {
            UNetManager.StopServer ();

            ConnectionType = ConnectionTypes.None;
        }

        public virtual void StartClient ()
        {
            UNetManager.StartClient ();

            ConnectionType = ConnectionTypes.Client;
            Statistics.ConnectionStartTime = DateTick.Current;
        }
        public virtual void StopClient ()
        {
            UNetManager.StopClient ();

            ConnectionType = ConnectionTypes.None;
        }

        public virtual void StartHost ()
        {
            UNetManager.StartHost ();

            ConnectionType = ConnectionTypes.Host;
            Statistics.ConnectionStartTime = DateTick.Current;
        }
        public virtual void StopHost ()
        {
            UNetManager.StopHost ();

            ConnectionType = ConnectionTypes.None;
        }

        public string GetLocalIPAddress ()
        {
            var host = Dns.GetHostEntry ( Dns.GetHostName () );

            foreach ( var ip in host.AddressList )
            {
                if ( ip.AddressFamily == AddressFamily.InterNetwork )
                {
                    return ip.ToString ();
                }
            }

            return "localhost";
        }
        public ushort GetRandomPort ()
        {
            return ( ushort ) RandomEx.GetRandomInteger ( 5000, 29999 );
        }

        public void ApplyServerFrameRateRate ()
        {
            PreviousServerFrameRate = Application.targetFrameRate;
            Application.targetFrameRate = ServerFrameRate;
        }
        public void RestoreServerFrameRate ()
        {
            Application.targetFrameRate = PreviousServerFrameRate;
        }

        /// <summary>
        /// This method is basically StopServer / Stop Client / StopHost, except it figures it out automatically.
        /// </summary>
        public virtual void Shutdown ()
        {
            StopServer ();
            StopClient ();
            StopHost ();

            RestoreServerFrameRate ();
        }

        private void Reset ()
        {
            Transport.Reset ();

            Statistics = new HumanlightsNetworkStatistics ();
        }

        public HumanlightsNetworkStatistics GetStatistics ()
        {
            if ( ConnectionType == ConnectionTypes.None ) { Reset (); return Statistics; }

            Statistics.CurrentBytesPerSecondReceived = Transport.ClientDataReceivedCount + Transport.ServerDataReceivedCount;
            Statistics.TotalBytesPerSecondReceived = Transport.ClientDataReceivedTotalCount + Transport.ServerDataReceivedTotalCount;
            Statistics.CurrentBytesPerSecondSent = Transport.ClientDataSentCount + Transport.ServerDataSentCount;
            Statistics.TotalBytesPerSecondSent = Transport.ClientDataSentTotalCount + Transport.ServerDataSentTotalCount;

            Statistics.TotalMessagesInBuffer = Transport.client.ReceiveQueueCount + Transport.server.ReceiveQueueCount;

            return Statistics;
        }

        #endregion

        #region Callbacks

        public Action OnStartHost { get { return UNetManager.OnStartHostCallback; } set { UNetManager.OnStartHostCallback = value; } }
        public Action OnStopHost { get { return UNetManager.OnStopHostCallback; } set { UNetManager.OnStopHostCallback = value; } }

        public Action OnStartServer { get { return UNetManager.OnStartServerCallback; } set { UNetManager.OnStartServerCallback = value; } }
        public Action OnStopServer { get { return UNetManager.OnStopServerCallback; } set { UNetManager.OnStopServerCallback = value; } }

        public Action OnStartClient { get { return UNetManager.OnStartClientCallback; } set { UNetManager.OnStartClientCallback = value; } }
        public Action OnStopClient { get { return UNetManager.OnStopClientCallback; } set { UNetManager.OnStopClientCallback = value; } }

        //
        // Server
        //
        /// <summary>
        /// Client connected to the server.
        /// </summary>
        public Action<NetworkConnection> OnServerConnect { get { return UNetManager.OnServerConnectCallback; } set { UNetManager.OnServerConnectCallback = value; } }
        public Action<NetworkConnection> OnServerDisconnect { get { return UNetManager.OnServerDisconnectCallback; } set { UNetManager.OnServerDisconnectCallback = value; } }
        public Action<NetworkConnection> OnServerReady { get { return UNetManager.OnServerReadyCallback; } set { UNetManager.OnServerReadyCallback = value; } }
        public Action<NetworkConnection, NetworkIdentity> OnServerAddPlayer { get { return UNetManager.OnServerAddPlayerCallback; } set { UNetManager.OnServerAddPlayerCallback = value; } }
        public Action<NetworkConnection, NetworkIdentity> OnServerRemovePlayer { get { return UNetManager.OnServerRemovePlayerCallback; } set { UNetManager.OnServerRemovePlayerCallback = value; } }
        public Action<NetworkConnection, int> OnServerError { get { return UNetManager.OnServerErrorCallback; } set { UNetManager.OnServerErrorCallback = value; } }
        public Action<string> OnServerChangeScene { get { return UNetManager.OnServerChangeSceneCallback; } set { UNetManager.OnServerChangeSceneCallback = value; } }
        public Action<string> OnServerSceneChanged { get { return UNetManager.OnServerSceneChangedCallback; } set { UNetManager.OnServerSceneChangedCallback = value; } }

        //
        // Client
        //
        public Action<NetworkConnection> OnClientConnect { get { return UNetManager.OnClientConnectCallback; } set { UNetManager.OnClientConnectCallback = value; } }
        public Action<NetworkConnection> OnClientDisconnect { get { return UNetManager.OnClientDisconnectCallback; } set { UNetManager.OnClientDisconnectCallback = value; } }
        public Action<NetworkConnection, int> OnClientError { get { return UNetManager.OnClientErrorCallback; } set { UNetManager.OnClientErrorCallback = value; } }
        public Action<string, SceneOperation> OnClientChangeScene { get { return UNetManager.OnClientChangeSceneCallback; } set { UNetManager.OnClientChangeSceneCallback = value; } }
        public Action<NetworkConnection> OnClientSceneChanged { get { return UNetManager.OnClientSceneChangedCallback; } set { UNetManager.OnClientSceneChangedCallback = value; } }

        #endregion

        public virtual void StartServer ( string ip, ushort port = 7777, int? maximumConnections = null )
        {
            SetAddress ( ip, port );
            if ( maximumConnections.HasValue ) MaximumConnections = maximumConnections.Value;

            StartServer ();
        }
        public virtual void StartClient ( string ip, ushort port = 7777 )
        {
            SetAddress ( ip, port );
            StartClient ();
        }
        public virtual void StartHost ( string ip = "localhost", ushort port = 7777, int? maximumConnections = null )
        {
            SetAddress ( ip, port );
            if ( maximumConnections.HasValue ) MaximumConnections = maximumConnections.Value;

            StartHost ();
        }
    }
}