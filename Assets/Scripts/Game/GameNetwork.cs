﻿using Humanlights.Unity.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameNetwork : HumanlightsNetworkManager
{
    public new static GameNetwork Instance { get; set; }

    public override void Awake ()
    {
        base.Awake ();

        Instance = this;
    }
}
