﻿using Humanlights.Extensions;
using Humanlights.Unity.EditorTool;
using Humanlights.Unity.EditorTool.Components;
using Humanlights.Unity.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TestingEditor : HumanlightsEditor
{
    [MenuItem ( "Humanlights/Testing Tool" )]
    public static void Launch ()
    {
        ShowEditor ( typeof ( TestingEditor ) );
    }

    public override void InstallTabs ()
    {
        base.InstallTabs ();

        UnityUI.Tabs.Add ( GetTab () );
    }

    public EditorTab GetTab ()
    {
        var tab = new EditorTab ( "Testing Area", "This is some area where testing happens and shit." );

        tab.SubTabs.Add ( GetTest () );

        return tab;
    }


    private EditorSubTab GetTest ()
    {
        var subTab = new EditorSubTab ( "Shit", "Hewo wowd.",
            onDraw: delegate ()
            {
                UnityUI.HorizontalGroup ( () =>
                {
                    UnityUI.ButtonImportant ( "Build Client", () =>
                      {
                          var buildsFolder = $"{Application.dataPath}/../Builds";
                          OsEx.Folder.Create ( buildsFolder );

                          HumanlightsNetworkEditor.SwitchClient ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Client", "TestClient.exe", Humanlights.PlatformType.Win, true, false );

                      }, false );
                    UnityUI.ButtonImportant ( "Build Server", () =>
                      {
                          var buildsFolder = $"{Application.dataPath}/../Builds";
                          OsEx.Folder.Create ( buildsFolder );

                          HumanlightsNetworkEditor.SwitchServer ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Server", "TestServer.exe", Humanlights.PlatformType.Win, true, false );
                          if ( !OsEx.File.Exists ( $"{buildsFolder}/Server/start.bat" ) ) OsEx.File.Create ( $"{buildsFolder}/Server/start.bat", "TestServer.exe -batchmode +port 12000" );

                      }, false );
                    UnityUI.ButtonImportant ( "Build Host", () =>
                      {
                          var buildsFolder = $"{Application.dataPath}/../Builds";
                          OsEx.Folder.Create ( buildsFolder );

                          HumanlightsNetworkEditor.SwitchClientServer ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Host", "TestHost.exe", Humanlights.PlatformType.Win, true, false );

                      }, false );
                } );

                UnityUI.HorizontalGroup ( () =>
                {
                    UnityUI.ButtonImportant ( "Build Client & Server (Separately)", () =>
                      {
                          var buildsFolder = $"{Application.dataPath}/../Builds";
                          OsEx.Folder.Create ( buildsFolder );

                          HumanlightsNetworkEditor.SwitchClient ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Client", "TestClient.exe", Humanlights.PlatformType.Win, true, false );

                          HumanlightsNetworkEditor.SwitchServer ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Server", "TestServer.exe", Humanlights.PlatformType.Win, true, false );
                          if ( !OsEx.File.Exists ( $"{buildsFolder}/Server/start.bat" ) ) OsEx.File.Create ( $"{buildsFolder}/Server/start.bat", "TestServer.exe -batchmode +port 12000" );

                      }, false );
                    UnityUI.ButtonImportant ( "Build All (Client, Server & Host)", () =>
                      {
                          var buildsFolder = $"{Application.dataPath}/../Builds";
                          OsEx.Folder.Create ( buildsFolder );

                          HumanlightsNetworkEditor.SwitchClient ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Client", "TestClient.exe", Humanlights.PlatformType.Win, true, false );

                          HumanlightsNetworkEditor.SwitchServer ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Server", "TestServer.exe", Humanlights.PlatformType.Win, true, false );
                          if ( !OsEx.File.Exists ( $"{buildsFolder}/Server/start.bat" ) ) OsEx.File.Create ( $"{buildsFolder}/Server/start.bat", "TestServer.exe -batchmode +port 12000" );

                          HumanlightsNetworkEditor.SwitchClientServer ();
                          Humanlights.Unity.Editor.BuildWrapper.Build ( $"{buildsFolder}/Host", "TestHost.exe", Humanlights.PlatformType.Win, true, false );

                      }, false );
                } );
            } );

        return subTab;
    }

}
