﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Humanlights.Extensions;
using System;
using Humanlights.Unity.Network;
using Humanlights.Components;

public class GameUI : HumanlightsSingleton<GameUI>
{
    public InputField IpInput, PortInput, NameInput;
    public Text StatusText, StatsText;
    public Button ConnectButton, DisconnectButton;
    public CanvasGroup Group;

    public void Start ()
    {
        NameInput.text += $" {RandomEx.GetRandomInteger ( 1000, 9999 )}";
        SetupCallbacks ();
        GameNetwork.Instance.SetAddress ( GameNetwork.Instance.GetLocalIPAddress (), 12000 );

        CanvasGroupEx.Enable ( Group, true );
        IpInput.text = GameNetwork.Instance.Ip;
        PortInput.text = GameNetwork.Instance.Port.ToString ();

        Status = "Idle";
        DisconnectButton.gameObject.SetActive ( false );

        InvokeRepeating ( () => { Refresh (); }, 1f, 1f );
    }

    private void SetupCallbacks ()
    {
        GameNetwork.Instance.OnClientDisconnect += ( connection ) => { Status = $"Time out"; CanvasGroupEx.Enable ( Group, true ); Group.interactable = true; DisconnectButton.gameObject.SetActive ( false ); };
        GameNetwork.Instance.OnClientConnect += ( connection ) =>
        {
            Status = $"Connected: {IpInput.text}:{PortInput.text}"; CanvasGroupEx.Enable ( Group, false );
            DisconnectButton.gameObject.SetActive ( true );
        };
    }
    private void Refresh ()
    {
        var manager = HumanlightsNetworkManager.Instance;
        var statistics = manager.GetStatistics ();
        var client = HumanlightsClient.Instance;

        var sb = new StringBody ();

        sb.Add ( $"Connected as: <b>{manager?.ConnectionType}</b>" )
            .Add ( $"IP: <b>{manager?.Ip}</b>" )
            .Add ( $"Port: <b>{manager?.Port}</b>" )
            .Add ( $"Connections: <b>{manager?.ConnectionCount}/{manager?.MaximumConnections}</b>" )
            .Add ( $"Connection Time: <b>{TimeEx.Format ( statistics.ConnectionPassedTime, true, true ).ToLower ()}</b>" )
            .Add ( "" )

        .Add ( $"Current Bytes Per Second Sent: <b>{statistics.CurrentBytesPerSecondSent} / {ByteEx.Format ( statistics.CurrentBytesPerSecondSent, shortName: true ).ToLower ()}</b>" )
            .Add ( $"Current Bytes Per Second Received: <b>{statistics.CurrentBytesPerSecondReceived} / {ByteEx.Format ( statistics.CurrentBytesPerSecondReceived, shortName: true ).ToLower ()}</b>" )
            .Add ( $"Total Bytes Per Second Sent: <b>{statistics.TotalBytesPerSecondSent} / {ByteEx.Format ( statistics.TotalBytesPerSecondSent, shortName: true ).ToLower ()}</b>" )
            .Add ( $"Total Bytes Per Second Received: <b>{statistics.TotalBytesPerSecondReceived} / {ByteEx.Format ( statistics.TotalBytesPerSecondReceived, shortName: true ).ToLower ()}</b>" )
            .Add ( $"Total Messages In Buffer: <b>{statistics.TotalMessagesInBuffer}</b>" ).Add ( "" )

        .Add ( $"Client" )
            .Add ( $"Connections: <b>{client?.ConnectedUserCount}/{client?.MaximumConnectionCount}</b>" )
            .Add ( $"Frame-Rate: <b>{client?.FrameRate}</b>" );

        StatsText.text = sb.ToNewLine ();
    }

    public string Status { get { return StatusText.text; } set { StatusText.text = value; } }

    public void Connect ()
    {
        GameNetwork.Instance.StartClient ( IpInput.text, ushort.Parse ( PortInput.text ) );

        Status = "Connecting...";
        Group.interactable = false;
    }
    public void Disconnect ()
    {
        GameNetwork.Instance.Shutdown ();

        Status = "Disconnected";
        Group.interactable = true;
        CanvasGroupEx.Enable ( Group, true );
        DisconnectButton.gameObject.SetActive ( false );
    }
}
