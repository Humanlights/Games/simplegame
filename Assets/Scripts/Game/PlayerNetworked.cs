﻿using Humanlights.Unity.Network;
using Humanlights.Unity.UNet;
using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworked : HumanlightsNetworkBehaviour
{
    public static PlayerNetworked Local { get; set; }

    [SyncVar] public string Name;
    public vThirdPersonInput Input;
    public TextMesh Text;

    public void Start ()
    {
        Text.text = Name;
    }

    public override void OnStartAuthority ()
    {
        base.OnStartAuthority ();

        Local = this;

        Input.enabled = true;

        GameManager.Instance.Camera.SetMainTarget ( transform );
        GameManager.Instance.Camera.Init ();

        Input.tpCamera = GameManager.Instance.Camera;
        CmdName ( Name = GameUI.Instance.NameInput.text );
    }

    [ClientCmd] public void CmdName ( string value ) { Name = value; RpcName ( value ); }
    [ServerCmd] public void RpcName ( string value ) { Name = value; Text.text = Name; }
}